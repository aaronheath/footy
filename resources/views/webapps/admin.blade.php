<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin Webapp</title>

    <link rel="stylesheet" href="{{ mix('/css/webapp-admin.css') }}">
</head>

<body>
<div id="app"></div>

<script>window.footy_api_token = '{{ $user->api_token }}'</script>

<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('/js/app-admin.js') }}"></script>
</body>
</html>
