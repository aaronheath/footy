<!DOCTYPE html>
<html>
<head>
    <title>Public Webapp</title>

    <link rel="stylesheet" href="{{ mix('/css/webapp-public.css') }}">
</head>

<body>
<div id="app"></div>

<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('/js/app-public.js') }}"></script>
</body>
</html>
