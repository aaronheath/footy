import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuelidate from 'vuelidate';
// import * as VueGoogleMaps from 'vue2-google-maps';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fal } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Chart from 'chart.js';

import routes from './routes';
import App from './components/App.vue';

/**
 * Register Plugins
 */

Vue.use(Vuelidate);
Vue.use(VueRouter);
// Vue.use(VueGoogleMaps, {
//     load: {
//         key: conf('google_maps_api'),
//         libraries: 'places',
//     },
// });

Chart.pluginService.register({
  beforeDraw(chart) {
    if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
      const { ctx } = chart.chart;
      const { chartArea } = chart;

      ctx.save();
      ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
      ctx.fillRect(
        chartArea.left,
        chartArea.top,
        chartArea.right - chartArea.left,
        chartArea.bottom - chartArea.top,
      );
      ctx.restore();
    }
  },
});

/**
 * Font Awesome Pro Icons
 */

library.add(fal);
Vue.component('fa', FontAwesomeIcon);

/**
 * Configure Vue Router
 */

/* eslint-disable no-new */
const router = new VueRouter({
  routes,
  scrollBehavior: () => ({ x: 0, y: 0 }),
});

/**
 * Global mixin
 */

Vue.mixin({
  methods: {
    setPageTitle(title) {
      this.$emit('pageTitle', title);
    },
  },
});

/**
 * Create Vue Application
 */

new Vue({
  el: '#app',
  components: { App },
  router,
  template: '<App/>',
});
