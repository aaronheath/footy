import ViewLeagues from './components/League/ViewLeagues.vue';
import CreateOrUpdateLeague from './components/League/CreateOrUpdateLeague.vue';
import ViewSeasons from './components/Season/ViewSeasons.vue';
import CreateOrUpdateSeason from './components/Season/CreateOrUpdateSeason.vue';
import ViewSeries from './components/Series/ViewSeries.vue';
import CreateOrUpdateSeries from './components/Series/CreateOrUpdateSeries.vue';
import ViewStates from './components/State/ViewStates.vue';
import CreateOrUpdateState from './components/State/CreateOrUpdateState.vue';
import ViewVenues from './components/Venue/ViewVenues.vue';
import CreateOrUpdateVenue from './components/Venue/CreateOrUpdateVenue.vue';
import ViewTeams from './components/Team/ViewTeams.vue';
import CreateOrUpdateTeam from './components/Team/CreateOrUpdateTeam.vue';
import ViewRounds from './components/Round/ViewRounds.vue';
import CreateOrUpdateRound from './components/Round/CreateOrUpdateRound.vue';
import ViewMatches from './components/Match/ViewMatches.vue';
import CreateOrUpdateMatch from './components/Match/CreateOrUpdateMatch.vue';

export default [
  { path: '/league/all', component: ViewLeagues, name: 'league-all' },
  { path: '/league/create', component: CreateOrUpdateLeague, name: 'league-create' },
  { path: '/league/:id', component: CreateOrUpdateLeague, name: 'league-update' },
  { path: '/season/all', component: ViewSeasons, name: 'season-all' },
  { path: '/season/create', component: CreateOrUpdateSeason, name: 'season-create' },
  { path: '/season/:id', component: CreateOrUpdateSeason, name: 'season-update' },
  { path: '/series/all', component: ViewSeries, name: 'series-all' },
  { path: '/series/create', component: CreateOrUpdateSeries, name: 'series-create' },
  { path: '/series/:id', component: CreateOrUpdateSeries, name: 'series-update' },
  { path: '/state/all', component: ViewStates, name: 'state-all' },
  { path: '/state/create', component: CreateOrUpdateState, name: 'state-create' },
  { path: '/state/:id', component: CreateOrUpdateState, name: 'state-update' },
  { path: '/venue/all', component: ViewVenues, name: 'venue-all' },
  { path: '/venue/create', component: CreateOrUpdateVenue, name: 'venue-create' },
  { path: '/venue/:id', component: CreateOrUpdateVenue, name: 'venue-update' },
  { path: '/team/all', component: ViewTeams, name: 'team-all' },
  { path: '/team/create', component: CreateOrUpdateTeam, name: 'team-create' },
  { path: '/team/:id', component: CreateOrUpdateTeam, name: 'team-update' },
  { path: '/round/all', component: ViewRounds, name: 'round-all' },
  { path: '/round/create', component: CreateOrUpdateRound, name: 'round-create' },
  { path: '/round/:id', component: CreateOrUpdateRound, name: 'round-update' },
  { path: '/match/all', component: ViewMatches, name: 'match-all' },
  { path: '/match/create', component: CreateOrUpdateMatch, name: 'match-create' },
  { path: '/match/:id', component: CreateOrUpdateMatch, name: 'match-update' },
];
