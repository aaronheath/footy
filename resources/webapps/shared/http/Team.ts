import TeamWrapper from '../wrappers/Team';
import BaseHttp from './BaseHttp';

export default class Team extends BaseHttp {
  public static init() {
    return new Team(TeamWrapper, 'teams');
  }

  public static async index(pageNumber) {
    return Team.init().requestIndex(pageNumber);
  }

  public static async fetch(id) {
    return Team.init().requestFetch(id);
  }

  public static async create(values) {
    return Team.init().requestCreate(values);
  }

  public static async update(model, values) {
    return Team.init().requestUpdate(model, values);
  }

  public static async remove(model) {
    return Team.init().requestRemove(model);
  }
}
