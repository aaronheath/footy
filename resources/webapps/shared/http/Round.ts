import RoundWrapper from '../wrappers/Round';
import BaseHttp from './BaseHttp';

export default class Round extends BaseHttp {
  public static init() {
    return new Round(RoundWrapper, 'rounds');
  }

  public static async index(pageNumber, queryBuilder) {
    return Round.init().requestIndex(pageNumber, queryBuilder);
  }

  public static async fetch(id) {
    return Round.init().requestFetch(id);
  }

  public static async create(values) {
    return Round.init().requestCreate(values);
  }

  public static async update(model, values) {
    return Round.init().requestUpdate(model, values);
  }

  public static async remove(model) {
    return Round.init().requestRemove(model);
  }
}
