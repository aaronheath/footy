import SeasonWrapper from '../wrappers/Season';
import BaseHttp from './BaseHttp';

export default class Season extends BaseHttp {
  public static init() {
    return new Season(SeasonWrapper, 'seasons');
  }

  public static async index(pageNumber) {
    return Season.init().requestIndex(pageNumber);
  }

  public static async fetch(id) {
    return Season.init().requestFetch(id);
  }

  public static async create(values) {
    return Season.init().requestCreate(values);
  }

  public static async update(model, values) {
    return Season.init().requestUpdate(model, values);
  }

  public static async remove(model) {
    return Season.init().requestRemove(model);
  }
}
