export default class QueryBuilder {
  protected filters = {};

  constructior() {
    //
  }

  public static create() {
    return new QueryBuilder();
  }

  public asObject() {
    return {...this.flattenFilters()};
  }

  public filter(key: string, value: string | number) {
    this.filters[key] = value;

    return this;
  }

  protected flattenFilters() {
    const flattened = {};

    Object.keys(this.filters).forEach((key) => {
      flattened[`filter[${key}]`] = this.filters[key];
    });

    return flattened;
  }
}
