import SeriesWrapper from '../wrappers/Series';
import BaseHttp from './BaseHttp';

export default class Series extends BaseHttp {
  public static init() {
    return new Series(SeriesWrapper, 'series');
  }

  public static async index(pageNumber, queryBuilder) {
    return Series.init().requestIndex(pageNumber, queryBuilder);
  }

  public static async fetch(id) {
    return Series.init().requestFetch(id);
  }

  public static async create(values) {
    return Series.init().requestCreate(values);
  }

  public static async update(model, values) {
    return Series.init().requestUpdate(model, values);
  }

  public static async remove(model) {
    return Series.init().requestRemove(model);
  }
}
