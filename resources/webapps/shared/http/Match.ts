import MatchWrapper from '../wrappers/Match';
import BaseHttp from './BaseHttp';

export default class Match extends BaseHttp {
  public static init() {
    return new Match(MatchWrapper, 'matches');
  }

  public static async index(pageNumber) {
    return Match.init().requestIndex(pageNumber);
  }

  public static async fetch(id) {
    return Match.init().requestFetch(id);
  }

  public static async create(values) {
    return Match.init().requestCreate(values);
  }

  public static async update(model, values) {
    return Match.init().requestUpdate(model, values);
  }

  public static async remove(model) {
    return Match.init().requestRemove(model);
  }
}
