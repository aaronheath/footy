import StateWrapper from '../wrappers/State';
import BaseHttp from './BaseHttp';

export default class State extends BaseHttp {
  public static init() {
    return new State(StateWrapper, 'states');
  }

  public static async index(pageNumber) {
    return State.init().requestIndex(pageNumber);
  }

  public static async fetch(id) {
    return State.init().requestFetch(id);
  }

  public static async create(values) {
    return State.init().requestCreate(values);
  }

  public static async update(model, values) {
    return State.init().requestUpdate(model, values);
  }

  public static async remove(model) {
    return State.init().requestRemove(model);
  }
}
