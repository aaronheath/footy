import LeagueWrapper from '../wrappers/League';
import BaseHttp from './BaseHttp';

export default class League extends BaseHttp {
  public static init() {
    return new League(LeagueWrapper, 'leagues');
  }

  public static async index(pageNumber) {
    return League.init().requestIndex(pageNumber);
  }

  public static async fetch(id) {
    return League.init().requestFetch(id);
  }

  public static async create(values) {
    return League.init().requestCreate(values);
  }

  public static async update(model, values) {
    return League.init().requestUpdate(model, values);
  }

  public static async remove(model) {
    return League.init().requestRemove(model);
  }
}
