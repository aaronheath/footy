import VenueWrapper from '../wrappers/Venue';
import BaseHttp from './BaseHttp';

export default class Venue extends BaseHttp {
  public static init() {
    return new Venue(VenueWrapper, 'venues');
  }

  public static async index(pageNumber) {
    return Venue.init().requestIndex(pageNumber);
  }

  public static async fetch(id) {
    return Venue.init().requestFetch(id);
  }

  public static async create(values) {
    return Venue.init().requestCreate(values);
  }

  public static async update(model, values) {
    return Venue.init().requestUpdate(model, values);
  }

  public static async remove(model) {
    return Venue.init().requestRemove(model);
  }
}
