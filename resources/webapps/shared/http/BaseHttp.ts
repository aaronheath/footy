import HttpCall from '../wrappers/HttpCall';
import QueryBuilder from './QueryBuilder';

export default abstract class BaseHttp {
  protected wrapper;

  protected namespace;

  constructor(wrapper, namespace) {
    this.wrapper = wrapper;
    this.namespace = namespace;
  }

  async requestIndex(pageNumber, queryBuilder: QueryBuilder | null = null) {
    const httpCall = HttpCall.create('get', `/api/${this.namespace}`).page(pageNumber);

    if(queryBuilder) {
      httpCall.query(queryBuilder.asObject());
    }

    const response = await httpCall.call();

    if (response.isError) {
      return window.alert(`An error was received when requesting ${this.namespace}.`);
    }

    return {
      [this.namespace]: response.data.map((item) => this.wrapper.create(item)),
      ...(response.hasPagination ? { pagination: response.pagination } : {}),
    };
  }

  async requestFetch(id) {
    const response = await HttpCall.create('get', `/api/${this.namespace}/${id}`).call();

    if (response.isError) {
      return window.alert(`An error was received when requesting individual ${this.namespace}.`);
    }

    return this.wrapper.create(response.data);
  }

  async requestCreate(values) {
    const response = await HttpCall.create('post', `/api/${this.namespace}`)
      .body(values)
      .call();

    if (response.isError) {
      return window.alert(`An error occurred when creating ${this.namespace}.`);
    }

    return true;
  }

  async requestUpdate(model, values) {
    const response = await HttpCall.create('put', `/api/${this.namespace}/${model.id}`)
      .body(values)
      .call();

    if (response.isError) {
      return window.alert(`An error occurred when updating ${this.namespace}.`);
    }

    return true;
  }

  async requestRemove(model) {
    const response = await HttpCall.create('delete', `/api/${this.namespace}/${model.id}`).call();

    if (response.isError) {
      return window.alert(`An error was received removing individual ${this.namespace}.`);
    }

    return true;
  }
}
