export default class State {
  protected model;

  protected _homeTeams = [];

  protected _venues = [];

  constructor(series) {
    this.model = series;

    // this.homeTeams = this.model.home_teams;
    // this.venues = this.model.venues;
  }

  static create(series) {
    return new State(series);
  }

  get id() {
    return this.model.id;
  }

  get name() {
    return this.model.name;
  }

  get abbreviation() {
    return this.model.abbreviation;
  }

  get timezone() {
    return this.model.timezone;
  }

  // set homeTeams(homeTeams) {
  //   if (!homeTeams) {
  //     return;
  //   }
  //
  //   this._homeTeams = homeTeams.map((team) => Team.create(team));
  // }

  get homeTeams() {
    return this._homeTeams;
  }

  // set venues(venues) {
  //   if (!venues) {
  //     return;
  //   }
  //
  //   this._venues = venues.map((venue) => Venue.create(venue));
  // }

  get venues() {
    return this._venues;
  }

  get createdAt() {
    return this.model.created_at; // TODO convert to object
  }

  get updatedAt() {
    return this.model.updatedAt; // TODO convert to object
  }
}
