import Series from './Series';

export default class Round {
  protected model;

  protected _series;

  constructor(round) {
    this.model = round;

    this.series = this.model.series;
  }

  static create(round) {
    return new Round(round);
  }

  get id() {
    return this.model.id;
  }

  get name() {
    return this.model.name;
  }

  get round() {
    return this.model.round;
  }

  set series(series) {
    if (!series) {
      return;
    }

    this._series = Series.create(series);
  }

  get series() {
    return this._series;
  }

  get createdAt() {
    return this.model.created_at; // TODO convert to object
  }

  get updatedAt() {
    return this.model.updatedAt; // TODO convert to object
  }
}
