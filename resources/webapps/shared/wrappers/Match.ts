import Round from './Round';
import Team from './Team';
import Venue from './Venue';

export default class Match {
  protected model;

  protected _round;
  protected _venue;
  protected _homeTeam;
  protected _awayTeam;

  constructor(match) {
    this.model = match;

    this.round = this.model.round;
    this.venue = this.model.venue;
    this.homeTeam = this.model.home_team;
    this.awayTeam = this.model.away_team;
  }

  static create(match) {
    return new Match(match);
  }

  get id() {
    return this.model.id;
  }

  set homeTeam(homeTeam) {
    if (!homeTeam) {
      return;
    }

    this._homeTeam = Team.create(homeTeam);
  }

  get homeTeam() {
    return this._homeTeam;
  }

  get homeTeamGoals() {
    return this.model.home_team_goals;
  }

  get homeTeamBehinds() {
    return this.model.home_team_behinds;
  }

  get homeTeamPoints() {
    return this.model.home_team_points;
  }

  get homeTeamScoreLine() {
    if(!this.homeTeamGoals || !this.homeTeamBehinds || !this.homeTeamPoints) {
      return '';
    }

    return `${this.homeTeamGoals}.${this.homeTeamBehinds} (${this.homeTeamPoints})`;
  }

  set awayTeam(awayTeam) {
    if (!awayTeam) {
      return;
    }

    this._awayTeam = Team.create(awayTeam);
  }

  get awayTeam() {
    return this._awayTeam;
  }

  get awayTeamGoals() {
    return this.model.away_team_goals;
  }

  get awayTeamBehinds() {
    return this.model.away_team_behinds;
  }

  get awayTeamPoints() {
    return this.model.away_team_points;
  }

  get awayTeamScoreLine() {
    if(!this.awayTeamGoals || !this.awayTeamBehinds || !this.awayTeamPoints) {
      return '';
    }

    return `${this.awayTeamGoals}.${this.awayTeamBehinds} (${this.awayTeamPoints})`;
  }

  get startTime() {
    return this.model.start_time;
  }

  get attendance() {
    return this.model.attendance;
  }

  get reference() {
    return this.model.reference;
  }

  get name() {
    return this.model.name;
  }

  set round(round) {
    if (!round) {
      return;
    }

    this._round = Round.create(round);
  }

  get round() {
    return this._round;
  }

  set venue(venue) {
    if (!venue) {
      return;
    }

    this._venue = Venue.create(venue);
  }

  get venue() {
    return this._venue;
  }

  get createdAt() {
    return this.model.created_at; // TODO convert to object
  }

  get updatedAt() {
    return this.model.updatedAt; // TODO convert to object
  }

  get hasTeams() {
    return this.homeTeam && this.awayTeam;
  }

  get hasScores() {
    return this.homeTeamPoints && this.awayTeamPoints;
  }

  get resultOneLinerJoiner() {
    if(!this.hasScores) {
      return 'v';
    }

    if(this.homeTeamPoints > this.awayTeamPoints) {
      return 'def';
    }

    return this.homeTeamPoints < this.awayTeamPoints ? 'def by' : 'drew with';
  }

  get teamsResultOneLiner() {
    if(!this.homeTeam || !this.awayTeam) {
      return '-';
    }

    return `${this.homeTeam.shortName} ${this.homeTeamScoreLine} ${this.resultOneLinerJoiner} ${this.awayTeam.shortName} ${this.awayTeamScoreLine}`;
  }
}
