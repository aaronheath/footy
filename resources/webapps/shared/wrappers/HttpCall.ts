import axios from 'axios';
import HttpResponse from './HttpResponse';

export default class HttpCall {
  protected _method: string;

  protected _path: string;

  protected _query: object;

  protected _page: number;

  protected _body: object;

  constructor(method: string, path: string) {
    this._method = method;
    this._path = path;
  }

  static create(method, path) {
    return new HttpCall(method, path);
  }

  query(query) {
    this._query = query;

    return this;
  }

  page(page) {
    this._page = page;

    return this;
  }

  body(body) {
    this._body = body;

    return this;
  }

  get queryAsString() {
    if (!(this._query || this._page)) {
      return false;
    }

    let queryObj = {};

    console.log('_query', this._query)

    if (this._query && Object.keys(this._query).length) {
      queryObj = { ...this._query };
    }

    if (this._page) {
      queryObj = { ...queryObj, ...{ page: this._page } };
    }

    // console.log()

    return (new URLSearchParams(queryObj)).toString();
  }

  get fullPath() {
    return `${this._path}${this.queryAsString ? `?${this.queryAsString}` : ''}`;
  }

  async call() {
    try {
      return new HttpResponse(await axios[this._method](this.fullPath, this._body));
    } catch (error) {
      return new HttpResponse(error.response);
    }
  }
}
