export default class League {
  protected model;

  constructor(league) {
    this.model = league;
  }

  static create(league) {
    return new League(league);
  }

  get id() {
    return this.model.id;
  }

  get abbreviation() {
    return this.model.abbreviation;
  }

  get name() {
    return this.model.name;
  }

  get series() {
    return this.model.series; // TODO use series wrapper
  }

  get teams() {
    return this.model.teams; // TODO use teams wrapper
  }

  get createdAt() {
    return this.model.created_at; // TODO convert to object
  }

  get updatedAt() {
    return this.model.updatedAt; // TODO convert to object
  }
}
