import State from './State';

export default class Venue {
  protected model;

  protected _state;

  constructor(series) {
    this.model = series;

    this.state = this.model.state;
  }

  static create(series) {
    return new Venue(series);
  }

  get id() {
    return this.model.id;
  }

  get name() {
    return this.model.name;
  }

  get abbreviation() {
    return this.model.abbreviation;
  }

  get capacity() {
    return this.model.capacity;
  }

  get latitude() {
    return this.model.latitude;
  }

  get longitude() {
    return this.model.longitude;
  }

  set state(state) {
    if (!state) {
      return;
    }

    this._state = State.create(state);
  }

  get state() {
    return this._state;
  }

  get createdAt() {
    return this.model.created_at; // TODO convert to object
  }

  get updatedAt() {
    return this.model.updatedAt; // TODO convert to object
  }
}
