export default class Season {
  protected model;

  constructor(season) {
    this.model = season;
  }

  static create(season) {
    return new Season(season);
  }

  get id() {
    return this.model.id;
  }

  get year() {
    return this.model.year;
  }

  get createdAt() {
    return this.model.created_at; // TODO convert to object
  }

  get updatedAt() {
    return this.model.updatedAt; // TODO convert to object
  }
}
