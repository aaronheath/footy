import State from './State';
import League from './League';

export default class Team {
  protected model;

  protected _homeState;

  protected _league;

  constructor(series) {
    this.model = series;

    this._homeState = this.model.home_state;
    this.league = this.model.league;
  }

  static create(series) {
    return new Team(series);
  }

  get id() {
    return this.model.id;
  }

  get fullName() {
    return this.model.full_name;
  }

  get shortName() {
    return this.model.short_name;
  }

  get abbreviation() {
    return this.model.abbreviation;
  }

  get primaryColour() {
    return this.model.primary_colour;
  }

  get secondaryColour() {
    return this.model.secondary_colour;
  }

  get tertiaryColour() {
    return this.model.tertiary_colour;
  }

  get address() {
    return this.model.address;
  }

  set homeState(state) {
    if (!state) {
      return;
    }

    this._homeState = State.create(state);
  }

  get homeState() {
    return this._homeState;
  }

  set league(league) {
    if (!league) {
      return;
    }

    this._league = League.create(league);
  }

  get league() {
    return this._league;
  }

  get createdAt() {
    return this.model.created_at; // TODO convert to object
  }

  get updatedAt() {
    return this.model.updatedAt; // TODO convert to object
  }
}
