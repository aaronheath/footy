import League from './League';
import Season from './Season';

export default class Series {
  protected model;

  protected _league;

  protected _season;

  constructor(series) {
    this.model = series;

    this.league = this.model.league;
    this.season = this.model.season;
  }

  static create(series) {
    return new Series(series);
  }

  get id() {
    return this.model.id;
  }

  get name() {
    return this.model.name;
  }

  get pointsForWin() {
    return this.model.points_for_win;
  }

  get percentageDivisor() {
    return this.model.percentage_divisor;
  }

  set league(league) {
    if (!league) {
      return;
    }

    this._league = League.create(league);
  }

  get league() {
    return this._league;
  }

  set season(season) {
    if (!season) {
      return;
    }

    this._season = Season.create(season);
  }

  get season() {
    return this._season;
  }

  get createdAt() {
    return this.model.created_at; // TODO convert to object
  }

  get updatedAt() {
    return this.model.updatedAt; // TODO convert to object
  }
}
