export default class HttpResponse {
  original: any;

  constructor(response) {
    this.original = response;
  }

  get data() {
    return this.original.data.data;
  }

  get isSuccess(): boolean {
    return this.original.status >= 200 && this.original.status <= 299;
  }

  get isError(): boolean {
    return !this.isSuccess;
  }

  get hasMeta() {
    return !!this.meta;
  }

  get meta() {
    return this.original.data.meta;
  }

  get hasPagination() {
    return this.hasMeta && this.meta.current_page;
  }

  get pagination() {
    if (!this.hasPagination) {
      return false;
    }

    return {
      currentPage: this.meta.current_page,
      lastPage: this.meta.last_page,
      currentPageStart: this.meta.from,
      currentPageEnd: this.meta.to,
      perPage: this.meta.per_page,
      total: this.meta.total,
    };
  }
}
