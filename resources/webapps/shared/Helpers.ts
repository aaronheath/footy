export function removeUnchanged(original, current) {
  const filtered = { ...current };

  Object.keys(original).forEach((key) => {
    if (original[key] === filtered[key]) {
      delete filtered[key];
    }
  });

  return filtered;
}

export function transformInputs(omitUnchanged, original, current, nullable = []) {
  const parsed = omitUnchanged ? removeUnchanged(original, current) : { ...current };

  nullable.forEach((key) => {
    if (parsed[key] === '') {
      parsed[key] = null;
    }
  });

  return parsed;
}
