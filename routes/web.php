<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', 'Webapps\PublicController@app')->name('home');
Route::get('/admin', 'Webapps\AdminController@app')->name('admin');

Route::resource('/api/seasons', 'SeasonController')->except(['create', 'edit']);
Route::resource('/api/series', 'SeriesController')->except(['create', 'edit']);
Route::resource('/api/leagues', 'LeagueController')->except(['create', 'edit']);
Route::resource('/api/states', 'StateController')->except(['create', 'edit']);
Route::resource('/api/venues', 'VenueController')->except(['create', 'edit']);
Route::resource('/api/teams', 'TeamController')->except(['create', 'edit']);
Route::resource('/api/rounds', 'RoundController')->except(['create', 'edit']);
Route::resource('/api/matches', 'MatchController')->except(['create', 'edit']);

Route::get('/api/ladders/{series}', 'LadderController@latest');
