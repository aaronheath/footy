<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::resource('seasons', 'SeasonController')->except(['create', 'edit']);
//Route::resource('series', 'SeriesController')->except(['create', 'edit']);
//Route::resource('leagues', 'LeagueController')->except(['create', 'edit']);
//Route::resource('states', 'StateController')->except(['create', 'edit']);
//Route::resource('venues', 'VenueController')->except(['create', 'edit']);
//Route::resource('teams', 'TeamController')->except(['create', 'edit']);
//Route::resource('rounds', 'RoundController')->except(['create', 'edit']);
//Route::resource('matches', 'MatchController')->except(['create', 'edit']);
//
//Route::get('/ladders/{series}', 'LadderController@latest');
