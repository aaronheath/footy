<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->unsignedBigInteger('home_state_id');
            $table->unsignedBigInteger('league_id');
            $table->string('abbreviation', 3);
            $table->string('short_name', 20);
            $table->string('full_name', 50);
            $table->string('primary_colour', 7);
            $table->string('secondary_colour', 7);
            $table->string('tertiary_colour', 7);
            $table->string('address', 100);
            $table->timestamps();

            $table->unique([
                'league_id',
                'abbreviation',
            ]);

            $table->unique([
                'league_id',
                'short_name',
            ]);

            $table->unique([
                'league_id',
                'full_name',
            ]);

            $table->unique([
                'league_id',
                'primary_colour',
                'secondary_colour',
                'tertiary_colour',
            ], 'teams_league_id_team_colors');

            $table->unique([
                'league_id',
                'address',
            ]);

            $table->foreign('home_state_id')
                ->references('id')
                ->on('states');

            $table->foreign('league_id')
                ->references('id')
                ->on('leagues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
