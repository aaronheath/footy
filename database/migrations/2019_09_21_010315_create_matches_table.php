<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->unsignedBigInteger('round_id');
            $table->unsignedBigInteger('venue_id')->nullable();
            $table->unsignedBigInteger('home_team_id')->nullable();
            $table->unsignedTinyInteger('home_team_goals')->nullable();
            $table->unsignedTinyInteger('home_team_behinds')->nullable();
            $table->unsignedSmallInteger('home_team_points')->nullable();
            $table->unsignedBigInteger('away_team_id')->nullable();
            $table->unsignedTinyInteger('away_team_goals')->nullable();
            $table->unsignedTinyInteger('away_team_behinds')->nullable();
            $table->unsignedSmallInteger('away_team_points')->nullable();
            $table->dateTime('start_time')->nullable()->index();
            $table->unsignedMediumInteger('attendance')->nullable()->index();
            $table->string('reference', 20)->unique()->nullable()->index();
            $table->text('name')->nullable();
            $table->timestamps();

            $table->foreign('round_id')
                ->references('id')
                ->on('rounds');

            $table->foreign('venue_id')
                ->references('id')
                ->on('venues');

            $table->foreign('home_team_id')
                ->references('id')
                ->on('teams');

            $table->foreign('away_team_id')
                ->references('id')
                ->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
