<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->unsignedBigInteger('season_id');
            $table->unsignedBigInteger('league_id');
            $table->string('name', 50)->unique();
            $table->string('points_for_win')->default(4);
            $table->string('percentage_divisor')->default('points_against');
            $table->timestamps();

            $table->foreign('season_id')
                ->references('id')
                ->on('seasons');

            $table->foreign('league_id')
                ->references('id')
                ->on('leagues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series');
    }
}
