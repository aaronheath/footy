<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\League;
use Faker\Generator as Faker;

$factory->define(League::class, function (Faker $faker) {
    return [
        'abbreviation' => strtoupper($faker->unique()->lexify('?????')),
        'name' => $faker->unique()->word,
    ];
});
