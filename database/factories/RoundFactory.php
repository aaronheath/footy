<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Round;
use App\Models\Series;
use Faker\Generator as Faker;

$factory->define(Round::class, function (Faker $faker) {
    return [
        'series_id' => function () {
            return factory(Series::class)->create()->id;
        },
        'round' => $faker->unique()->numberBetween(1, 100),
        'name' => $faker->boolean(50) ? $faker->sentence(3) : null,
    ];
});
