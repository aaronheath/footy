<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Match;
use App\Models\Round;
use App\Models\Team;
use App\Models\Venue;
use Faker\Generator as Faker;

$factory->define(Match::class, function (Faker $faker) {
    $homeGoals = $faker->numberBetween(5, 20);
    $homeBehinds = $faker->numberBetween(5, 20);
    $homePoints = $homeGoals * 6 + $homeBehinds;

    $awayGoals = $faker->numberBetween(5, 20);
    $awayBehinds = $faker->numberBetween(5, 20);
    $awayPoints = $awayGoals * 6 + $awayBehinds;

    return [
        'round_id' => function () {
            return factory(Round::class)->create()->id;
        },
        'venue_id' => function () {
            return factory(Venue::class)->create()->id;
        },
        'home_team_id' => function () {
            return factory(Team::class)->create()->id;
        },
        'home_team_goals' => $homeGoals,
        'home_team_behinds' => $homeBehinds,
        'home_team_points' => $homePoints,
        'away_team_id' => function () {
            return factory(Team::class)->create()->id;
        },
        'away_team_goals' => $awayGoals,
        'away_team_behinds' => $awayBehinds,
        'away_team_points' => $awayPoints,
        'start_time' => $faker->dateTime('2030-12-31 23:59:59'),
        'attendance' => $faker->numberBetween(10000, 100000),
    ];
});

$factory->state(Match::class, 'not-played', function ($faker) {
    return [
        'home_team_goals' => null,
        'home_team_behinds' => null,
        'home_team_points' => null,
        'away_team_goals' => null,
        'away_team_behinds' => null,
        'away_team_points' => null,
        'attendance' => null,
    ];
});
