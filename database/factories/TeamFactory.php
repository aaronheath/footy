<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\League;
use App\Models\State;
use App\Models\Team;
use Faker\Generator as Faker;

$factory->define(Team::class, function (Faker $faker) {
    return [
        'home_state_id' => function () {
            return factory(State::class)->create()->id;
        },
        'league_id' => function () {
            return factory(League::class)->create()->id;
        },
        'abbreviation' => $faker->unique()->lexify('???'),
        'short_name' => $faker->unique()->text(20),
        'full_name' => $faker->unique()->text(50),
        'primary_colour' => $faker->unique()->hexcolor,
        'secondary_colour' => $faker->unique()->hexcolor,
        'tertiary_colour' => $faker->unique()->hexcolor,
        'address' => $faker->unique()->address,
    ];
});
