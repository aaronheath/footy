<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\League;
use App\Models\Season;
use App\Models\Series;
use Faker\Generator as Faker;

$factory->define(Series::class, function (Faker $faker) {
    return [
        'season_id' => function () {
            return factory(Season::class)->create()->id;
        },
        'league_id' => function () {
            return factory(League::class)->create()->id;
        },
        'name' => $faker->unique()->text(50),
    ];
});

$factory->state(Series::class, 'sanfl-style', function ($faker) {
    return [
        'points_for_win' => 2,
        'percentage_divisor' => 'total_points',
    ];
});
