<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\State;
use App\Models\Venue;
use Faker\Generator as Faker;

$factory->define(Venue::class, function (Faker $faker) {
    return [
        'state_id' => function () {
            return factory(State::class)->create()->id;
        },
        'abbreviation' => $faker->unique()->lexify('???'),
        'name' => $faker->unique()->sentence(2),
        'capacity' => $faker->numberBetween(10000, 100000),
        'latitude' => $faker->unique()->numberBetween(-90000, 90000) / 1000,
        'longitude' => $faker->unique()->numberBetween(-90000, 90000) / 1000,
    ];
});
