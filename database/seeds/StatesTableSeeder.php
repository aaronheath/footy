<?php

use App\Models\State;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        State::create([
            'abbreviation' => 'SA',
            'name' => 'South Australia',
            'timezone' => 'Australia/Adelaide',
        ]);

        State::create([
            'abbreviation' => 'QLD',
            'name' => 'Queensland',
            'timezone' => 'Australia/Brisbane',
        ]);

        State::create([
            'abbreviation' => 'NSW',
            'name' => 'New South Wales',
            'timezone' => 'Australia/Sydney',
        ]);

        State::create([
            'abbreviation' => 'ACT',
            'name' => 'Australian Capital Territory',
            'timezone' => 'Australia/Sydney',
        ]);

        State::create([
            'abbreviation' => 'VIC',
            'name' => 'Victoria',
            'timezone' => 'Australia/Melbourne',
        ]);

        State::create([
            'abbreviation' => 'TAS',
            'name' => 'Tasmania',
            'timezone' => 'Australia/Hobart',
        ]);

        State::create([
            'abbreviation' => 'WA',
            'name' => 'Western Australia',
            'timezone' => 'Australia/Perth',
        ]);

        State::create([
            'abbreviation' => 'NT',
            'name' => 'Northern Territory',
            'timezone' => 'Australia/Darwin',
        ]);

        State::create([
            'abbreviation' => 'CN',
            'name' => 'China',
            'timezone' => 'Asia/Shanghai',
        ]);
    }
}
