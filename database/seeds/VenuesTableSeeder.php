<?php

use App\Models\State;
use App\Models\Venue;
use Illuminate\Database\Seeder;

class VenuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Venue::create([
            'state_id' => State::whereAbbreviation('SA')->first()->id,
            'abbreviation' => 'AO',
            'name' => 'Adelaide Oval',
            'capacity' => 55317,
            'latitude' => -34.9157151,
            'longitude' => 138.5958033,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('QLD')->first()->id,
            'abbreviation' => 'G',
            'name' => 'Brisbane Cricket Ground',
            'capacity' => 41974,
            'latitude' => -27.4858868,
            'longitude' => 153.037333,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('QLD')->first()->id,
            'abbreviation' => 'MS',
            'name' => 'Metricon Stadium',
            'capacity' => 25000,
            'latitude' => -28.0062787,
            'longitude' => 153.3663465,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('NSW')->first()->id,
            'abbreviation' => 'SCG',
            'name' => 'Sydney Cricket Ground',
            'capacity' => 48000,
            'latitude' => -33.8917589,
            'longitude' => 151.2244916,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('NSW')->first()->id,
            'abbreviation' => 'SSGS',
            'name' => 'GIANTS Stadium',
            'capacity' => 24000,
            'latitude' => -33.8431521,
            'longitude' => 151.0670015,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('ACT')->first()->id,
            'abbreviation' => 'UNSW',
            'name' => 'UNSW Canberra Oval',
            'capacity' => 16000,
            'latitude' => -35.3181675,
            'longitude' => 149.1342262,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('VIC')->first()->id,
            'abbreviation' => 'MCG',
            'name' => 'Melbourne Cricket Ground',
            'capacity' => 100024,
            'latitude' => -37.8199952,
            'longitude' => 144.9829286,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('VIC')->first()->id,
            'abbreviation' => 'MRVL',
            'name' => 'Marvel Stadium',
            'capacity' => 56347,
            'latitude' => -37.8165756,
            'longitude' => 144.9469625,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('VIC')->first()->id,
            'abbreviation' => 'GS',
            'name' => 'GMHBA Stadium',
            'capacity' => 36000,
            'latitude' => -38.1581188,
            'longitude' => 144.354165,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('TAS')->first()->id,
            'abbreviation' => 'BA',
            'name' => 'Blundstone Arena',
            'capacity' => 19500,
            'latitude' => -42.877333,
            'longitude' => 147.3734011,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('TAS')->first()->id,
            'abbreviation' => 'UT',
            'name' => 'University of Tasmania Stadium',
            'capacity' => 21000,
            'latitude' => -41.4260369,
            'longitude' => 147.1368021,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('WA')->first()->id,
            'abbreviation' => 'OS',
            'name' => 'Optus Stadium',
            'capacity' => 60000,
            'latitude' => -31.9511981,
            'longitude' => 115.88845,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('NT')->first()->id,
            'abbreviation' => 'TP',
            'name' => 'TIO Traeger Park',
            'capacity' => 7164,
            'latitude' => -23.709795,
            'longitude' => 133.875940,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('NT')->first()->id,
            'abbreviation' => 'TIO',
            'name' => 'TIO Stadium',
            'capacity' => 12500,
            'latitude' => -12.399183,
            'longitude' => 130.887276,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('VIC')->first()->id,
            'abbreviation' => 'MARS',
            'name' => 'Mars Stadium',
            'capacity' => 11000,
            'latitude' => -37.539346,
            'longitude' => 143.848113,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('QLD')->first()->id,
            'abbreviation' => 'RS',
            'name' => 'Riverway Stadium',
            'capacity' => 10000,
            'latitude' => -19.3176357,
            'longitude' => 146.7313496,
        ]);

        Venue::create([
            'state_id' => State::whereAbbreviation('CN')->first()->id,
            'abbreviation' => 'AAJS',
            'name' => 'Jiangwan Stadium',
            'capacity' => 11000,
            'latitude' => 31.308458,
            'longitude' => 121.511116,
        ]);
    }
}
