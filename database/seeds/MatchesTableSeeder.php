<?php

use App\Models\Match;
use App\Models\Round;
use App\Models\Series;
use Illuminate\Database\Seeder;

class MatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aflFinalsSeriesId = Series::whereName('2020 AFL Finals Series')->first()->id;

        Match::create([
            'round_id' => Round::whereSeriesId($aflFinalsSeriesId)->whereRound('Week 1')->first()->id,
            'reference' => '2020_AFL_W1_Q1',
            'name' => 'First Qualifying Final',
        ]);

        Match::create([
            'round_id' => Round::whereSeriesId($aflFinalsSeriesId)->whereRound('Week 1')->first()->id,
            'reference' => '2020_AFL_W1_Q2',
            'name' => 'Second Qualifying Final',
        ]);

        Match::create([
            'round_id' => Round::whereSeriesId($aflFinalsSeriesId)->whereRound('Week 1')->first()->id,
            'reference' => '2020_AFL_W1_E1',
            'name' => 'First Elimination Final',
        ]);

        Match::create([
            'round_id' => Round::whereSeriesId($aflFinalsSeriesId)->whereRound('Week 1')->first()->id,
            'reference' => '2020_AFL_W1_E2',
            'name' => 'Second Elimination Final',
        ]);

        Match::create([
            'round_id' => Round::whereSeriesId($aflFinalsSeriesId)->whereRound('Week 2')->first()->id,
            'reference' => '2020_AFL_W2_SF1',
            'name' => 'First Semi Final',
        ]);

        Match::create([
            'round_id' => Round::whereSeriesId($aflFinalsSeriesId)->whereRound('Week 2')->first()->id,
            'reference' => '2020_AFL_W2_SF2',
            'name' => 'Second Semi Final',
        ]);

        Match::create([
            'round_id' => Round::whereSeriesId($aflFinalsSeriesId)->whereRound('Week 3')->first()->id,
            'reference' => '2020_AFL_W3_PF1',
            'name' => 'First Preliminary Final',
        ]);

        Match::create([
            'round_id' => Round::whereSeriesId($aflFinalsSeriesId)->whereRound('Week 3')->first()->id,
            'reference' => '2020_AFL_W3_PF2',
            'name' => 'Second Preliminary Final',
        ]);

        Match::create([
            'round_id' => Round::whereSeriesId($aflFinalsSeriesId)->whereRound('Week 4')->first()->id,
            'reference' => '2020_AFL_W4_GF',
            'name' => 'Grand Final',
        ]);
    }
}
