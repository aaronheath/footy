<?php

use App\Models\League;
use App\Models\State;
use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::create([
            'home_state_id' => State::whereAbbreviation('SA')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'ADL',
            'short_name' => 'Adelaide',
            'full_name' => 'Adelaide FC',
            'primary_colour' => '#004B8D',
            'secondary_colour' => '#E11937',
            'tertiary_colour' => '#FDC307',
            'address' => '105 W Lakes Blvd, West Lakes SA 5021',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('QLD')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'BL',
            'short_name' => 'Brisbane',
            'full_name' => 'Brisbane Lions FC',
            'primary_colour' => '#8B0042',
            'secondary_colour' => '#EDC157',
            'tertiary_colour' => '#000F2F',
            'address' => '2/812 Stanley St, Woolloongabba QLD 4102',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('VIC')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'CAR',
            'short_name' => 'Carlton',
            'full_name' => 'Carlton FC',
            'primary_colour' => '#031E2F',
            'secondary_colour' => '#FFFFFF',
            'tertiary_colour' => '#031A29',
            'address' => 'Ikon Park, Royal Parade, Carlton North VIC 3054',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('VIC')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'COL',
            'short_name' => 'Collingwood',
            'full_name' => 'Collingwood FC',
            'primary_colour' => '#000000',
            'secondary_colour' => '#FFFFFF',
            'tertiary_colour' => '#AB9767',
            'address' => 'Holden Centre Olympic Blvd &, Batman Ave, Olympic Park VIC 3000',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('VIC')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'ESS',
            'short_name' => 'Essendon',
            'full_name' => 'Essendon FC',
            'primary_colour' => '#000000',
            'secondary_colour' => '#C90427',
            'tertiary_colour' => '#D1D1D1',
            'address' => '275 Melrose Dr, Melbourne Airport VIC 3045',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('WA')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'FRE',
            'short_name' => 'Fremantle',
            'full_name' => 'Fremantle FC',
            'primary_colour' => '#331C54',
            'secondary_colour' => '#FFFFFF',
            'tertiary_colour' => '#331C54',
            'address' => '31 Veterans Parade, Cockburn Central WA 6164',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('VIC')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'GEE',
            'short_name' => 'Geelong',
            'full_name' => 'Geelong FC',
            'primary_colour' => '#002244',
            'secondary_colour' => '#FFFFFF',
            'tertiary_colour' => '#001E3C',
            'address' => 'Simonds Stadium Kardinia Park, Latrobe Terrace, Geelong VIC 3220',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('QLD')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'GC',
            'short_name' => 'Gold Coast',
            'full_name' => 'Gold Coast Suns FC',
            'primary_colour' => '#E0320F',
            'secondary_colour' => '#FCD801',
            'tertiary_colour' => '#D12B1F',
            'address' => 'Austworld Centre, 296 Nerang Broadbeach Rd, Carrara QLD 4211',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('NSW')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'GWS',
            'short_name' => 'GWS Giants',
            'full_name' => 'Greater Western Sydney Giants FC',
            'primary_colour' => '#FF7901',
            'secondary_colour' => '#FFFFFF',
            'tertiary_colour' => '#FF7901',
            'address' => '1 Olympic Blvd, Sydney Olympic Park NSW 2127',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('VIC')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'HAW',
            'short_name' => 'Hawthorn',
            'full_name' => 'Hawthorn FC',
            'primary_colour' => '#FFC402',
            'secondary_colour' => '#562500',
            'tertiary_colour' => '#FFC402',
            'address' => '3/2 Stadium Circuit, Mulgrave VIC 3170',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('VIC')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'MEL',
            'short_name' => 'Melbourne',
            'full_name' => 'Melbourne FC',
            'primary_colour' => '#DE0317',
            'secondary_colour' => '#061A33',
            'tertiary_colour' => '#DE0317',
            'address' => '120 Brunton Ave, East Melbourne VIC 3002',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('VIC')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'NM',
            'short_name' => 'North Melbourne',
            'full_name' => 'North Melbourne FC',
            'primary_colour' => '#003CA0',
            'secondary_colour' => '#FFFFFF',
            'tertiary_colour' => '#003CA0',
            'address' => '204-206 Arden St, North Melbourne VIC 3051',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('SA')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'PA',
            'short_name' => 'Port Adelaide',
            'full_name' => 'Port Adelaide FC',
            'primary_colour' => '#008AAB',
            'secondary_colour' => '#FFFFFF',
            'tertiary_colour' => '#000000',
            'address' => 'Brougham Pl, Alberton SA 5014',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('VIC')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'RIC',
            'short_name' => 'Richmond',
            'full_name' => 'Richmond FC',
            'primary_colour' => '#000000',
            'secondary_colour' => '#FFD200',
            'tertiary_colour' => '#FFFFFF',
            'address' => 'Swinburne Centre, Punt Rd, Richmond VIC 3121',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('VIC')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'STK',
            'short_name' => 'St Kilda',
            'full_name' => 'St Kilda FC',
            'primary_colour' => '#DF0134',
            'secondary_colour' => '#FFFFFF',
            'tertiary_colour' => '#000000',
            'address' => '32/60 Linton St, Moorabbin VIC 3189',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('NSW')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'SYD',
            'short_name' => 'Sydney',
            'full_name' => 'Sydney Swans FC',
            'primary_colour' => '#EE1F31',
            'secondary_colour' => '#FFFFFF',
            'tertiary_colour' => '#EE1F31',
            'address' => 'Driver Ave, Moore Park NSW 2021',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('WA')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'WC',
            'short_name' => 'West Coast',
            'full_name' => 'West Coast Eagles FC',
            'primary_colour' => '#002A77',
            'secondary_colour' => '#F2A901',
            'tertiary_colour' => '#003087',
            'address' => 'Mineral Resources Park, 42 Bishopsgate St, Lathlain WA 6100',
        ]);

        Team::create([
            'home_state_id' => State::whereAbbreviation('VIC')->first()->id,
            'league_id' => League::whereAbbreviation('AFL')->first()->id,
            'abbreviation' => 'WB',
            'short_name' => 'Western Bulldogs',
            'full_name' => 'Western Bulldogs FC',
            'primary_colour' => '#C60C30',
            'secondary_colour' => '#0039A6',
            'tertiary_colour' => '#FFFFFF',
            'address' => '417 Barkly St, Footscray VIC 3011',
        ]);

//        Team::create([
//            'home_state_id' => State::whereAbbreviation('')->first()->id,
//            'league_id' => League::whereAbbreviation('AFL')->first()->id,
//            'abbreviation' => '',
//            'short_name' => '',
//            'full_name' => '',
//            'primary_colour' => '#',
//            'secondary_colour' => '#',
//            'tertiary_colour' => '#',
//            'address' => '',
//        ]);
    }
}
