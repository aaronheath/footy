<?php

use App\Models\League;
use App\Models\Season;
use App\Models\Series;
use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $season = Season::where('year', '2020')->first();

        $afl = League::whereAbbreviation('AFL')->first();
        $vfl = League::whereAbbreviation('VFL')->first();
        $sanfl = League::whereAbbreviation('SANFL')->first();

        Series::create([
            'season_id' => $season->id,
            'league_id' => $afl->id,
            'name' => '2020 AFL Pre-Season',
        ]);

        Series::create([
            'season_id' => $season->id,
            'league_id' => $afl->id,
            'name' => '2020 AFL Premiership Season',
        ]);

        Series::create([
            'season_id' => $season->id,
            'league_id' => $afl->id,
            'name' => '2020 AFL Finals Series',
        ]);

        Series::create([
            'season_id' => $season->id,
            'league_id' => $vfl->id,
            'name' => '2020 VFL Premiership Season',
        ]);

        Series::create([
            'season_id' => $season->id,
            'league_id' => $vfl->id,
            'name' => '2020 VFL Finals Series',
        ]);

        Series::create([
            'season_id' => $season->id,
            'league_id' => $sanfl->id,
            'name' => '2020 SANFL Premiership Season',
        ]);

        Series::create([
            'season_id' => $season->id,
            'league_id' => $sanfl->id,
            'name' => '2020 SANFL Finals Series',
        ]);
    }
}
