<?php

use App\Models\Round;
use App\Models\Series;
use Illuminate\Database\Seeder;

class RoundsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aflPremiershipSeriesId = Series::whereName('2020 AFL Premiership Season')->first()->id;
        $aflFinalsSeriesId = Series::whereName('2020 AFL Finals Series')->first()->id;

        $round = 1;

        while ($round <= 23) {
            Round::create([
                'series_id' => $aflPremiershipSeriesId,
                'round' => $round,
            ]);

            $round++;
        }

        Round::create([
            'series_id' => $aflFinalsSeriesId,
            'round' => 'Week 1',
            'name' => 'Qualifying and Elimination Finals',
        ]);

        Round::create([
            'series_id' => $aflFinalsSeriesId,
            'round' => 'Week 2',
            'name' => 'Semi Finals',
        ]);

        Round::create([
            'series_id' => $aflFinalsSeriesId,
            'round' => 'Week 3',
            'name' => 'Preliminary Finals',
        ]);

        Round::create([
            'series_id' => $aflFinalsSeriesId,
            'round' => 'Week 4',
            'name' => 'Grand Final',
        ]);
    }
}
