<?php

use App\Models\League;
use Illuminate\Database\Seeder;

class LeaguesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        League::create([
            'abbreviation' => 'AFL',
            'name' => 'Australian Football League',
        ]);

        League::create([
            'abbreviation' => 'VFL',
            'name' => 'Victorian Football League',
        ]);

        League::create([
            'abbreviation' => 'SANFL',
            'name' => 'South Australian National Football League',
        ]);
    }
}
