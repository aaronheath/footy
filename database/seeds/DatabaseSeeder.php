<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
             UsersTableSeeder::class,
             LeaguesTableSeeder::class,
             SeasonsTableSeeder::class,
             SeriesTableSeeder::class,
             StatesTableSeeder::class,
             VenuesTableSeeder::class,
             TeamsTableSeeder::class,
             RoundsTableSeeder::class,
             MatchesTableSeeder::class,
         ]);
    }
}
