# docker build -t registry.gitlab.com/aaronheath/footy .
# docker push registry.gitlab.com/aaronheath/footy

FROM php:7.4

RUN apt-get update

RUN apt-get install -qq git curl libmcrypt-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev libcurl4-gnutls-dev \
    libxml2-dev zlib1g-dev gnupg2 mariadb-client librrds-perl libzip-dev libonig-dev

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs

RUN docker-php-ext-install bcmath ctype curl json mbstring pdo_mysql tokenizer xml zip

RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
