module.exports = {
    "root": true,
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "plugin:vue/essential",
        "airbnb-base"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "parser": "@typescript-eslint/parser",
        "sourceType": "module"
    },
    "plugins": [
        "vue",
        "@typescript-eslint"
    ],
    "rules": {
      "consistent-return": [
        "error",
        {
          "treatUndefinedAsUnspecified": true,
        }
      ],
      "import/extensions": [
        "error",
        "never"
      ],
      "import/no-extraneous-dependencies": [
        "error",
        {
            "devDependencies": true,
            "optionalDependencies": false,
            "peerDependencies": false,
        },
      ],
      "import/no-unresolved": "off",
      "max-len": "off",
      "no-underscore-dangle": "off",
      "vue/max-attributes-per-line": [
        "error",
        {
          "singleline": 1,
          "multiline": {
            "max": 1,
            "allowFirstLine": false
          }
        }
      ],
      "vue/max-len": [
        "error",
        {
          "code": 120,
          "template": 999,
        }
      ]
    },
};
