module.exports = {
  theme: {
    extend: {},
  },
  variants: {
    backgroundColor: ['odd'],
    cursor: ['disabled'],
    opacity: ['disabled'],
  },
  plugins: [
    require('@tailwindcss/ui')
  ],
}
