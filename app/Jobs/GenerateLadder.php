<?php

namespace App\Jobs;

use App\Actions\Ladder;
use App\Models\Series;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateLadder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $series;

    public function __construct(Series $series)
    {
        $this->series = $series;
    }

    public function handle()
    {
        logger()->info('Requesting ladder generation.', [
            'series_id' => $this->series->id,
            'series_name' => $this->series->name,
        ]);

        app(Ladder::class)->series($this->series)->cache()->generate();
    }
}
