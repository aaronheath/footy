<?php

namespace App\Models;

use App\Traits\HasUuid;

class Venue extends Model
{
    use HasUuid;

    protected $dependants = [
        'matches',
    ];

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function matches()
    {
        return $this->hasMany(Match::class);
    }

    public function getLatitudeAttribute($value)
    {
        return $this->formatCoordinate($value);
    }

    public function getLongitudeAttribute($value)
    {
        return $this->formatCoordinate($value);
    }

    protected function formatCoordinate($value)
    {
        return (float) rtrim(rtrim($value, '0'), '.');
    }
}
