<?php

namespace App\Models;

use App\Exceptions\UnableToDeleteModelWithRelationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    protected $guarded = [];

    protected $dependants = [];

    public function hasDependants()
    {
        $this->loadMissing($this->dependants);

        return (bool) collect($this->dependants)
            ->first(function ($relation) {
                $relationValue = $this->$relation;

                if (is_null($relationValue)) {
                    return false;
                }

                if ($relationValue instanceof Collection && $relationValue->isEmpty()) {
                    return false;
                }

                return true;
            });
    }

    public function hasNoDependants()
    {
        return ! $this->hasDependants();
    }

    public function delete($force = false)
    {
        if (! $force && $this->hasDependants()) {
            throw new UnableToDeleteModelWithRelationException(
                sprintf(
                    'Unable to delete %s as it has active dependants.',
                    strtolower(class_basename(get_class($this)))
                )
            );
        }

        return parent::delete();
    }

    public static function idForUuid($uuid)
    {
        return optional(self::whereUuid($uuid)->first())->id;
    }
}
