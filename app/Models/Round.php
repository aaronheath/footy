<?php

namespace App\Models;

use App\Traits\HasUuid;

class Round extends Model
{
    use HasUuid;

    protected $dependants = [
        'matches',
    ];

    protected $casts = [
        'round' => 'string',
    ];

    public function series()
    {
        return $this->belongsTo(Series::class);
    }

    public function matches()
    {
        return $this->hasMany(Match::class);
    }
}
