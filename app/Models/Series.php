<?php

namespace App\Models;

use App\Traits\HasUuid;

class Series extends Model
{
    use HasUuid;

    protected $casts = [
        'points_for_win' => 'integer',
    ];

    protected $dependants = [
        'rounds',
    ];

    public function league()
    {
        return $this->belongsTo(League::class);
    }

    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    public function rounds()
    {
        return $this->hasMany(Round::class);
    }

    public function scopeLeague($query, $leagueUuid)
    {
        return $query->whereHas('league', function($query) use ($leagueUuid) {
            $query->whereUuid($leagueUuid);
        });
    }
}
