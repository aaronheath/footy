<?php

namespace App\Models;

use App\Traits\HasUuid;

class Season extends Model
{
    use HasUuid;

    protected $dependants = [
        'series',
    ];

    public function series()
    {
        return $this->hasMany(Series::class);
    }
}
