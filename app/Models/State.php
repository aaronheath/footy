<?php

namespace App\Models;

use App\Traits\HasUuid;

class State extends Model
{
    use HasUuid;

    protected $dependants = [
        'venues',
        'homeTeams',
    ];

    public function venues()
    {
        return $this->hasMany(Venue::class);
    }

    public function homeTeams()
    {
        return $this->hasMany(Team::class, 'home_state_id');
    }
}
