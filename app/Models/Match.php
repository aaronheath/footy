<?php

namespace App\Models;

use App\Traits\HasUuid;
use Webpatser\Uuid\Uuid;

class Match extends Model
{
    use HasUuid;

    protected $dependants = [
//        'matches',
    ];

    protected $casts = [
        'start_time' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });

        static::saving(function($model) {
            [$homePoints, $awayPoints] = self::matchPoints($model);

            $model->home_team_points = $homePoints;
            $model->away_team_points = $awayPoints;
        });
    }

    public function round()
    {
        return $this->belongsTo(Round::class);
    }

    public function venue()
    {
        return $this->belongsTo(Venue::class);
    }

    public function homeTeam()
    {
        return $this->belongsTo(Team::class, 'home_team_id');
    }

    public function awayTeam()
    {
        return $this->belongsTo(Team::class, 'away_team_id');
    }

    public function getSeriesAttribute()
    {
        return $this->round->series;
    }

    public static function totalPoints($goals, $behinds)
    {
        if (is_null($goals) || is_null($behinds)) {
            return;
        }

        return $goals * 6 + $behinds;
    }

    protected static function matchPoints($match)
    {
        $homePoints = self::totalPoints(
            $match->home_team_goals ?? null,
            $match->home_team_behinds ?? null
        );

        $awayPoints = self::totalPoints(
            $match->away_team_goals ?? null,
            $match->away_team_behinds ?? null
        );

        if(is_null($homePoints) || is_null($awayPoints)) {
            return [null, null];
        }

        return [$homePoints, $awayPoints];
    }
}
