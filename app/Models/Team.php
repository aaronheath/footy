<?php

namespace App\Models;

use App\Traits\HasUuid;

class Team extends Model
{
    use HasUuid;

    protected $dependants = [
        'homeMatches',
        'awayMatches',
    ];

    public function homeState()
    {
        return $this->belongsTo(State::class, 'home_state_id');
    }

    public function league()
    {
        return $this->belongsTo(League::class);
    }

    public function homeMatches()
    {
        return $this->hasMany(Match::class, 'home_team_id');
    }

    public function awayMatches()
    {
        return $this->hasMany(Match::class, 'away_team_id');
    }
}
