<?php

namespace App\Models;

use App\Traits\HasUuid;

class League extends Model
{
    use HasUuid;

    protected $dependants = [
        'series',
        'teams',
    ];

    public function series()
    {
        return $this->hasMany(Series::class);
    }

    public function teams()
    {
        return $this->hasMany(Team::class);
    }
}
