<?php

namespace App\Actions;

use App\Models\Match;
use App\Models\Series;
use App\Models\Team;

class Ladder
{
    protected $ladder;

    protected $series;

    protected $cache = false;

    public function __construct()
    {
        $this->ladder = collect();
    }

    public function series(Series $series)
    {
        $this->series = $series;

        return $this;
    }

    public function cache()
    {
        $this->cache = true;

        return $this;
    }

    public function pointForWin()
    {
        return $this->series->points_for_win;
    }

    public function pointsForDraw()
    {
        return $this->pointForWin() / 2;
    }

    public function percentageDivisor()
    {
        return $this->series->percentage_divisor;
    }

    public function generate()
    {
        if (! $this->series instanceof Series) {
            throw new \Exception('Series not defined');
        }

        $rounds = $this->series->rounds->sortBy(function ($round) {
            return (int) $round->round;
        });

        $rounds->each(function ($round) {
            $round = $round->load('matches');

            $round->matches->each(function ($match) {
                $this->addMatch($match);
            });
        });

        $this->sort();

        $this->cacheIfRequested();

        logger()->info('Ladder has been generated.', [
            'series_id' => $this->series->id,
            'series_name' => $this->series->name,
        ]);

        return $this->ladder;
    }

    protected function addMatch(Match $match)
    {
        if (is_null($match->homeTeam) || is_null($match->awayTeam)) {
            return;
        }

        $homeTeamRecord = $this->teamInLadder($match->homeTeam);
        $awayTeamRecord = $this->teamInLadder($match->awayTeam);

        if (is_null($homeTeamRecord)) {
            $homeTeamRecord = $this->blankLadderRecord($match->homeTeam);
            $this->ladder->push($homeTeamRecord);
        }

        if (is_null($awayTeamRecord)) {
            $awayTeamRecord = $this->blankLadderRecord($match->awayTeam);
            $this->ladder->push($awayTeamRecord);
        }

        // If match is not yet played we can bail out however if team is yet to be seeded into ladder then ensure we add it.
        if (is_null($match->home_team_points) || is_null($match->away_team_points)) {
            $this->replaceLadderRecord($homeTeamRecord);
            $this->replaceLadderRecord($awayTeamRecord);

            return;
        }

        $homeTeamRecord['played']++;
        $awayTeamRecord['played']++;

        if ($match->home_team_points > $match->away_team_points) {
            $homeTeamRecord['won']++;
            $awayTeamRecord['lost']++;
        }

        if ($match->home_team_points < $match->away_team_points) {
            $homeTeamRecord['lost']++;
            $awayTeamRecord['won']++;
        }

        if ($match->home_team_points === $match->away_team_points) {
            $homeTeamRecord['drawn']++;
            $awayTeamRecord['drawn']++;
        }

        $homeTeamRecord['goals_for'] = $homeTeamRecord['goals_for'] + $match->home_team_goals;
        $homeTeamRecord['behinds_for'] = $homeTeamRecord['behinds_for'] + $match->home_team_behinds;
        $homeTeamRecord['points_for'] = $homeTeamRecord['goals_for'] * 6 + $homeTeamRecord['behinds_for'];

        $homeTeamRecord['goals_against'] = $homeTeamRecord['goals_against'] + $match->away_team_goals;
        $homeTeamRecord['behinds_against'] = $homeTeamRecord['behinds_against'] + $match->away_team_behinds;
        $homeTeamRecord['points_against'] = $homeTeamRecord['goals_against'] * 6 + $homeTeamRecord['behinds_against'];

        $awayTeamRecord['goals_for'] = $awayTeamRecord['goals_for'] + $match->away_team_goals;
        $awayTeamRecord['behinds_for'] = $awayTeamRecord['behinds_for'] + $match->away_team_behinds;
        $awayTeamRecord['points_for'] = $awayTeamRecord['goals_for'] * 6 + $awayTeamRecord['behinds_for'];

        $awayTeamRecord['goals_against'] = $awayTeamRecord['goals_against'] + $match->home_team_goals;
        $awayTeamRecord['behinds_against'] = $awayTeamRecord['behinds_against'] + $match->home_team_behinds;
        $awayTeamRecord['points_against'] = $awayTeamRecord['goals_against'] * 6 + $awayTeamRecord['behinds_against'];

        $homeTeamRecord['percentage'] = $this->percentage($homeTeamRecord['points_for'], $homeTeamRecord['points_against']);
        $awayTeamRecord['percentage'] = $this->percentage($awayTeamRecord['points_for'], $awayTeamRecord['points_against']);

        $homeTeamRecord['points'] = $this->premiershipPoints($homeTeamRecord['won'], $homeTeamRecord['drawn']);
        $awayTeamRecord['points'] = $this->premiershipPoints($awayTeamRecord['won'], $awayTeamRecord['drawn']);

        $this->replaceLadderRecord($homeTeamRecord);
        $this->replaceLadderRecord($awayTeamRecord);
    }

    protected function teamInLadder($team)
    {
        return collect($this->ladder)->first(function ($record) use ($team) {
            return $record['team']->id == $team->id;
        });
    }

    protected function blankLadderRecord(Team $team)
    {
        return [
            'team' => $team,
            'played' => 0,
            'won' => 0,
            'drawn' => 0,
            'lost' => 0,
            'points' => 0,
            'percentage' => 0,
            'goals_for' => 0,
            'behinds_for' => 0,
            'points_for' => 0,
            'goals_against' => 0,
            'behinds_against' => 0,
            'points_against' => 0,
        ];
    }

    protected function replaceLadderRecord($record)
    {
        $this->ladder = $this->ladder->map(function ($ladderRecord) use ($record) {
            return $ladderRecord['team']->id == $record['team']->id ? $record : $ladderRecord;
        });
    }

    protected function premiershipPoints($wins, $draws)
    {
        return $wins * $this->pointForWin() + $draws * $this->pointsForDraw();
    }

    protected function percentage($pointsFor, $pointsAgainst)
    {
        $divisor = $this->percentageDivisor();

        if ($divisor == 'points_against') {
            return round(($pointsFor / $pointsAgainst) * 100, 5);
        }

        if ($divisor == 'total_points') {
            return round(($pointsFor / ($pointsFor + $pointsAgainst)) * 100, 5);
        }

        throw new \Exception('Unknown percentage divisor - '.$divisor);
    }

    protected function sort()
    {
        $this->ladder = $this->ladder
            ->sortBy(function ($record) {
                $exploded = explode('.', (string) $record['percentage']);

                $int = str_pad($exploded[0], 6, 0, STR_PAD_LEFT);
                $decimals = isset($exploded[1]) ? str_pad($exploded[1], 5, 0) : '00000';

                return sprintf(
                    '%s_%s_%s',
                    str_pad($record['points'], 5, 0, STR_PAD_LEFT),
                    $int.'.'.$decimals,
                    $record['team']->full_name
                );
            }, SORT_REGULAR, true)
            ->values();
    }

    protected function cacheIfRequested()
    {
        if (! $this->cache) {
            return;
        }

        cache()->put($this->cacheKey(), $this->ladder);
    }

    protected function cacheKey()
    {
        return 'ladder_'.$this->series->uuid;
    }
}
