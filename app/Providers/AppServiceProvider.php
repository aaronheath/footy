<?php

namespace App\Providers;

use App\Models\League;
use App\Models\Match;
use App\Models\Round;
use App\Models\Season;
use App\Models\Series;
use App\Models\State;
use App\Models\Team;
use App\Models\User;
use App\Models\Venue;
use App\Observers\LeagueObserver;
use App\Observers\MatchObserver;
use App\Observers\RoundObserver;
use App\Observers\SeasonObserver;
use App\Observers\SeriesObserver;
use App\Observers\StateObserver;
use App\Observers\TeamObserver;
use App\Observers\UserObserver;
use App\Observers\VenueObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->registerModelObservers();
    }

    protected function registerModelObservers()
    {
        League::observe(LeagueObserver::class);
        Match::observe(MatchObserver::class);
        Round::observe(RoundObserver::class);
        Season::observe(SeasonObserver::class);
        Series::observe(SeriesObserver::class);
        State::observe(StateObserver::class);
        Team::observe(TeamObserver::class);
        User::observe(UserObserver::class);
        Venue::observe(VenueObserver::class);
    }
}
