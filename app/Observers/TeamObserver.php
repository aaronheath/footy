<?php

namespace App\Observers;

use App\Models\Team;
use App\Traits\ObserverHelper;

class TeamObserver
{
    use ObserverHelper;

    public function created(Team $team)
    {
        $this->logCreated($team);
    }

    public function updated(Team $team)
    {
        $this->logUpdated($team);
    }

    public function deleted(Team $team)
    {
        $this->logDeleted($team);
    }

    public function restored(Team $team)
    {
        $this->logRestored($team);
    }

    public function forceDeleted(Team $team)
    {
        $this->logForceDeleted($team);
    }
}
