<?php

namespace App\Observers;

use App\Models\Round;
use App\Traits\ObserverHelper;

class RoundObserver
{
    use ObserverHelper;

    public function created(Round $round)
    {
        $this->logCreated($round);
    }

    public function updated(Round $round)
    {
        $this->logUpdated($round);
    }

    public function deleted(Round $round)
    {
        $this->logDeleted($round);
    }

    public function restored(Round $round)
    {
        $this->logRestored($round);
    }

    public function forceDeleted(Round $round)
    {
        $this->logForceDeleted($round);
    }
}
