<?php

namespace App\Observers;

use App\Models\User;
use App\Traits\ObserverHelper;

class UserObserver
{
    use ObserverHelper;

    public function created(User $user)
    {
        $this->logCreated($user);
    }

    public function updated(User $user)
    {
        $this->logUpdated($user);
    }

    public function deleted(User $user)
    {
        $this->logDeleted($user);
    }

    public function restored(User $user)
    {
        $this->logRestored($user);
    }

    public function forceDeleted(User $user)
    {
        $this->logForceDeleted($user);
    }
}
