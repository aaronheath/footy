<?php

namespace App\Observers;

use App\Models\Season;
use App\Traits\ObserverHelper;

class SeasonObserver
{
    use ObserverHelper;

    public function created(Season $season)
    {
        $this->logCreated($season);
    }

    public function updated(Season $season)
    {
        $this->logUpdated($season);
    }

    public function deleted(Season $season)
    {
        $this->logDeleted($season);
    }

    public function restored(Season $season)
    {
        $this->logRestored($season);
    }

    public function forceDeleted(Season $season)
    {
        $this->logForceDeleted($season);
    }
}
