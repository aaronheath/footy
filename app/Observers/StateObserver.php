<?php

namespace App\Observers;

use App\Models\State;
use App\Traits\ObserverHelper;

class StateObserver
{
    use ObserverHelper;

    public function created(State $state)
    {
        $this->logCreated($state);
    }

    public function updated(State $state)
    {
        $this->logUpdated($state);
    }

    public function deleted(State $state)
    {
        $this->logDeleted($state);
    }

    public function restored(State $state)
    {
        $this->logRestored($state);
    }

    public function forceDeleted(State $state)
    {
        $this->logForceDeleted($state);
    }
}
