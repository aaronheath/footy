<?php

namespace App\Observers;

use App\Models\League;
use App\Traits\ObserverHelper;

class LeagueObserver
{
    use ObserverHelper;

    public function created(League $league)
    {
        $this->logCreated($league);
    }

    public function updated(League $league)
    {
        $this->logUpdated($league);
    }

    public function deleted(League $league)
    {
        $this->logDeleted($league);
    }

    public function restored(League $league)
    {
        $this->logRestored($league);
    }

    public function forceDeleted(League $league)
    {
        $this->logForceDeleted($league);
    }
}
