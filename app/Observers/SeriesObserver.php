<?php

namespace App\Observers;

use App\Models\Series;
use App\Traits\ObserverHelper;

class SeriesObserver
{
    use ObserverHelper;

    public function created(Series $series)
    {
        $this->logCreated($series);
    }

    public function updated(Series $series)
    {
        $this->logUpdated($series);
    }

    public function deleted(Series $series)
    {
        $this->logDeleted($series);
    }

    public function restored(Series $series)
    {
        $this->logRestored($series);
    }

    public function forceDeleted(Series $series)
    {
        $this->logForceDeleted($series);
    }
}
