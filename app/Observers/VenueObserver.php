<?php

namespace App\Observers;

use App\Models\Venue;
use App\Traits\ObserverHelper;

class VenueObserver
{
    use ObserverHelper;

    public function created(Venue $venue)
    {
        $this->logCreated($venue);
    }

    public function updated(Venue $venue)
    {
        $this->logUpdated($venue);
    }

    public function deleted(Venue $venue)
    {
        $this->logDeleted($venue);
    }

    public function restored(Venue $venue)
    {
        $this->logRestored($venue);
    }

    public function forceDeleted(Venue $venue)
    {
        $this->logForceDeleted($venue);
    }
}
