<?php

namespace App\Observers;

use App\Jobs\GenerateLadder;
use App\Models\Match;
use App\Traits\ObserverHelper;

class MatchObserver
{
    use ObserverHelper;

    public function created(Match $match)
    {
        $this->logCreated($match);

        $this->generateLadder($match);
    }

    public function updated(Match $match)
    {
        $this->logUpdated($match);

        $this->generateLadder($match);
    }

    public function deleted(Match $match)
    {
        $this->logDeleted($match);

        $this->generateLadder($match);
    }

    public function restored(Match $match)
    {
        $this->logRestored($match);
    }

    public function forceDeleted(Match $match)
    {
        $this->logForceDeleted($match);
    }

    protected function generateLadder(Match $match)
    {
        dispatch(new GenerateLadder($match->series));
    }
}
