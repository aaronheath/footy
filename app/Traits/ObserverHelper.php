<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;

trait ObserverHelper
{
    public $censorAttributes = [
        'password',
        'remember_token',
    ];

    public function logCreated(Model $model)
    {
        $values = $model->getAttributes();

        $this->logAction('Created', $model, [
            'init_values' => $this->censoredValues($values),
        ]);
    }

    public function logUpdated(Model $model)
    {
        $this->logAction('Updated', $model, [
            'changed_values' => $this->censoredValues($model->getChanges()),
        ]);
    }

    public function logDeleted(Model $model)
    {
        $this->logAction('Deleted', $model);
    }

    public function logRestored(Model $model)
    {
        $this->logAction('Restored', $model);
    }

    public function logForceDeleted(Model $model)
    {
        $this->logAction('Force Deleted', $model);
    }

    protected function logAction($action, Model $model, array $supplementary = [])
    {
        logger()->info($action.' '.$this->modelBasename($model).' with id '.$model->id, $supplementary);
    }

    public function modelBasename(Model $model)
    {
        return class_basename(get_class($model));
    }

    protected function censoredValues(array $attributes)
    {
        collect($this->censorAttributes)->each(function ($key) use (&$attributes) {
            if (isset($attributes[$key]) && ! is_null($attributes[$key])) {
                $attributes[$key] = '********';
            }
        });

        return $attributes;
    }
}
