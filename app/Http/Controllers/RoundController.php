<?php

namespace App\Http\Controllers;

use App\Http\Resources\Round as RoundResource;
use App\Models\Round;
use App\Models\Series;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoundController extends Controller
{
    protected $loadRelations = ['series'];

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        return RoundResource::collection(
            Round::with($this->loadRelations)
                ->orderBy('series_id')
                ->orderBy('round')
                ->paginate()
        );
    }

    public function show(Round $round)
    {
        return new RoundResource($round->loadMissing($this->loadRelations));
    }

    public function store(Request $request)
    {
        return new RoundResource(
            Round::create($this->translateValidated($this->validated($request)))
                ->loadMissing($this->loadRelations)
        );
    }

    public function update(Request $request, Round $round)
    {
        $round->update($this->translateValidated($this->validated($request, $round)));

        return new RoundResource($round->refresh()->loadMissing($this->loadRelations));
    }

    public function destroy(Round $round)
    {
        $round->delete();
    }

    protected function validated($request, Round $round = null)
    {
        $validator = Validator::make($request->all(), [
            'series_id' => [
                is_null($round) ? 'required' : null,
                'exists:series,uuid',
            ],
            'round' => [
                is_null($round) ? 'required' : null,
                'string',
                'max:50',
            ],
            'name' => [
                'nullable',
                'string',
                'max:50',
            ],
        ]);

        $validator->after(function ($validator) use ($request, $round) {
            $series = $this->series($request, $round);

            if (is_null($series)) {
                return;
            }

            $this->validateUniquenessOfMultipleColumns(
                $validator,
                $this->uniqueMultipleColumnSets($request, $series, $round)
            );
        });

        $validator->validate();

        return $validator->validated();
    }

    protected function series($request, Round $round = null)
    {
        $series = Series::whereUuid($request->series_id)->first();

        if (! is_null($series)) {
            return $series;
        }

        return is_null($round) ? null : $round->series;
    }

    protected function uniqueMultipleColumnSets($request, Series $series, Round $round = null)
    {
        return [
            [
                'field' => 'round',
                'message' => 'The round is already associated with series.',
                'query' => Round::whereRound($request->round ?? $round->round)->whereSeriesId($series->id),
            ],
        ];
    }
}
