<?php

namespace App\Http\Controllers;

use App\Http\Resources\Venue as VenueResource;
use App\Models\Venue;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class VenueController extends Controller
{
    protected $loadRelations = ['state'];

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        return VenueResource::collection(
            Venue::with($this->loadRelations)->orderBy('name')->paginate()
        );
    }

    public function show(Venue $venue)
    {
        return new VenueResource($venue->loadMissing($this->loadRelations));
    }

    public function store(Request $request)
    {
        return new VenueResource(
            Venue::create($this->translateValidated($this->validated($request)))
                ->loadMissing($this->loadRelations)
        );
    }

    public function update(Request $request, Venue $venue)
    {
        $venue->update($this->translateValidated($this->validated($request, $venue)));

        return new VenueResource($venue->refresh()->loadMissing($this->loadRelations));
    }

    public function destroy(Venue $venue)
    {
        $venue->delete();
    }

    protected function validated($request, Venue $venue = null)
    {
        return $request->validate([
            'state_id' => [
                is_null($venue) ? 'required' : null,
                'exists:states,uuid',
            ],
            'abbreviation' => [
                is_null($venue) ? 'required' : null,
                is_null($venue) ? Rule::unique('venues') : Rule::unique('venues')->ignore($venue),
                'string',
                'max:4',
            ],
            'name' => [
                is_null($venue) ? 'required' : null,
                is_null($venue) ? Rule::unique('venues') : Rule::unique('venues')->ignore($venue),
                'string',
                'max:50',
            ],
            'capacity' => [
                is_null($venue) ? 'required' : null,
                'numeric',
                'between:1,300000',
            ],
            'latitude' => [
                is_null($venue) ? 'required' : null,
                'numeric',
                'between:-90,90',
            ],
            'longitude' => [
                is_null($venue) ? 'required' : null,
                'numeric',
                'between:-180,180',
            ],
        ]);
    }
}
