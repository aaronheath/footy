<?php

namespace App\Http\Controllers;

use App\Models\League;
use App\Models\Round;
use App\Models\Season;
use App\Models\Series;
use App\Models\State;
use App\Models\Team;
use App\Models\Venue;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function translateValidated($data)
    {
        $classMap = [
            'home_state_id' => State::class,
            'home_team_id' => Team::class,
            'away_team_id' => Team::class,
            'season_id' => Season::class,
            'league_id' => League::class,
            'state_id' => State::class,
            'venue_id' => Venue::class,
            'series_id' => Series::class,
            'round_id' => Round::class,
        ];

        return collect($data)
            ->mapWithKeys(function ($value, $key) use ($classMap) {
                return [
                    $key => in_array($key, array_keys($classMap))
                        ? $classMap[$key]::idForUuid($value)
                        : $value,
                ];
            })
            ->all();
    }

    public function validateUniquenessOfMultipleColumns($validator, array $validations)
    {
        collect($validations)
            ->each(function ($field) use ($validator) {
                if ($field['query']->exists()) {
                    $validator->errors()->add($field['field'], $field['message']);
                }
            });
    }
}
