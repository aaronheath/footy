<?php

namespace App\Http\Controllers;

use App\Http\Resources\Season as SeasonResource;
use App\Models\Season;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SeasonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        $query = Season::orderBy('year');

        return SeasonResource::collection(
            request()->input('page', 1) === 'all' ? $query->get() : $query->paginate()
        );
    }

    public function store(Request $request)
    {
        return new SeasonResource(Season::create($this->validated($request)));
    }

    public function show(Season $season)
    {
        return new SeasonResource($season);
    }

    public function update(Request $request, Season $season)
    {
        $season->update($this->validated($request, $season));

        return new SeasonResource($season->refresh());
    }

    public function destroy(Season $season)
    {
        $season->delete();
    }

    protected function validated($request, Season $season = null)
    {
        return $request->validate([
            'year' => [
                'required',
                is_null($season) ? Rule::unique('seasons') : Rule::unique('seasons')->ignore($season),
                'numeric',
                'min:1990',
                'max:2099',
            ],
        ]);
    }
}
