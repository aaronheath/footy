<?php

namespace App\Http\Controllers;

use App\Actions\Ladder;
use App\Http\Resources\Team;
use App\Models\Series;
use Illuminate\Http\Request;

class LadderController extends Controller
{
//    protected $loadRelations = ['series', 'teams'];

    public function __construct()
    {
//        $this->middleware('auth')->except(['index', 'show']);
    }

    public function latest(Series $series)
    {
//        $ladder = cache()->rememberForever('ladder_'.$series->uuid, function() use ($series) {
//            return Ladder::series($series)->generate();
//        });

        $ladder = cache()->get('ladder_'.$series->uuid);

//        $team = Team::make($ladder[0]['team'])->toArray(request());

//        dd($team);

        $ladder = collect($ladder)->map(function ($record) {
            return array_merge($record, [
                'team' => Team::make($record['team'])->toArray(request()),
            ]);
        });

        return [
            'status' => 'success',
            'data' => $ladder,
        ];
    }
}
