<?php

namespace App\Http\Controllers;

use App\Http\Resources\Match as MatchResource;
use App\Http\Resources\MatchCollection;
use App\Models\Match;
use App\Models\Series;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class MatchController extends Controller
{
    protected $loadRelations = ['round', 'venue', 'homeTeam', 'awayTeam'];

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        return new MatchCollection(
            Match::with($this->loadRelations)->orderBy('start_time')->paginate()
        );
    }

    public function show(Match $match)
    {
//        QueryBuilder::for(Series::class)
//            ->allowedFilters([
//                AllowedFilter::scope('league'),
//            ])
//            ->with($this->loadRelations)
//            ->orderBy('name')
//            ->paginate();

//        dd($match->loadMissing(
//            array_merge($this->loadRelations, ['round.series'])
//        ));


        return new MatchResource($match->loadMissing(
            array_merge(['round.series', 'round.series.league'], $this->loadRelations)
        ));
    }

    public function store(Request $request)
    {
        return new MatchResource(
            Match::create($this->translateValidated($this->validated($request)))
                ->loadMissing(
                    array_merge(['round.series', 'round.series.league'], $this->loadRelations)
                )
        );
    }

    public function update(Request $request, Match $match)
    {
        $match->update($this->translateValidated($this->validated($request, $match)));

        return new MatchResource($match->refresh()->loadMissing(
            array_merge(['round.series', 'round.series.league'], $this->loadRelations)
        ));
    }

    public function destroy(Match $match)
    {
        $match->delete();
    }

    protected function validated($request, Match $match = null)
    {
        return $request->validate([
            'round_id' => [
                is_null($match) ? 'required' : null,
                'exists:rounds,uuid',
            ],
            'venue_id' => [
                'exists:venues,uuid',
                'nullable',
            ],
            'home_team_id' => [
                'exists:teams,uuid',
                'nullable',
            ],
            'home_team_goals' => [
                'nullable',
                'numeric',
                'min:0',
            ],
            'home_team_behinds' => [
                'nullable',
                'numeric',
                'min:0',
            ],
            'away_team_id' => [
                'nullable',
                'exists:teams,uuid',
            ],
            'away_team_goals' => [
                'nullable',
                'numeric',
                'min:0',
            ],
            'away_team_behinds' => [
                'nullable',
                'numeric',
                'min:0',
            ],
            'start_time' => [
                'nullable',
                'date',
            ],
            'attendance' => [
                'nullable',
                'numeric',
                'min:0',
            ],
            'reference' => [
                'nullable',
                'string',
                'max:20',
            ],
            'name' => [
                'nullable',
                'string',
                'max:65000',
            ],
        ]);
    }
}
