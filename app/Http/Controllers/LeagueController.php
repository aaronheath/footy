<?php

namespace App\Http\Controllers;

use App\Http\Resources\League as LeagueResource;
use App\Models\League;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class LeagueController extends Controller
{
    protected $loadRelations = ['series', 'teams'];

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        return LeagueResource::collection(
            League::with($this->loadRelations)->orderBy('name')->paginate()
        );
    }

    public function show(League $league)
    {
        return new LeagueResource($league->loadMissing($this->loadRelations));
    }

    public function store(Request $request)
    {
        return new LeagueResource(
            League::create($this->translateValidated($this->validated($request)))
                ->loadMissing($this->loadRelations)
        );
    }

    public function update(Request $request, League $league)
    {
        $league->update($this->translateValidated($this->validated($request, $league)));

        return new LeagueResource($league->refresh()->loadMissing($this->loadRelations));
    }

    public function destroy(League $league)
    {
        $league->delete();
    }

    protected function validated($request, League $league = null)
    {
        return $request->validate([
            'abbreviation' => [
                is_null($league) ? 'required' : null,
                is_null($league) ? Rule::unique('leagues') : Rule::unique('leagues')->ignore($league),
                'string',
                'max:5',
            ],
            'name' => [
                is_null($league) ? 'required' : null,
                is_null($league) ? Rule::unique('leagues') : Rule::unique('leagues')->ignore($league),
                'string',
                'max:50',
            ],
        ]);
    }
}
