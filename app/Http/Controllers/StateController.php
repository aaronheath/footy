<?php

namespace App\Http\Controllers;

use App\Http\Resources\State as StateResource;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class StateController extends Controller
{
    protected $loadRelations = ['venues', 'homeTeams'];

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        return StateResource::collection(
            State::with($this->loadRelations)->orderBy('name')->paginate()
        );
    }

    public function show(State $state)
    {
        return new StateResource($state->loadMissing($this->loadRelations));
    }

    public function store(Request $request)
    {
        return new StateResource(
            State::create($this->translateValidated($this->validated($request)))
                ->loadMissing($this->loadRelations)
        );
    }

    public function update(Request $request, State $state)
    {
        $state->update($this->translateValidated($this->validated($request, $state)));

        return new StateResource($state->refresh()->loadMissing($this->loadRelations));
    }

    public function destroy(State $state)
    {
        $state->delete();
    }

    protected function validated($request, State $state = null)
    {
        return $request->validate([
            'abbreviation' => [
                is_null($state) ? 'required' : null,
                is_null($state) ? Rule::unique('states') : Rule::unique('states')->ignore($state),
                'string',
                'max:3',
            ],
            'name' => [
                is_null($state) ? 'required' : null,
                is_null($state) ? Rule::unique('states') : Rule::unique('states')->ignore($state),
                'string',
                'max:50',
            ],
            'timezone' => [
                is_null($state) ? 'required' : null,
                'timezone',
            ],
        ]);
    }
}
