<?php

namespace App\Http\Controllers;

use App\Http\Resources\Team as TeamResource;
use App\Models\League;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TeamController extends Controller
{
    protected $loadRelations = ['homeState', 'league'];

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        return TeamResource::collection(
            Team::with($this->loadRelations)->orderBy('short_name')->paginate()
        );
    }

    public function show(Team $team)
    {
        return new TeamResource($team->loadMissing($this->loadRelations));
    }

    public function store(Request $request)
    {
        return new TeamResource(
            Team::create($this->translateValidated($this->validated($request)))
                ->loadMissing($this->loadRelations)
        );
    }

    public function update(Request $request, Team $team)
    {
        $team->update($this->translateValidated($this->validated($request, $team)));

        return new TeamResource($team->refresh()->loadMissing($this->loadRelations));
    }

    public function destroy(Team $team)
    {
        $team->delete();
    }

    protected function validated($request, Team $team = null)
    {
        $validator = Validator::make($request->all(), [
            'home_state_id' => [
                is_null($team) ? 'required' : null,
                'exists:states,uuid',
            ],
            'league_id' => [
                is_null($team) ? 'required' : null,
                'exists:leagues,uuid',
            ],
            'abbreviation' => [
                is_null($team) ? 'required' : null,
                'string',
                'max:3',
            ],
            'short_name' => [
                is_null($team) ? 'required' : null,
                'string',
                'max:20',
            ],
            'full_name' => [
                is_null($team) ? 'required' : null,
                'string',
                'max:50',
            ],
            'primary_colour' => [
                is_null($team) ? 'required' : null,
                'string',
                'max:7',
            ],
            'secondary_colour' => [
                is_null($team) ? 'required' : null,
                'string',
                'max:7',
            ],
            'tertiary_colour' => [
                is_null($team) ? 'required' : null,
                'string',
                'max:7',
            ],
            'address' => [
                is_null($team) ? 'required' : null,
                'string',
                'max:100',
            ],
        ]);

        $validator->after(function ($validator) use ($request, $team) {
            $league = $this->league($request, $team);

            if (is_null($league)) {
                return;
            }

            $this->validateUniquenessOfMultipleColumns(
                $validator,
                $this->uniqueMultipleColumnSets($request, $league, $team)
            );
        });

        $validator->validate();

        return $validator->validated();
    }

    protected function league($request, Team $team = null)
    {
        $league = League::whereUuid($request->league_id)->first();

        if (! is_null($league)) {
            return $league;
        }

        return is_null($team) ? null : $team->league;
    }

    protected function uniqueMultipleColumnSets($request, League $league, Team $team = null)
    {
        return [
            [
                'field' => 'abbreviation',
                'message' => 'The abbreviation is already associated with league.',
                'query' => Team::whereAbbreviation($request->abbreviation ?? $team->abbreviation)
                    ->whereLeagueId($league->id),
            ],
            [
                'field' => 'short_name',
                'message' => 'The short name is already associated with league.',
                'query' => Team::whereShortName($request->short_name ?? $team->short_name)
                    ->whereLeagueId($league->id),
            ],
            [
                'field' => 'full_name',
                'message' => 'The full name is already associated with league.',
                'query' => Team::whereFullName($request->full_name ?? $team->full_name)
                    ->whereLeagueId($league->id),
            ],
            [
                'field' => 'colours',
                'message' => 'The colours are in use by another team in league.',
                'query' => Team::wherePrimaryColour($request->primary_colour ?? $team->primary_colour)
                    ->whereSecondaryColour($request->secondary_colour ?? $team->secondary_colour)
                    ->whereTertiaryColour($request->tertiary_colour ?? $team->tertiary_colour)
                    ->whereLeagueId($league->id),
            ],
            [
                'field' => 'address',
                'message' => 'The address is already associated with league.',
                'query' => Team::whereAddress($request->address ?? $team->address)
                    ->whereLeagueId($league->id),
            ],
        ];
    }
}
