<?php

namespace App\Http\Controllers\Webapps;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function app()
    {
        return view('webapps.admin', ['user' => request()->user()]);
//        return view('home');
    }
}
