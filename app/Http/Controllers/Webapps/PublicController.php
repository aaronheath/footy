<?php

namespace App\Http\Controllers\Webapps;

use App\Http\Controllers\Controller;

class PublicController extends Controller
{
    public function __construct()
    {
        //
    }

    public function app()
    {
        return view('webapps.public');
//        return view('welcome');
    }
}
