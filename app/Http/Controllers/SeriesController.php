<?php

namespace App\Http\Controllers;

use App\Http\Resources\Series as SeriesResource;
use App\Models\Series;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SeriesController extends Controller
{
    protected $loadRelations = ['league', 'season'];

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        $series = QueryBuilder::for(Series::class)
            ->allowedFilters([
                AllowedFilter::scope('league'),
            ])
            ->with($this->loadRelations)
            ->orderBy('name')
            ->paginate();


        return SeriesResource::collection(
            $series
//            Series::with($this->loadRelations)->orderBy('name')->paginate()
        );
    }

    public function show(Series $series)
    {
        return new SeriesResource($series->loadMissing($this->loadRelations));
    }

    public function store(Request $request)
    {
        return new SeriesResource(
            Series::create($this->translateValidated($this->validated($request)))
                ->loadMissing($this->loadRelations)
        );
    }

    public function update(Request $request, Series $series)
    {
        $series->update($this->translateValidated($this->validated($request, $series)));

        return new SeriesResource($series->refresh()->loadMissing($this->loadRelations));
    }

    public function destroy(Series $series)
    {
        $series->delete();
    }

    protected function validated($request, Series $series = null)
    {
        return $request->validate([
            'season_id' => [
                is_null($series) ? 'required' : null,
                'exists:seasons,uuid',
            ],
            'league_id' => [
                is_null($series) ? 'required' : null,
                'exists:leagues,uuid',
            ],
            'name' => [
                is_null($series) ? 'required' : null,
                is_null($series) ? Rule::unique('series') : Rule::unique('series')->ignore($series),
                'string',
                'max:50',
            ],
            'points_for_win' => [
                Rule::in([2, 4]),
            ],
            'percentage_divisor' => [
                Rule::in(['points_against', 'total_points']),
            ],
        ]);
    }
}
