<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Match extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        $this->resource->round->unsetRelation('series');

        return [
            'id' => $this->uuid,
            'home_team_goals' => $this->home_team_goals,
            'home_team_behinds' => $this->home_team_behinds,
            'home_team_points' => $this->home_team_points,
            'away_team_goals' => $this->away_team_goals,
            'away_team_behinds' => $this->away_team_behinds,
            'away_team_points' => $this->away_team_points,
            'start_time' => $this->start_time,
            'attendance' => $this->attendance,
            'reference' => $this->reference,
            'name' => $this->name,
            'round' => new Round($this->whenloaded('round')),
            'venue' => new Venue($this->whenloaded('venue')),
            'home_team' => new Team($this->whenloaded('homeTeam')),
            'away_team' => new Team($this->whenloaded('awayTeam')),
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
