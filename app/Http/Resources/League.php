<?php

namespace App\Http\Resources;

use App\Http\Resources\Series as SeriesResource;
use App\Http\Resources\Team as TeamsResource;
use Illuminate\Http\Resources\Json\JsonResource;

class League extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->uuid,
            'abbreviation' => $this->abbreviation,
            'name' => $this->name,
            'series' => SeriesResource::collection($this->whenLoaded('series')),
            'teams' => TeamsResource::collection($this->whenLoaded('teams')),
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
