<?php

namespace App\Http\Resources;

use App\Http\Resources\League as LeagueResource;
use App\Http\Resources\State as StateResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Team extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return collect([
                'home_state' => $this->relationLoaded('homeState')
                    ? new StateResource($this->whenloaded('homeState'))
                    : null,
                'league' => $this->relationLoaded('league')
                    ? new LeagueResource($this->whenloaded('league'))
                    : null,
            ])
            ->filter(function ($value) {
                return ! is_null($value);
            })
            ->merge([
                'id' => $this->uuid,
                'abbreviation' => $this->abbreviation,
                'short_name' => $this->short_name,
                'full_name' => $this->full_name,
                'primary_colour' => $this->primary_colour,
                'secondary_colour' => $this->secondary_colour,
                'tertiary_colour' => $this->tertiary_colour,
                'address' => $this->address,
                'created_at' => $this->created_at->toDateTimeString(),
                'updated_at' => $this->updated_at->toDateTimeString(),
            ])
            ->all();
    }
}
