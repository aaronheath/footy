<?php

namespace App\Http\Resources;

use App\Http\Resources\Team as TeamsResource;
use App\Http\Resources\Venue as VenueResource;
use Illuminate\Http\Resources\Json\JsonResource;

class State extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->uuid,
            'abbreviation' => $this->abbreviation,
            'name' => $this->name,
            'timezone' => $this->timezone,
            'venues' => VenueResource::collection($this->whenLoaded('venues')),
            'home_teams' => TeamsResource::collection($this->whenLoaded('homeTeams')),
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
