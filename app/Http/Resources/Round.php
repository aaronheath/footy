<?php

namespace App\Http\Resources;

use App\Http\Resources\Series as SeriesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Round extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        dump($this->resource);

        return [
            'id' => $this->uuid,
            'round' => $this->round,
            'name' => $this->name,
            'series' => new SeriesResource($this->whenloaded('series')),
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
