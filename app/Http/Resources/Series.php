<?php

namespace App\Http\Resources;

use App\Http\Resources\League as LeagueResource;
use App\Http\Resources\Season as SeasonResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Series extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->uuid,
            'name' => $this->name,
            'points_for_win' => $this->points_for_win,
            'percentage_divisor' => $this->percentage_divisor,
            'season' => new SeasonResource($this->whenLoaded('season')),
            'league' => new LeagueResource($this->whenLoaded('league')),
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
