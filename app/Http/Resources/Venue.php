<?php

namespace App\Http\Resources;

use App\Http\Resources\State as StateResource;
//use App\Http\Resources\Match as MatchesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Venue extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->uuid,
            'abbreviation' => $this->abbreviation,
            'name' => $this->name,
            'capacity' => $this->capacity,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'state' => new StateResource($this->whenLoaded('state')),
//            'matches' => new MatchesResource($this->whenLoaded('matches')),
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
