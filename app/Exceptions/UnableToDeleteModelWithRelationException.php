<?php

namespace App\Exceptions;

use Exception;

class UnableToDeleteModelWithRelationException extends Exception
{
    public function render($request)
    {
        return response([
            'message' => 'The given data was invalid.',
            'errors' => [
                $this->getMessage(),
            ],
        ], 422);
    }
}
