<?php

namespace Tests\Unit\Jobs;

use App\Actions\Ladder;
use App\Jobs\GenerateLadder;
use App\Models\Series;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

class GenerateLadderTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_requests_ladder_be_generated()
    {
        $series = factory(Series::class)->create();

        $this->instance(Ladder::class, Mockery::mock(Ladder::class, function ($mock) use ($series) {
            $mock->shouldReceive('series')
                ->once()
                ->withArgs(function ($argSeries) use ($series) {
                    return $argSeries->uuid === $series->uuid;
                })
                ->andReturnSelf();

            $mock->shouldReceive('cache')
                ->once()
                ->andReturnSelf();

            $mock->shouldReceive('generate')
                ->once()
                ->andReturnSelf();
        }));

        $job = new GenerateLadder($series);

        $job->handle();
    }
}
