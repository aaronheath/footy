<?php

namespace Tests\Unit\Traits;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;
use TiMacDonald\Log\LogFake;

class ObserverHelperTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_logs_when_model_created()
    {
        Log::swap(new LogFake());

        $user = factory(User::class)->create();

        Log::assertLogged('info', function ($message, $context) use ($user) {
            $expectedMessage = 'Created User with id '.$user->id;

            $expectedContext = [
                'init_values' => [
                    'name' => $user->name,
                    'email' => $user->email,
                    'email_verified_at' => $user->email_verified_at,
                    'password' => '********', // censored
                    'remember_token' => '********', // censored
                    'updated_at' => $user->updated_at,
                    'created_at' => $user->created_at,
                    'id' => $user->id,
                ],
            ];

            if ($message != $expectedMessage) {
                return false;
            }

            if ($context != $expectedContext) {
                return false;
            }

            return true;
        });
    }

    /**
     * @test
     */
    public function it_logs_when_model_updated()
    {
        $user = factory(User::class)->create();

        Log::swap(new LogFake());

        $user->update([
            'email' => 'john@example.com',
            'password' => bcrypt('password'),
        ]);

        Log::assertLogged('info', function ($message, $context) use ($user) {
            $expectedMessage = 'Updated User with id '.$user->id;

            $expectedContext = [
                'changed_values' => [
                    'email' => $user->email,
                    'password' => '********', // censored
                ],
            ];

            if ($message != $expectedMessage) {
                return false;
            }

            if ($context != $expectedContext) {
                return false;
            }

            return true;
        });
    }

    /**
     * @test
     */
    public function it_logs_when_model_deleted()
    {
        $user = factory(User::class)->create();

        Log::swap(new LogFake());

        $user->delete();

        Log::assertLogged('info', function ($message, $context) use ($user) {
            return $message == 'Deleted User with id '.$user->id;
        });
    }

    // TODO create tests for logRestored and logForceDeleted when we use a table with soft deletes
}
