<?php

namespace Tests\Unit\Models;

use App\Models\Match;
use App\Models\Round;
use App\Models\Series;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RoundTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_uuid()
    {
        $this->assertUuid(factory(Round::class)->create()->uuid);
    }

    /**
     * @test
     */
    public function it_should_have_series_relation()
    {
        $series = factory(Series::class)->create();

        $round = factory(Round::class)->create([
            'series_id' => $series->id,
        ]);

        $this->assertInstanceOf(Series::class, $round->series);
        $this->assertEquals($series->id, $round->series->id);
    }

    /**
     * @test
     */
    public function it_has_many_matches()
    {
        $round = factory(Round::class)->create();

        $match1 = factory(Match::class)->create([
            'round_id' => $round->id,
        ]);

        $match2 = factory(Match::class)->create([
            'round_id' => $round->id,
        ]);

        $this->assertCount(2, $round->matches);
        $this->assertInstanceOf(Match::class, $round->matches[0]);
        $this->assertEquals($match1->id, $round->matches[0]->id);
        $this->assertInstanceOf(Match::class, $round->matches[1]);
        $this->assertEquals($match2->id, $round->matches[1]->id);
    }
}
