<?php

namespace Tests\Unit\Models;

use App\Models\State;
use App\Models\Team;
use App\Models\Venue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StateTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_uuid()
    {
        $this->assertUuid(factory(State::class)->create()->uuid);
    }

    /**
     * @test
     */
    public function it_has_many_venues()
    {
        $state = factory(State::class)->create();

        $venue1 = factory(Venue::class)->create([
            'state_id' => $state->id,
        ]);

        $venue2 = factory(Venue::class)->create([
            'state_id' => $state->id,
        ]);

        $this->assertCount(2, $state->venues);
        $this->assertInstanceOf(Venue::class, $state->venues[0]);
        $this->assertEquals($venue1->id, $state->venues[0]->id);
        $this->assertInstanceOf(Venue::class, $state->venues[1]);
        $this->assertEquals($venue2->id, $state->venues[1]->id);
    }

    /**
     * @test
     */
    public function it_has_many_home_teams()
    {
        $state = factory(State::class)->create();

        $homeTeam1 = factory(Team::class)->create([
            'home_state_id' => $state->id,
        ]);

        $homeTeam2 = factory(Team::class)->create([
            'home_state_id' => $state->id,
        ]);

        $this->assertCount(2, $state->homeTeams);
        $this->assertInstanceOf(Team::class, $state->homeTeams[0]);
        $this->assertEquals($homeTeam1->id, $state->homeTeams[0]->id);
        $this->assertInstanceOf(Team::class, $state->homeTeams[1]);
        $this->assertEquals($homeTeam2->id, $state->homeTeams[1]->id);
    }
}
