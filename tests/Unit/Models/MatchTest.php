<?php

namespace Tests\Unit\Models;

use App\Models\Match;
use App\Models\Round;
use App\Models\Team;
use App\Models\Venue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MatchTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_uuid()
    {
        $this->assertUuid(factory(Match::class)->create()->uuid);
    }

    /**
     * @test
     */
    public function it_should_have_round_relation()
    {
        $round = factory(Round::class)->create();

        $match = factory(Match::class)->create([
            'round_id' => $round->id,
        ]);

        $this->assertInstanceOf(Round::class, $match->round);
        $this->assertEquals($round->id, $match->round->id);
    }

    /**
     * @test
     */
    public function it_should_have_venue_relation()
    {
        $venue = factory(Venue::class)->create();

        $match = factory(Match::class)->create([
            'venue_id' => $venue->id,
        ]);

        $this->assertInstanceOf(Venue::class, $match->venue);
        $this->assertEquals($venue->id, $match->venue->id);
    }

    /**
     * @test
     */
    public function it_should_have_home_team_relation()
    {
        $homeTeam = factory(Team::class)->create();

        $match = factory(Match::class)->create([
            'home_team_id' => $homeTeam->id,
        ]);

        $this->assertInstanceOf(Team::class, $match->homeTeam);
        $this->assertEquals($homeTeam->id, $match->homeTeam->id);
    }

    /**
     * @test
     */
    public function it_should_have_away_team_relation()
    {
        $awayTeam = factory(Team::class)->create();

        $match = factory(Match::class)->create([
            'away_team_id' => $awayTeam->id,
        ]);

        $this->assertInstanceOf(Team::class, $match->awayTeam);
        $this->assertEquals($awayTeam->id, $match->awayTeam->id);
    }

    /**
     * @test
     */
    public function can_calculate_teams_score()
    {
        $this->assertEquals(100, Match::totalPoints(15, 10));
        $this->assertEquals(25, Match::totalPoints(3, 7));
        $this->assertEquals(139, Match::totalPoints(19, 25));

        $this->assertNull(Match::totalPoints(null, 25));
        $this->assertNull(Match::totalPoints(19, null));
    }

    /**
     * @test
     */
    public function it_should_calculate_total_point_scores_when_data_supplied_upon_creation()
    {
        $match = factory(Match::class)->create([
            'home_team_goals' => 15,
            'home_team_behinds' => 10,
            'away_team_goals' => 16,
            'away_team_behinds' => 5,
        ]);

        $this->assertEquals(100, $match->home_team_points);
        $this->assertEquals(101, $match->away_team_points);
    }

    /**
     * @test
     */
    public function it_should_not_calculate_total_points_when_not_enough_data_not_supplied_upon_creation()
    {
        $match1 = factory(Match::class)->create([
            'home_team_goals' => null,
            'home_team_behinds' => null,
            'away_team_goals' => null,
            'away_team_behinds' => null,
        ]);

        $this->assertNull($match1->home_team_points);
        $this->assertNull($match1->away_team_points);

        $match2 = factory(Match::class)->create([
            'home_team_goals' => null,
            'home_team_behinds' => 10,
            'away_team_goals' => 16,
            'away_team_behinds' => 5,
        ]);

        $this->assertNull($match2->home_team_points);
        $this->assertNull($match2->away_team_points);
    }

    /**
     * @test
     */
    public function it_should_calculate_total_point_scores_when_data_supplied_upon_update()
    {
        $match = factory(Match::class)->create();

        $match->update([
            'home_team_goals' => 15,
            'home_team_behinds' => 10,
            'away_team_goals' => 16,
            'away_team_behinds' => 5,
        ]);

        $this->assertEquals(100, $match->home_team_points);
        $this->assertEquals(101, $match->away_team_points);
    }

    /**
     * @test
     */
    public function it_should_not_calculate_total_points_when_not_enough_data_not_supplied_upon_update()
    {
        $match1 = factory(Match::class)->create();

        $match1->update([
            'home_team_goals' => null,
            'home_team_behinds' => null,
            'away_team_goals' => null,
            'away_team_behinds' => null,
        ]);

        $this->assertNull($match1->home_team_points);
        $this->assertNull($match1->away_team_points);

        $match2 = factory(Match::class)->create();

        $match2->update([
            'home_team_goals' => null,
            'home_team_behinds' => 10,
            'away_team_goals' => 16,
            'away_team_behinds' => 5,
        ]);

        $this->assertNull($match2->home_team_points);
        $this->assertNull($match2->away_team_points);
    }
}
