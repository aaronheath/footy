<?php

namespace Tests\Unit\Models;

use App\Models\League;
use App\Models\Round;
use App\Models\Season;
use App\Models\Series;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SeriesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_uuid()
    {
        $this->assertUuid(factory(Series::class)->create()->uuid);
    }

    /**
     * @test
     */
    public function it_should_have_season_relation()
    {
        $season = factory(Season::class)->create();

        $series = factory(Series::class)->create([
            'season_id' => $season->id,
        ]);

        $this->assertInstanceOf(Season::class, $series->season);
        $this->assertEquals($season->id, $series->season->id);
    }

    /**
     * @test
     */
    public function it_should_have_league_relation()
    {
        $league = factory(League::class)->create();

        $series = factory(Series::class)->create([
            'league_id' => $league->id,
        ]);

        $this->assertInstanceOf(League::class, $series->league);
        $this->assertEquals($league->id, $series->league->id);
    }

    /**
     * @test
     */
    public function it_should_have_many_rounds()
    {
        $series = factory(Series::class)->create();

        $round1 = factory(Round::class)->create([
            'series_id' => $series->id,
        ]);

        $round2 = factory(Round::class)->create([
            'series_id' => $series->id,
        ]);

        $rounds = $series->rounds()->orderBy('id')->get();

        $this->assertCount(2, $rounds);
        $this->assertInstanceOf(Round::class, $rounds[0]);
        $this->assertEquals($round1->id, $rounds[0]->id);
        $this->assertInstanceOf(Round::class, $rounds[1]);
        $this->assertEquals($round2->id, $rounds[1]->id);
    }
}
