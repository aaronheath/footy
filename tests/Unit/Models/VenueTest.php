<?php

namespace Tests\Unit\Models;

use App\Models\Match;
use App\Models\State;
use App\Models\Venue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class VenueTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_uuid()
    {
        $this->assertUuid(factory(Venue::class)->create()->uuid);
    }

    /**
     * @test
     */
    public function it_should_have_state_relation()
    {
        $state = factory(State::class)->create();

        $venue = factory(Venue::class)->create([
            'state_id' => $state->id,
        ]);

        $this->assertInstanceOf(State::class, $venue->state);
        $this->assertEquals($state->id, $venue->state->id);
    }

    /**
     * @test
     */
    public function it_has_many_matches()
    {
        $venue = factory(Venue::class)->create();

        $match1 = factory(Match::class)->create([
            'venue_id' => $venue->id,
        ]);

        $match2 = factory(Match::class)->create([
            'venue_id' => $venue->id,
        ]);

        $this->assertCount(2, $venue->matches);
        $this->assertInstanceOf(Match::class, $venue->matches[0]);
        $this->assertEquals($match1->id, $venue->matches[0]->id);
        $this->assertInstanceOf(Match::class, $venue->matches[1]);
        $this->assertEquals($match2->id, $venue->matches[1]->id);
    }
}
