<?php

namespace Tests\Unit\Models;

use App\Models\League;
use App\Models\Match;
use App\Models\State;
use App\Models\Team;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TeamTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_uuid()
    {
        $this->assertUuid(factory(Team::class)->create()->uuid);
    }

    /**
     * @test
     */
    public function it_should_have_home_state_relation()
    {
        $homeState = factory(State::class)->create();

        $team = factory(Team::class)->create([
            'home_state_id' => $homeState->id,
        ]);

        $this->assertInstanceOf(State::class, $team->homeState);
        $this->assertEquals($homeState->id, $team->homeState->id);
    }

    /**
     * @test
     */
    public function it_should_have_league_relation()
    {
        $league = factory(League::class)->create();

        $team = factory(Team::class)->create([
            'league_id' => $league->id,
        ]);

        $this->assertInstanceOf(League::class, $team->league);
        $this->assertEquals($league->id, $team->league->id);
    }

    /**
     * @test
     */
    public function it_has_many_home_team_matches()
    {
        $homeTeam = factory(Team::class)->create();

        $match1 = factory(Match::class)->create([
            'home_team_id' => $homeTeam->id,
        ]);

        $match2 = factory(Match::class)->create([
            'home_team_id' => $homeTeam->id,
        ]);

        $this->assertCount(2, $homeTeam->homeMatches);
        $this->assertInstanceOf(Match::class, $homeTeam->homeMatches[0]);
        $this->assertEquals($match1->id, $homeTeam->homeMatches[0]->id);
        $this->assertInstanceOf(Match::class, $homeTeam->homeMatches[1]);
        $this->assertEquals($match2->id, $homeTeam->homeMatches[1]->id);
    }

    /**
     * @test
     */
    public function it_has_many_away_team_matches()
    {
        $awayTeam = factory(Team::class)->create();

        $match1 = factory(Match::class)->create([
            'away_team_id' => $awayTeam->id,
        ]);

        $match2 = factory(Match::class)->create([
            'away_team_id' => $awayTeam->id,
        ]);

        $this->assertCount(2, $awayTeam->awayMatches);
        $this->assertInstanceOf(Match::class, $awayTeam->awayMatches[0]);
        $this->assertEquals($match1->id, $awayTeam->awayMatches[0]->id);
        $this->assertInstanceOf(Match::class, $awayTeam->awayMatches[1]);
        $this->assertEquals($match2->id, $awayTeam->awayMatches[1]->id);
    }
}
