<?php

namespace Tests\Unit\Models;

use App\Models\Season;
use App\Models\Series;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SeasonTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_uuid()
    {
        $this->assertUuid(factory(Season::class)->create()->uuid);
    }

    /**
     * @test
     */
    public function it_has_many_series()
    {
        $season = factory(Season::class)->create();

        $series1 = factory(Series::class)->create([
            'season_id' => $season->id,
        ]);

        $series2 = factory(Series::class)->create([
            'season_id' => $season->id,
        ]);

        $this->assertCount(2, $season->series);
        $this->assertInstanceOf(Series::class, $season->series[0]);
        $this->assertEquals($series1->id, $season->series[0]->id);
        $this->assertInstanceOf(Series::class, $season->series[1]);
        $this->assertEquals($series2->id, $season->series[1]->id);
    }
}
