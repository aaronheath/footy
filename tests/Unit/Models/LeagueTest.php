<?php

namespace Tests\Unit\Models;

use App\Models\League;
use App\Models\Series;
use App\Models\Team;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LeagueTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_uuid()
    {
        $this->assertUuid(factory(League::class)->create()->uuid);
    }

    /**
     * @test
     */
    public function it_has_many_series()
    {
        $league = factory(League::class)->create();

        $series1 = factory(Series::class)->create([
            'league_id' => $league->id,
        ]);

        $series2 = factory(Series::class)->create([
            'league_id' => $league->id,
        ]);

        $this->assertCount(2, $league->series);
        $this->assertInstanceOf(Series::class, $league->series[0]);
        $this->assertEquals($series1->id, $league->series[0]->id);
        $this->assertInstanceOf(Series::class, $league->series[1]);
        $this->assertEquals($series2->id, $league->series[1]->id);
    }

    /**
     * @test
     */
    public function it_has_many_teams()
    {
        $league = factory(League::class)->create();

        $team1 = factory(Team::class)->create([
            'league_id' => $league->id,
        ]);

        $team2 = factory(Team::class)->create([
            'league_id' => $league->id,
        ]);

        $teams = $league->teams()->orderBy('id')->get();

        $this->assertCount(2, $teams);
        $this->assertInstanceOf(Team::class, $teams[0]);
        $this->assertEquals($team1->id, $teams[0]->id);
        $this->assertInstanceOf(Team::class, $teams[1]);
        $this->assertEquals($team2->id, $teams[1]->id);
    }
}
