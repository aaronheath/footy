<?php

namespace Tests\Unit\Actions;

use App\Models\Match;
use App\Models\Round;
use App\Models\Series;
use App\Models\Team;
use Facades\App\Actions\Ladder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use Tests\TestCase;

class LadderTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function can_generate_ladder_for_small_afl_style_unfinished_season()
    {
        $series = factory(Series::class)->create()->fresh();

        $teamA = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team A']);
        $teamB = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team B']);
        $teamC = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team C']);
        $teamD = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team D']);

        // Rnd 1
        $rnd1 = factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => 1,
        ]);

        $this->buildMatch($rnd1, $teamA, 19, 17, $teamB, 15, 19);
        $this->buildMatch($rnd1, $teamC, 8, 4, $teamD, 10, 7);

        // Rnd 2
        $rnd2 = factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => 2,
        ]);

        $this->buildMatch($rnd2, $teamA, 13, 10, $teamC, 10, 5);
        $this->buildMatch($rnd2, $teamB, 10, 10, $teamD, 11, 4);

        // Rnd 3
        $rnd3 = factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => 3,
        ]);

        $this->buildMatch($rnd3, $teamB, 15, 11, $teamA, 13, 17);

        factory(Match::class)->states('not-played')->create([
            'round_id' => $rnd3->id,
            'home_team_id' => $teamD->id,
            'away_team_id' => $teamC->id,
        ]);

        $ladder = Ladder::series($series)->generate();

        $this->assertEquals(Arr::get($ladder, '0.team.id'), $teamA->id);
        $this->assertEquals(Arr::get($ladder, '0.played'), 3);
        $this->assertEquals(Arr::get($ladder, '0.won'), 2);
        $this->assertEquals(Arr::get($ladder, '0.drawn'), 0);
        $this->assertEquals(Arr::get($ladder, '0.lost'), 1);
        $this->assertEquals(Arr::get($ladder, '0.points'), 8);
        $this->assertEquals(Arr::get($ladder, '0.percentage'), 114.18182);
        $this->assertEquals(Arr::get($ladder, '0.goals_for'), 45);
        $this->assertEquals(Arr::get($ladder, '0.behinds_for'), 44);
        $this->assertEquals(Arr::get($ladder, '0.points_for'), 314);
        $this->assertEquals(Arr::get($ladder, '0.goals_against'), 40);
        $this->assertEquals(Arr::get($ladder, '0.behinds_against'), 35);
        $this->assertEquals(Arr::get($ladder, '0.points_against'), 275);

        $this->assertEquals(Arr::get($ladder, '1.team.id'), $teamD->id);
        $this->assertEquals(Arr::get($ladder, '1.played'), 2);
        $this->assertEquals(Arr::get($ladder, '1.won'), 1);
        $this->assertEquals(Arr::get($ladder, '1.drawn'), 1);
        $this->assertEquals(Arr::get($ladder, '1.lost'), 0);
        $this->assertEquals(Arr::get($ladder, '1.points'), 6);
        $this->assertEquals(Arr::get($ladder, '1.percentage'), 112.29508);
        $this->assertEquals(Arr::get($ladder, '1.goals_for'), 21);
        $this->assertEquals(Arr::get($ladder, '1.behinds_for'), 11);
        $this->assertEquals(Arr::get($ladder, '1.points_for'), 137);
        $this->assertEquals(Arr::get($ladder, '1.goals_against'), 18);
        $this->assertEquals(Arr::get($ladder, '1.behinds_against'), 14);
        $this->assertEquals(Arr::get($ladder, '1.points_against'), 122);

        $this->assertEquals(Arr::get($ladder, '2.team.id'), $teamB->id);
        $this->assertEquals(Arr::get($ladder, '2.played'), 3);
        $this->assertEquals(Arr::get($ladder, '2.won'), 1);
        $this->assertEquals(Arr::get($ladder, '2.drawn'), 1);
        $this->assertEquals(Arr::get($ladder, '2.lost'), 1);
        $this->assertEquals(Arr::get($ladder, '2.points'), 6);
        $this->assertEquals(Arr::get($ladder, '2.percentage'), 94.59459);
        $this->assertEquals(Arr::get($ladder, '2.goals_for'), 40);
        $this->assertEquals(Arr::get($ladder, '2.behinds_for'), 40);
        $this->assertEquals(Arr::get($ladder, '2.points_for'), 280);
        $this->assertEquals(Arr::get($ladder, '2.goals_against'), 43);
        $this->assertEquals(Arr::get($ladder, '2.behinds_against'), 38);
        $this->assertEquals(Arr::get($ladder, '2.points_against'), 296);

        $this->assertEquals(Arr::get($ladder, '3.team.id'), $teamC->id);
        $this->assertEquals(Arr::get($ladder, '3.played'), 2);
        $this->assertEquals(Arr::get($ladder, '3.won'), 0);
        $this->assertEquals(Arr::get($ladder, '3.drawn'), 0);
        $this->assertEquals(Arr::get($ladder, '3.lost'), 2);
        $this->assertEquals(Arr::get($ladder, '3.points'), 0);
        $this->assertEquals(Arr::get($ladder, '3.percentage'), 75.48387);
        $this->assertEquals(Arr::get($ladder, '3.goals_for'), 18);
        $this->assertEquals(Arr::get($ladder, '3.behinds_for'), 9);
        $this->assertEquals(Arr::get($ladder, '3.points_for'), 117);
        $this->assertEquals(Arr::get($ladder, '3.goals_against'), 23);
        $this->assertEquals(Arr::get($ladder, '3.behinds_against'), 17);
        $this->assertEquals(Arr::get($ladder, '3.points_against'), 155);
    }

    /**
     * @test
     */
    public function can_generate_ladder_for_small_sanfl_style_unfinished_season()
    {
        $series = factory(Series::class)->states('sanfl-style')->create();

        $teamA = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team A']);
        $teamB = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team B']);
        $teamC = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team C']);
        $teamD = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team D']);

        // Rnd 1
        $rnd1 = factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => 1,
        ]);

        $this->buildMatch($rnd1, $teamA, 19, 17, $teamB, 15, 19);
        $this->buildMatch($rnd1, $teamC, 8, 4, $teamD, 10, 7);

        // Rnd 2
        $rnd2 = factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => 2,
        ]);

        $this->buildMatch($rnd2, $teamA, 13, 10, $teamC, 10, 5);
        $this->buildMatch($rnd2, $teamB, 10, 10, $teamD, 11, 4);

        // Rnd 3
        $rnd3 = factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => 3,
        ]);

        $this->buildMatch($rnd3, $teamB, 15, 11, $teamA, 13, 17);

        factory(Match::class)->states('not-played')->create([
            'round_id' => $rnd3->id,
            'home_team_id' => $teamD->id,
            'away_team_id' => $teamC->id,
        ]);

        $ladder = Ladder::series($series)->generate($series);

        $this->assertEquals(Arr::get($ladder, '0.team.id'), $teamA->id);
        $this->assertEquals(Arr::get($ladder, '0.played'), 3);
        $this->assertEquals(Arr::get($ladder, '0.won'), 2);
        $this->assertEquals(Arr::get($ladder, '0.drawn'), 0);
        $this->assertEquals(Arr::get($ladder, '0.lost'), 1);
        $this->assertEquals(Arr::get($ladder, '0.points'), 4);
        $this->assertEquals(Arr::get($ladder, '0.percentage'), 53.3107);
        $this->assertEquals(Arr::get($ladder, '0.goals_for'), 45);
        $this->assertEquals(Arr::get($ladder, '0.behinds_for'), 44);
        $this->assertEquals(Arr::get($ladder, '0.points_for'), 314);
        $this->assertEquals(Arr::get($ladder, '0.goals_against'), 40);
        $this->assertEquals(Arr::get($ladder, '0.behinds_against'), 35);
        $this->assertEquals(Arr::get($ladder, '0.points_against'), 275);

        $this->assertEquals(Arr::get($ladder, '1.team.id'), $teamD->id);
        $this->assertEquals(Arr::get($ladder, '1.played'), 2);
        $this->assertEquals(Arr::get($ladder, '1.won'), 1);
        $this->assertEquals(Arr::get($ladder, '1.drawn'), 1);
        $this->assertEquals(Arr::get($ladder, '1.lost'), 0);
        $this->assertEquals(Arr::get($ladder, '1.points'), 3);
        $this->assertEquals(Arr::get($ladder, '1.percentage'), 52.89575);
        $this->assertEquals(Arr::get($ladder, '1.goals_for'), 21);
        $this->assertEquals(Arr::get($ladder, '1.behinds_for'), 11);
        $this->assertEquals(Arr::get($ladder, '1.points_for'), 137);
        $this->assertEquals(Arr::get($ladder, '1.goals_against'), 18);
        $this->assertEquals(Arr::get($ladder, '1.behinds_against'), 14);
        $this->assertEquals(Arr::get($ladder, '1.points_against'), 122);

        $this->assertEquals(Arr::get($ladder, '2.team.id'), $teamB->id);
        $this->assertEquals(Arr::get($ladder, '2.played'), 3);
        $this->assertEquals(Arr::get($ladder, '2.won'), 1);
        $this->assertEquals(Arr::get($ladder, '2.drawn'), 1);
        $this->assertEquals(Arr::get($ladder, '2.lost'), 1);
        $this->assertEquals(Arr::get($ladder, '2.points'), 3);
        $this->assertEquals(Arr::get($ladder, '2.percentage'), 48.61111);
        $this->assertEquals(Arr::get($ladder, '2.goals_for'), 40);
        $this->assertEquals(Arr::get($ladder, '2.behinds_for'), 40);
        $this->assertEquals(Arr::get($ladder, '2.points_for'), 280);
        $this->assertEquals(Arr::get($ladder, '2.goals_against'), 43);
        $this->assertEquals(Arr::get($ladder, '2.behinds_against'), 38);
        $this->assertEquals(Arr::get($ladder, '2.points_against'), 296);

        $this->assertEquals(Arr::get($ladder, '3.team.id'), $teamC->id);
        $this->assertEquals(Arr::get($ladder, '3.played'), 2);
        $this->assertEquals(Arr::get($ladder, '3.won'), 0);
        $this->assertEquals(Arr::get($ladder, '3.drawn'), 0);
        $this->assertEquals(Arr::get($ladder, '3.lost'), 2);
        $this->assertEquals(Arr::get($ladder, '3.points'), 0);
        $this->assertEquals(Arr::get($ladder, '3.percentage'), 43.01471);
        $this->assertEquals(Arr::get($ladder, '3.goals_for'), 18);
        $this->assertEquals(Arr::get($ladder, '3.behinds_for'), 9);
        $this->assertEquals(Arr::get($ladder, '3.points_for'), 117);
        $this->assertEquals(Arr::get($ladder, '3.goals_against'), 23);
        $this->assertEquals(Arr::get($ladder, '3.behinds_against'), 17);
        $this->assertEquals(Arr::get($ladder, '3.points_against'), 155);
    }

    /**
     * @test
     */
    public function it_caches_ladder_when_requested()
    {
        $series = factory(Series::class)->states('sanfl-style')->create();

        $teamA = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team A']);
        $teamB = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team B']);
        $teamC = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team C']);
        $teamD = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team D']);

        // Rnd 1
        $rnd1 = factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => 1,
        ]);

        $this->buildMatch($rnd1, $teamA, 19, 17, $teamB, 15, 19);
        $this->buildMatch($rnd1, $teamC, 8, 4, $teamD, 10, 7);

        // Rnd 2
        $rnd2 = factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => 2,
        ]);

        $this->buildMatch($rnd2, $teamA, 13, 10, $teamC, 10, 5);
        $this->buildMatch($rnd2, $teamB, 10, 10, $teamD, 11, 4);

        // Rnd 3
        $rnd3 = factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => 3,
        ]);

        $this->buildMatch($rnd3, $teamB, 15, 11, $teamA, 13, 17);

        factory(Match::class)->states('not-played')->create([
            'round_id' => $rnd3->id,
            'home_team_id' => $teamD->id,
            'away_team_id' => $teamC->id,
        ]);

        Ladder::series($series)->cache()->generate($series);

        $ladder = cache()->get('ladder_'.$series->uuid);

        $this->assertEquals(Arr::get($ladder, '0.team.id'), $teamA->id);
        $this->assertEquals(Arr::get($ladder, '0.played'), 3);
        $this->assertEquals(Arr::get($ladder, '0.won'), 2);
        $this->assertEquals(Arr::get($ladder, '0.drawn'), 0);
        $this->assertEquals(Arr::get($ladder, '0.lost'), 1);
        $this->assertEquals(Arr::get($ladder, '0.points'), 4);
        $this->assertEquals(Arr::get($ladder, '0.percentage'), 53.3107);
        $this->assertEquals(Arr::get($ladder, '0.goals_for'), 45);
        $this->assertEquals(Arr::get($ladder, '0.behinds_for'), 44);
        $this->assertEquals(Arr::get($ladder, '0.points_for'), 314);
        $this->assertEquals(Arr::get($ladder, '0.goals_against'), 40);
        $this->assertEquals(Arr::get($ladder, '0.behinds_against'), 35);
        $this->assertEquals(Arr::get($ladder, '0.points_against'), 275);

        $this->assertEquals(Arr::get($ladder, '1.team.id'), $teamD->id);
        $this->assertEquals(Arr::get($ladder, '1.played'), 2);
        $this->assertEquals(Arr::get($ladder, '1.won'), 1);
        $this->assertEquals(Arr::get($ladder, '1.drawn'), 1);
        $this->assertEquals(Arr::get($ladder, '1.lost'), 0);
        $this->assertEquals(Arr::get($ladder, '1.points'), 3);
        $this->assertEquals(Arr::get($ladder, '1.percentage'), 52.89575);
        $this->assertEquals(Arr::get($ladder, '1.goals_for'), 21);
        $this->assertEquals(Arr::get($ladder, '1.behinds_for'), 11);
        $this->assertEquals(Arr::get($ladder, '1.points_for'), 137);
        $this->assertEquals(Arr::get($ladder, '1.goals_against'), 18);
        $this->assertEquals(Arr::get($ladder, '1.behinds_against'), 14);
        $this->assertEquals(Arr::get($ladder, '1.points_against'), 122);

        $this->assertEquals(Arr::get($ladder, '2.team.id'), $teamB->id);
        $this->assertEquals(Arr::get($ladder, '2.played'), 3);
        $this->assertEquals(Arr::get($ladder, '2.won'), 1);
        $this->assertEquals(Arr::get($ladder, '2.drawn'), 1);
        $this->assertEquals(Arr::get($ladder, '2.lost'), 1);
        $this->assertEquals(Arr::get($ladder, '2.points'), 3);
        $this->assertEquals(Arr::get($ladder, '2.percentage'), 48.61111);
        $this->assertEquals(Arr::get($ladder, '2.goals_for'), 40);
        $this->assertEquals(Arr::get($ladder, '2.behinds_for'), 40);
        $this->assertEquals(Arr::get($ladder, '2.points_for'), 280);
        $this->assertEquals(Arr::get($ladder, '2.goals_against'), 43);
        $this->assertEquals(Arr::get($ladder, '2.behinds_against'), 38);
        $this->assertEquals(Arr::get($ladder, '2.points_against'), 296);

        $this->assertEquals(Arr::get($ladder, '3.team.id'), $teamC->id);
        $this->assertEquals(Arr::get($ladder, '3.played'), 2);
        $this->assertEquals(Arr::get($ladder, '3.won'), 0);
        $this->assertEquals(Arr::get($ladder, '3.drawn'), 0);
        $this->assertEquals(Arr::get($ladder, '3.lost'), 2);
        $this->assertEquals(Arr::get($ladder, '3.points'), 0);
        $this->assertEquals(Arr::get($ladder, '3.percentage'), 43.01471);
        $this->assertEquals(Arr::get($ladder, '3.goals_for'), 18);
        $this->assertEquals(Arr::get($ladder, '3.behinds_for'), 9);
        $this->assertEquals(Arr::get($ladder, '3.points_for'), 117);
        $this->assertEquals(Arr::get($ladder, '3.goals_against'), 23);
        $this->assertEquals(Arr::get($ladder, '3.behinds_against'), 17);
        $this->assertEquals(Arr::get($ladder, '3.points_against'), 155);
    }

    /** @test */
    public function it_does_not_add_team_form_match_when_either_team_is_null()
    {
        $series = factory(Series::class)->create()->fresh();

        $teamA = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team A']);
        $teamB = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team B']);

        $rnd1 = factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => 1,
        ]);

        $this->buildMatch($rnd1, $teamA, 19, 17, $teamB, 15, 19);

        Match::create(['round_id' => $rnd1->id]);

        $ladder = Ladder::series($series)->generate();

        $this->assertCount(2, $ladder);
    }

    protected function buildMatch($round, $homeTeam, $homeGoals, $homeBehinds, $awayTeam, $awayGoals, $awayBehinds)
    {
        return factory(Match::class)->create([
            'round_id' => $round->id,
            'home_team_id' => $homeTeam->id,
            'home_team_goals' => $homeGoals,
            'home_team_behinds' => $homeBehinds,
            'home_team_points' => $homeGoals * 6 + $homeBehinds,
            'away_team_id' => $awayTeam->id,
            'away_team_goals' => $awayGoals,
            'away_team_behinds' => $awayBehinds,
            'away_team_points' => $awayGoals * 6 + $awayBehinds,
        ]);
    }
}
