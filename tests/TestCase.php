<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use PHPUnit\Framework\Constraint\RegularExpression;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, WithFaker;

    public static function assertUuid($string)
    {
        static::assertThat($string, static::logicalNot(static::isNull()), 'Expected UUID4 string but received null');

        static::assertThat(
            $string,
            new RegularExpression('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/'),
            'Invalid UUID4 format'
        );
    }

    public function stubText(int $minLength)
    {
        do {
            $response = $this->faker->text(intval($minLength * 1.2));
        } while (strlen($response) < $minLength);

        return $response;
    }

    public function verifySimplePagination($model, $path)
    {
        factory($model, 20)->create();

        $response = $this->getJson(
            $this->paginatePath($path, 2)
        );

        $response->assertOk();

        $response->assertJsonCount(5, 'data');

        $response->assertJsonFragment([
            'links' => [
                'first' => $this->paginatePath($path, 1),
                'last' => $this->paginatePath($path, 2),
                'prev' => $this->paginatePath($path, 1),
                'next' => null,
            ],
            'meta' => [
                'current_page' => 2,
                'from' => 16,
                'last_page' => 2,
                'path' => $this->paginatePath($path),
                'per_page' => 15,
                'to' => 20,
                'total' => 20,
            ],
        ]);
    }

    public function paginatePath($path, $pageNumber = null)
    {
        if (is_null($pageNumber)) {
            return config('app.url').$path;
        }

        return config('app.url').$path.'?page='.$pageNumber;
    }

    public function responseJsonValue(TestResponse $response, string $path)
    {
        return Arr::get(json_decode($response->getContent(), true), $path);
    }
}
