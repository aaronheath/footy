<?php

namespace Tests\Feature;

use App\Models\Match;
use App\Models\State;
use App\Models\User;
use App\Models\Venue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class VenueTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function can_view_venue()
    {
        $this->withoutExceptionHandling();

        $state = factory(State::class)->create();
        $venue = factory(Venue::class)->create(['state_id' => $state->id])->fresh();

        $response = $this->getJson('/api/venues/'.$venue->uuid);

        $response->assertOk();

        $response->assertExactJson([
            'data' => [
                'id' => $venue->uuid,
                'abbreviation' => $venue->abbreviation,
                'name' => $venue->name,
                'capacity' => $venue->capacity,
                'latitude' => $venue->latitude,
                'longitude' => $venue->longitude,
                'state' => [
                    'id' => $venue->state->uuid,
                    'abbreviation' => $venue->state->abbreviation,
                    'name' => $venue->state->name,
                    'timezone' => $venue->state->timezone,
                    'created_at' => $venue->state->created_at->toDateTimeString(),
                    'updated_at' => $venue->state->updated_at->toDateTimeString(),
                ],
                'created_at' => $venue->created_at->toDateTimeString(),
                'updated_at' => $venue->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function can_view_all_venues()
    {
        $state = factory(State::class)->create();

        $mcg = factory(Venue::class)->create([
            'name' => 'Melbourne Cricket Ground',
            'state_id' => $state->id,
        ]);

        $gmhba = factory(Venue::class)->create([
            'name' => 'GMHBA Stadium',
            'state_id' => $state->id,
        ]);

        $marvel = factory(Venue::class)->create([
            'name' => 'Marvel Stadium',
            'state_id' => $state->id,
        ]);

        $response = $this->getJson('/api/venues');

        $response->assertOk();

        $response->assertJsonCount(3, 'data');

        $this->assertEquals($gmhba->name, $response->decodeResponseJson('data.0.name'));
        $this->assertEquals($marvel->name, $response->decodeResponseJson('data.1.name'));
        $this->assertEquals($mcg->name, $response->decodeResponseJson('data.2.name'));

        $response->assertJsonFragment([
            'id' => $mcg->uuid,
            'abbreviation' => $mcg->abbreviation,
            'name' => $mcg->name,
            'capacity' => $mcg->capacity,
            'latitude' => $mcg->latitude,
            'longitude' => $mcg->longitude,
            'state' => [
                'id' => $mcg->state->uuid,
                'abbreviation' => $mcg->state->abbreviation,
                'name' => $mcg->state->name,
                'timezone' => $mcg->state->timezone,
                'created_at' => $mcg->state->created_at->toDateTimeString(),
                'updated_at' => $mcg->state->updated_at->toDateTimeString(),
            ],
            'created_at' => $mcg->created_at->toDateTimeString(),
            'updated_at' => $mcg->updated_at->toDateTimeString(),
        ]);
    }

    /**
     * @test
     */
    public function it_has_functional_pagination_when_viewing_all()
    {
        $this->verifySimplePagination(Venue::class, '/api/venues');
    }

    /**
     * @test
     */
    public function authenticated_user_can_create_venue()
    {
        $this->withoutExceptionHandling();

        $state = factory(State::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/venues', [
            'state_id' => $state->uuid,
            'abbreviation' => 'MCG',
            'name' => 'Melbourne Cricket Ground',
            'capacity' => 100024,
            'latitude' => -37.8199952,
            'longitude' => 144.9829286,
        ]);

        $response->assertStatus(201);

        $venue = Venue::with(['state'])->first();

        $response->assertExactJson([
            'data' => [
                'id' => $venue->uuid,
                'abbreviation' => $venue->abbreviation,
                'name' => $venue->name,
                'capacity' => $venue->capacity,
                'latitude' => $venue->latitude,
                'longitude' => $venue->longitude,
                'state' => [
                    'id' => $venue->state->uuid,
                    'abbreviation' => $venue->state->abbreviation,
                    'name' => $venue->state->name,
                    'timezone' => $venue->state->timezone,
                    'created_at' => $venue->state->created_at->toDateTimeString(),
                    'updated_at' => $venue->state->updated_at->toDateTimeString(),
                ],
                'created_at' => $venue->created_at->toDateTimeString(),
                'updated_at' => $venue->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_required_fields()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/venues', []);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'state_id' => [
                    'The state id field is required.',
                ],
                'abbreviation' => [
                    'The abbreviation field is required.',
                ],
                'name' => [
                    'The name field is required.',
                ],
                'capacity' => [
                    'The capacity field is required.',
                ],
                'latitude' => [
                    'The latitude field is required.',
                ],
                'longitude' => [
                    'The longitude field is required.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_fields_with_unique_requirement()
    {
        $state = factory(State::class)->create();

        factory(Venue::class)->create([
            'abbreviation' => 'MCG',
            'name' => 'Melbourne Cricket Ground',
        ]);

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/venues', [
            'state_id' => $state->uuid,
            'abbreviation' => 'MCG',
            'name' => 'Melbourne Cricket Ground',
            'capacity' => 100024,
            'latitude' => -37.8199952,
            'longitude' => 144.9829286,
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation has already been taken.',
                ],
                'name' => [
                    'The name has already been taken.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_basic_attributes()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/venues', [
            'state_id' => 'xxx',
            'abbreviation' => 'xxxxx',
            'name' => 'This is a string that is greater than 50 characters in length.',
            'capacity' => 300001,
            'latitude' => -90.1,
            'longitude' => 180.1,
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'state_id' => [
                    'The selected state id is invalid.',
                ],
                'abbreviation' => [
                    'The abbreviation may not be greater than 4 characters.',
                ],
                'name' => [
                    'The name may not be greater than 50 characters.',
                ],
                'capacity' => [
                    'The capacity must be between 1 and 300000.',
                ],
                'latitude' => [
                    'The latitude must be between -90 and 90.',
                ],
                'longitude' => [
                    'The longitude must be between -180 and 180.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_create_venue()
    {
        $state = factory(State::class)->create();

        $response = $this->postJson('/api/venues', [
            'state_id' => $state->uuid,
            'abbreviation' => 'MCG',
            'name' => 'Melbourne Cricket Ground',
            'capacity' => 100024,
            'latitude' => -37.8199952,
            'longitude' => 144.9829286,
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_update_venues()
    {
        $this->actingAs(factory(User::class)->create());

        $venue = factory(Venue::class)->create();

        $state = factory(State::class)->create();

        $data = [
            'state_id' => $state->uuid,
            'abbreviation' => 'MCG',
            'name' => 'Melbourne Cricket Ground',
            'capacity' => 100024,
            'latitude' => -37.8199952,
            'longitude' => 144.9829286,
        ];

        $response = $this->putJson('/api/venues/'.$venue->uuid, $data);

        $response->assertOk();

        $venue->refresh();

        $response->assertExactJson([
            'data' => [
                'id' => $venue->uuid,
                'abbreviation' => $data['abbreviation'],
                'name' => $data['name'],
                'capacity' => $data['capacity'],
                'latitude' => $data['latitude'],
                'longitude' => $data['longitude'],
                'state' => [
                    'id' => $state->uuid,
                    'abbreviation' => $state->abbreviation,
                    'name' => $state->name,
                    'timezone' => $state->timezone,
                    'created_at' => $state->created_at->toDateTimeString(),
                    'updated_at' => $state->updated_at->toDateTimeString(),
                ],
                'created_at' => $venue->created_at->toDateTimeString(),
                'updated_at' => $venue->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_name_is_unique()
    {
        $this->actingAs(factory(User::class)->create());

        factory(Venue::class)->create([
            'abbreviation' => 'MCG',
            'name' => 'Melbourne Cricket Ground',
        ]);

        $venue = factory(Venue::class)->create();

        $data = [
            'abbreviation' => 'MCG',
            'name' => 'Melbourne Cricket Ground',
        ];

        $response = $this->putJson('/api/venues/'.$venue->uuid, $data);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation has already been taken.',
                ],
                'name' => [
                    'The name has already been taken.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_basic_attributes()
    {
        $venue = factory(Venue::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->putJson('/api/venues/'.$venue->uuid, [
            'state_id' => 'xxx',
            'abbreviation' => 'xxxxx',
            'name' => 'This is a string that is greater than 50 characters in length.',
            'capacity' => 300001,
            'latitude' => -90.1,
            'longitude' => 180.1,
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'state_id' => [
                    'The selected state id is invalid.',
                ],
                'abbreviation' => [
                    'The abbreviation may not be greater than 4 characters.',
                ],
                'name' => [
                    'The name may not be greater than 50 characters.',
                ],
                'capacity' => [
                    'The capacity must be between 1 and 300000.',
                ],
                'latitude' => [
                    'The latitude must be between -90 and 90.',
                ],
                'longitude' => [
                    'The longitude must be between -180 and 180.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_update_venue()
    {
        $venue = factory(Venue::class)->create();

        $response = $this->putJson('/api/venues/'.$venue->uuid, ['name' => 'xxx']);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_delete_venue()
    {
        $this->actingAs(factory(User::class)->create());

        $venue = factory(Venue::class)->create();

        $response = $this->delete('/api/venues/'.$venue->uuid);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('venues', ['id' => $venue->id]);
    }

    /**
     * @test
     */
    public function authenticated_user_cannot_delete_used_venue()
    {
        $this->actingAs(factory(User::class)->create());

        $match = factory(Match::class)->create();

        $response = $this->delete('/api/venues/'.$match->venue->uuid);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'Unable to delete venue as it has active dependants.',
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_delete_venue()
    {
        $venue = factory(Venue::class)->create();

        $response = $this->deleteJson('/api/venues/'.$venue->uuid);

        $response->assertStatus(401);
    }
}
