<?php

namespace Tests\Feature;

use App\Models\Match;
use App\Models\Round;
use App\Models\Series;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RoundTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function can_view_individual()
    {
        $round = factory(Round::class)->create();

        $response = $this->getJson('/api/rounds/'.$round->uuid);

        $response->assertOk();

        $response->assertExactJson([
            'data' => [
                'id' => $round->uuid,
                'round' => $round->round,
                'name' => $round->name,
                'series' => [
                    'id' => $round->series->uuid,
                    'name' => $round->series->name,
                    'points_for_win' => $round->series->points_for_win,
                    'percentage_divisor' => $round->series->percentage_divisor,
                    'created_at' => $round->series->created_at->toDateTimeString(),
                    'updated_at' => $round->series->updated_at->toDateTimeString(),
                ],
                'created_at' => $round->created_at->toDateTimeString(),
                'updated_at' => $round->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function can_view_all()
    {
        $series = factory(Series::class, 2)->create();

        $seriesA1 = factory(Round::class)->create([
            'round' => 1,
            'name' => null,
            'series_id' => $series[0]->id,
        ]);

        $seriesB1 = factory(Round::class)->create([
            'round' => 1,
            'name' => null,
            'series_id' => $series[1]->id,
        ]);

        $seriesA2 = factory(Round::class)->create([
            'round' => 2,
            'name' => null,
            'series_id' => $series[0]->id,
        ]);

        $response = $this->getJson('/api/rounds');

        $response->assertOk();

        $response->assertJsonCount(3, 'data');

        $this->assertEquals($seriesA1->round, $response->decodeResponseJson('data.0.round'));
        $this->assertEquals($seriesA2->round, $response->decodeResponseJson('data.1.round'));
        $this->assertEquals($seriesB1->round, $response->decodeResponseJson('data.2.round'));

        $response->assertJsonFragment([
            'id' => $seriesA1->uuid,
            'round' => $seriesA1->round,
            'name' => $seriesA1->name,
            'series' => [
                'id' => $seriesA1->series->uuid,
                'name' => $seriesA1->series->name,
                'points_for_win' => $seriesA1->series->points_for_win,
                'percentage_divisor' => $seriesA1->series->percentage_divisor,
                'created_at' => $seriesA1->series->created_at->toDateTimeString(),
                'updated_at' => $seriesA1->series->updated_at->toDateTimeString(),
            ],
            'created_at' => $seriesA1->created_at->toDateTimeString(),
            'updated_at' => $seriesA1->updated_at->toDateTimeString(),
        ]);
    }

    /**
     * @test
     */
    public function it_has_functional_pagination_when_viewing_all()
    {
        $this->verifySimplePagination(Round::class, '/api/rounds');
    }

    /**
     * @test
     */
    public function authenticated_user_can_create()
    {
        $series = factory(Series::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/rounds', [
            'series_id' => $series->uuid,
            'round' => 'Week 1',
            'name' => 'Qualifying and Elimination Finals',
        ]);

        $response->assertStatus(201);

        $round = Round::with(['series'])->first();

        $response->assertExactJson([
            'data' => [
                'id' => $round->uuid,
                'round' => $round->round,
                'name' => $round->name,
                'series' => [
                    'id' => $round->series->uuid,
                    'name' => $round->series->name,
                    'points_for_win' => $round->series->points_for_win,
                    'percentage_divisor' => $round->series->percentage_divisor,
                    'created_at' => $round->series->created_at->toDateTimeString(),
                    'updated_at' => $round->series->updated_at->toDateTimeString(),
                ],
                'created_at' => $round->created_at->toDateTimeString(),
                'updated_at' => $round->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_required_fields()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/rounds', []);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'series_id' => [
                    'The series id field is required.',
                ],
                'round' => [
                    'The round field is required.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_fields_with_unique_requirement()
    {
        $series = factory(Series::class)->create();

        factory(Round::class)->create([
            'series_id' => $series->id,
            'round' => '1',
        ]);

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/rounds', [
            'series_id' => $series->uuid,
            'round' => '1',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'round' => [
                    'The round is already associated with series.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_basic_attributes()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/rounds', [
            'series_id' => 'xxx',
            'round' => $this->stubText(51),
            'name' => $this->stubText(51),
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'series_id' => [
                    'The selected series id is invalid.',
                ],
                'round' => [
                    'The round may not be greater than 50 characters.',
                ],
                'name' => [
                    'The name may not be greater than 50 characters.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_create()
    {
        $series = factory(Series::class)->create();

        $response = $this->postJson('/api/rounds', [
            'series_id' => $series->id,
            'round' => 'Week 1',
            'name' => 'Qualifying and Elimination Finals',
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_update_round()
    {
        $this->actingAs(factory(User::class)->create());

        $round = factory(Round::class)->create();

        $series = factory(Series::class)->create()->fresh();

        $data = [
            'series_id' => $series->uuid,
            'round' => 'Week 1',
            'name' => 'Qualifying and Elimination Finals',
        ];

        $response = $this->putJson('/api/rounds/'.$round->uuid, $data);

        $response->assertOk();

        $round->refresh();

        $response->assertExactJson([
            'data' => [
                'id' => $round->uuid,
                'round' => $data['round'],
                'name' => $data['name'],
                'series' => [
                    'id' => $series->uuid,
                    'name' => $series->name,
                    'points_for_win' => $series->points_for_win,
                    'percentage_divisor' => $series->percentage_divisor,
                    'created_at' => $series->created_at->toDateTimeString(),
                    'updated_at' => $series->updated_at->toDateTimeString(),
                ],
                'created_at' => $round->created_at->toDateTimeString(),
                'updated_at' => $round->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function can_update_nullable_values_to_null()
    {
        $this->actingAs(factory(User::class)->create());

        $round = factory(Round::class)->create();

        $data = [
            'name' => null,
        ];

        $response = $this->putJson('/api/rounds/'.$round->uuid, $data);

        $response->assertOk();

        $response->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_unique_requirements()
    {
        $this->actingAs(factory(User::class)->create());

        $original = factory(Round::class)->create([
            'round' => '1',
        ]);

        $round = factory(Round::class)->create([
            'series_id' => $original->series->id,
        ]);

        $data = [
            'round' => '1',
        ];

        $response = $this->putJson('/api/rounds/'.$round->uuid, $data);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'round' => [
                    'The round is already associated with series.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_basic_attributes()
    {
        $round = factory(Round::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->putJson('/api/rounds/'.$round->uuid, [
            'series_id' => 'xxx',
            'round' => $this->stubText(51),
            'name' => $this->stubText(51),
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'series_id' => [
                    'The selected series id is invalid.',
                ],
                'round' => [
                    'The round may not be greater than 50 characters.',
                ],
                'name' => [
                    'The name may not be greater than 50 characters.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_update()
    {
        $round = factory(Round::class)->create();

        $response = $this->putJson('/api/rounds/'.$round->uuid, ['round' => 'xxx']);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_delete()
    {
        $this->actingAs(factory(User::class)->create());

        $round = factory(Round::class)->create();

        $response = $this->delete('/api/rounds/'.$round->uuid);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('rounds', ['id' => $round->id]);
    }

    /**
     * @test
     */
    public function authenticated_user_cannot_delete_used_round()
    {
        $this->actingAs(factory(User::class)->create());

        $match = factory(Match::class)->create();

        $response = $this->delete('/api/rounds/'.$match->round->uuid);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'Unable to delete round as it has active dependants.',
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_delete()
    {
        $round = factory(Round::class)->create();

        $response = $this->deleteJson('/api/rounds/'.$round->uuid);

        $response->assertStatus(401);
    }
}
