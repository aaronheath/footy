<?php

namespace Tests\Feature;

use App\Models\League;
use App\Models\Round;
use App\Models\Season;
use App\Models\Series;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SeriesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function can_view_series()
    {
        $this->withoutExceptionHandling();

        $league = factory(League::class)->create();
        $series = factory(Series::class)->create(['league_id' => $league->id])->fresh();

        $response = $this->getJson('/api/series/'.$series->uuid);

        $response->assertOk();

        $response->assertExactJson([
            'data' => [
                'id' => $series->uuid,
                'name' => $series->name,
                'points_for_win' => $series->points_for_win,
                'percentage_divisor' => $series->percentage_divisor,
                'season' => [
                    'id' => $series->season->uuid,
                    'year' => $series->season->year,
                    'created_at' => $series->season->created_at->toDateTimeString(),
                    'updated_at' => $series->season->updated_at->toDateTimeString(),
                ],
                'league' => [
                    'id' => $series->league->uuid,
                    'abbreviation' => $series->league->abbreviation,
                    'name' => $series->league->name,
                    'created_at' => $series->league->created_at->toDateTimeString(),
                    'updated_at' => $series->league->updated_at->toDateTimeString(),
                ],
                'created_at' => $series->created_at->toDateTimeString(),
                'updated_at' => $series->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function can_view_all_series()
    {
        $league = factory(League::class)->create();

        factory(Series::class)->create(['name' => '2019 AFL Pre-Season']);

        $series = factory(Series::class)
            ->create(['name' => '2019 AFL Premiership Series', 'league_id' => $league->id])
            ->fresh();

        factory(Series::class)->create(['name' => '2019 AFL Final Series']);

        $response = $this->getJson('/api/series');

        $response->assertOk();

        $response->assertJsonCount(3, 'data');

        $this->assertEquals('2019 AFL Final Series', $response->decodeResponseJson('data.0.name'));
        $this->assertEquals('2019 AFL Pre-Season', $response->decodeResponseJson('data.1.name'));
        $this->assertEquals('2019 AFL Premiership Series', $response->decodeResponseJson('data.2.name'));

        $response->assertJsonFragment([
            'id' => $series->uuid,
            'name' => $series->name,
            'points_for_win' => $series->points_for_win,
            'percentage_divisor' => $series->percentage_divisor,
            'season' => [
                'id' => $series->season->uuid,
                'year' => $series->season->year,
                'created_at' => $series->season->created_at->toDateTimeString(),
                'updated_at' => $series->season->updated_at->toDateTimeString(),
            ],
            'league' => [
                'id' => $series->league->uuid,
                'abbreviation' => $series->league->abbreviation,
                'name' => $series->league->name,
                'created_at' => $series->league->created_at->toDateTimeString(),
                'updated_at' => $series->league->updated_at->toDateTimeString(),
            ],
            'created_at' => $series->created_at->toDateTimeString(),
            'updated_at' => $series->updated_at->toDateTimeString(),
        ]);
    }

    /**
     * @test
     */
    public function can_view_filtered_series_by_league()
    {
        $league = factory(League::class)->create();

        $series1 = factory(Series::class)
            ->create(['name' => '2019 AFL Premiership Series', 'league_id' => $league->id])
            ->fresh();

        $series2 = factory(Series::class)
            ->create(['name' => '2020 AFL Premiership Series', 'league_id' => $league->id])
            ->fresh();

        factory(Series::class, 2)->create();

        $response = $this->getJson('/api/series?filter[league]='.$league->uuid);

        $response->assertOk();

        $response->assertJsonCount(2, 'data');

        $this->assertEquals('2019 AFL Premiership Series', $response->decodeResponseJson('data.0.name'));
        $this->assertEquals('2020 AFL Premiership Series', $response->decodeResponseJson('data.1.name'));
    }

    /**
     * @test
     */
    public function it_has_functional_pagination_when_viewing_all()
    {
        $this->verifySimplePagination(Series::class, '/api/series');
    }

    /**
     * @test
     */
    public function authenticated_user_can_create_series()
    {
        $this->withoutExceptionHandling();

        $season = factory(Season::class)->create();
        $league = factory(League::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/series', [
            'season_id' => $season->uuid,
            'league_id' => $league->uuid,
            'name' => '2019 AFL Premiership Series',
            'points_for_win' => 2,
            'percentage_divisor' => 'total_points',
        ]);

        $response->assertStatus(201);

        $series = Series::with(['league', 'season'])->first();

        $response->assertExactJson([
            'data' => [
                'id' => $series->uuid,
                'name' => $series->name,
                'points_for_win' => $series->points_for_win,
                'percentage_divisor' => $series->percentage_divisor,
                'season' => [
                    'id' => $series->season->uuid,
                    'year' => $series->season->year,
                    'created_at' => $series->season->created_at->toDateTimeString(),
                    'updated_at' => $series->season->updated_at->toDateTimeString(),
                ],
                'league' => [
                    'id' => $series->league->uuid,
                    'abbreviation' => $series->league->abbreviation,
                    'name' => $series->league->name,
                    'created_at' => $series->league->created_at->toDateTimeString(),
                    'updated_at' => $series->league->updated_at->toDateTimeString(),
                ],
                'created_at' => $series->created_at->toDateTimeString(),
                'updated_at' => $series->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_required_fields()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/series', []);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'season_id' => [
                    'The season id field is required.',
                ],
                'league_id' => [
                    'The league id field is required.',
                ],
                'name' => [
                    'The name field is required.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_name_is_unique()
    {
        factory(Series::class)->create(['name' => '2019 AFL Premiership Series']);

        $season = factory(Season::class)->create();
        $league = factory(League::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/series', [
            'season_id' => $season->uuid,
            'league_id' => $league->uuid,
            'name' => '2019 AFL Premiership Series',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => [
                    'The name has already been taken.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_basic_attributes()
    {
        factory(Series::class)->create(['name' => '2019 AFL Premiership Series']);

        $season = factory(Season::class)->create();
        $league = factory(League::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/series', [
            'season_id' => 'xxx',
            'league_id' => 'xxx',
            'name' => 'This is a string that is greater than 50 characters in length.',
            'points_for_win' => 8,
            'percentage_divisor' => 'xxx',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'season_id' => [
                    'The selected season id is invalid.',
                ],
                'league_id' => [
                    'The selected league id is invalid.',
                ],
                'name' => [
                    'The name may not be greater than 50 characters.',
                ],
                'points_for_win' => [
                    'The selected points for win is invalid.',
                ],
                'percentage_divisor' => [
                    'The selected percentage divisor is invalid.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_create_series()
    {
        $season = factory(Season::class)->create();
        $league = factory(League::class)->create();

        $response = $this->postJson('/api/series', [
            'season_id' => $season->uuid,
            'league_id' => $league->uuid,
            'name' => '2019 AFL Premiership Series',
            'points_for_win' => 2,
            'percentage_divisor' => 'total_points',
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_update_series()
    {
        $this->actingAs(factory(User::class)->create());

        $series = factory(Series::class)->create([
            'league_id' => factory(League::class)->create()->id,
        ]);

        $season = factory(Season::class)->create();
        $league = factory(League::class)->create();

        $data = [
            'season_id' => $season->uuid,
            'league_id' => $league->uuid,
            'name' => '2019 AFL Premiership Season',
            'points_for_win' => 2,
            'percentage_divisor' => 'total_points',
        ];

        $response = $this->putJson('/api/series/'.$series->uuid, $data);

        $response->assertOk();

        $series->refresh();

        $response->assertExactJson([
            'data' => [
                'id' => $series->uuid,
                'name' => $data['name'],
                'points_for_win' => $data['points_for_win'],
                'percentage_divisor' => $data['percentage_divisor'],
                'season' => [
                    'id' => $season->uuid,
                    'year' => $season->year,
                    'created_at' => $season->created_at->toDateTimeString(),
                    'updated_at' => $season->updated_at->toDateTimeString(),
                ],
                'league' => [
                    'id' => $league->uuid,
                    'abbreviation' => $league->abbreviation,
                    'name' => $league->name,
                    'created_at' => $league->created_at->toDateTimeString(),
                    'updated_at' => $league->updated_at->toDateTimeString(),
                ],
                'created_at' => $series->created_at->toDateTimeString(),
                'updated_at' => $series->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_name_is_unique()
    {
        $this->actingAs(factory(User::class)->create());

        factory(Series::class)->create([
            'name' => '2019 AFL Premiership Season',
        ]);

        $series = factory(Series::class)->create([
            'league_id' => factory(League::class)->create()->id,
        ]);

        $data = [
            'name' => '2019 AFL Premiership Season',
        ];

        $response = $this->putJson('/api/series/'.$series->uuid, $data);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => [
                    'The name has already been taken.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_basic_attributes()
    {
        $series = factory(Series::class)->create([
            'league_id' => factory(League::class)->create()->id,
        ]);

        $season = factory(Season::class)->create();
        $league = factory(League::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->putJson('/api/series/'.$series->uuid, [
            'season_id' => 'xxx',
            'league_id' => 'xxx',
            'name' => 'This is a string that is greater than 50 characters in length.',
            'points_for_win' => 8,
            'percentage_divisor' => 'xxx',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'season_id' => [
                    'The selected season id is invalid.',
                ],
                'league_id' => [
                    'The selected league id is invalid.',
                ],
                'name' => [
                    'The name may not be greater than 50 characters.',
                ],
                'points_for_win' => [
                    'The selected points for win is invalid.',
                ],
                'percentage_divisor' => [
                    'The selected percentage divisor is invalid.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_update_series()
    {
        $series = factory(Series::class)->create();

        $response = $this->putJson('/api/series/'.$series->uuid, ['name' => 'xxx']);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_delete_series()
    {
        $this->actingAs(factory(User::class)->create());

        $series = factory(Series::class)->create();

        $response = $this->delete('/api/series/'.$series->uuid);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('series', ['id' => $series->id]);
    }

    /**
     * @test
     */
    public function authenticated_user_cannot_delete_used_series()
    {
        $this->actingAs(factory(User::class)->create());

        $round = factory(Round::class)->create();

        $response = $this->delete('/api/series/'.$round->series->uuid);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'Unable to delete series as it has active dependants.',
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_delete_series()
    {
        $series = factory(Season::class)->create();

        $response = $this->deleteJson('/api/series/'.$series->uuid);

        $response->assertStatus(401);
    }
}
