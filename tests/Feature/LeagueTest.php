<?php

namespace Tests\Feature;

use App\Models\League;
use App\Models\Series;
use App\Models\Team;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LeagueTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function can_view_league()
    {
        $this->withoutExceptionHandling();

        $league = factory(League::class)->create();
        factory(Series::class, 2)->create(['league_id' => $league->id])->fresh();
        factory(Team::class, 2)->create(['league_id' => $league->id])->fresh();

        $response = $this->getJson('/api/leagues/'.$league->uuid);

        $response->assertOk();

        $response->assertExactJson([
            'data' => [
                'id' => $league->uuid,
                'abbreviation' => $league->abbreviation,
                'name' => $league->name,
                'series' => [
                    [
                        'id' => $league->series[0]->uuid,
                        'name' => $league->series[0]->name,
                        'points_for_win' => $league->series[0]->points_for_win,
                        'percentage_divisor' => $league->series[0]->percentage_divisor,
                        'created_at' => $league->series[0]->created_at->toDateTimeString(),
                        'updated_at' => $league->series[0]->updated_at->toDateTimeString(),
                    ],
                    [
                        'id' => $league->series[1]->uuid,
                        'name' => $league->series[1]->name,
                        'points_for_win' => $league->series[1]->points_for_win,
                        'percentage_divisor' => $league->series[1]->percentage_divisor,
                        'created_at' => $league->series[1]->created_at->toDateTimeString(),
                        'updated_at' => $league->series[1]->updated_at->toDateTimeString(),
                    ],
                ],
                'teams' => [
                    [
                        'id' => $league->teams[0]->uuid,
                        'abbreviation' => $league->teams[0]->abbreviation,
                        'short_name' => $league->teams[0]->short_name,
                        'full_name' => $league->teams[0]->full_name,
                        'primary_colour' => $league->teams[0]->primary_colour,
                        'secondary_colour' => $league->teams[0]->secondary_colour,
                        'tertiary_colour' => $league->teams[0]->tertiary_colour,
                        'address' => $league->teams[0]->address,
                        'created_at' => $league->teams[0]->created_at->toDateTimeString(),
                        'updated_at' => $league->teams[0]->updated_at->toDateTimeString(),
                    ],
                    [
                        'id' => $league->teams[1]->uuid,
                        'abbreviation' => $league->teams[1]->abbreviation,
                        'short_name' => $league->teams[1]->short_name,
                        'full_name' => $league->teams[1]->full_name,
                        'primary_colour' => $league->teams[1]->primary_colour,
                        'secondary_colour' => $league->teams[1]->secondary_colour,
                        'tertiary_colour' => $league->teams[1]->tertiary_colour,
                        'address' => $league->teams[1]->address,
                        'created_at' => $league->teams[1]->created_at->toDateTimeString(),
                        'updated_at' => $league->teams[1]->updated_at->toDateTimeString(),
                    ],
                ],
                'created_at' => $league->created_at->toDateTimeString(),
                'updated_at' => $league->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function can_view_all_leagues()
    {
        $league1 = factory(League::class)->create(['name' => 'SANFL']);
        factory(Series::class, 2)->create(['league_id' => $league1->id])->fresh();
        factory(Team::class, 2)->create(['league_id' => $league1->id])->fresh();

        $league2 = factory(League::class)->create(['name' => 'AFL']);
        factory(Series::class, 2)->create(['league_id' => $league2->id])->fresh();
        factory(Team::class, 2)->create(['league_id' => $league2->id])->fresh();

        $response = $this->getJson('/api/leagues');

        $response->assertOk();

        $response->assertJsonCount(2, 'data');

        $this->assertEquals($league2->uuid, $response->decodeResponseJson('data.0.id'));
        $this->assertEquals($league1->uuid, $response->decodeResponseJson('data.1.id'));

        $response->assertJsonFragment([
            'id' => $league1->uuid,
            'abbreviation' => $league1->abbreviation,
            'name' => $league1->name,
            'series' => [
                [
                    'id' => $league1->series[0]->uuid,
                    'name' => $league1->series[0]->name,
                    'points_for_win' => $league1->series[0]->points_for_win,
                    'percentage_divisor' => $league1->series[0]->percentage_divisor,
                    'created_at' => $league1->series[0]->created_at->toDateTimeString(),
                    'updated_at' => $league1->series[0]->updated_at->toDateTimeString(),
                ],
                [
                    'id' => $league1->series[1]->uuid,
                    'name' => $league1->series[1]->name,
                    'points_for_win' => $league1->series[1]->points_for_win,
                    'percentage_divisor' => $league1->series[1]->percentage_divisor,
                    'created_at' => $league1->series[1]->created_at->toDateTimeString(),
                    'updated_at' => $league1->series[1]->updated_at->toDateTimeString(),
                ],
            ],
            'teams' => [
                [
                    'id' => $league1->teams[0]->uuid,
                    'abbreviation' => $league1->teams[0]->abbreviation,
                    'short_name' => $league1->teams[0]->short_name,
                    'full_name' => $league1->teams[0]->full_name,
                    'primary_colour' => $league1->teams[0]->primary_colour,
                    'secondary_colour' => $league1->teams[0]->secondary_colour,
                    'tertiary_colour' => $league1->teams[0]->tertiary_colour,
                    'address' => $league1->teams[0]->address,
                    'created_at' => $league1->teams[0]->created_at->toDateTimeString(),
                    'updated_at' => $league1->teams[0]->updated_at->toDateTimeString(),
                ],
                [
                    'id' => $league1->teams[1]->uuid,
                    'abbreviation' => $league1->teams[1]->abbreviation,
                    'short_name' => $league1->teams[1]->short_name,
                    'full_name' => $league1->teams[1]->full_name,
                    'primary_colour' => $league1->teams[1]->primary_colour,
                    'secondary_colour' => $league1->teams[1]->secondary_colour,
                    'tertiary_colour' => $league1->teams[1]->tertiary_colour,
                    'address' => $league1->teams[1]->address,
                    'created_at' => $league1->teams[1]->created_at->toDateTimeString(),
                    'updated_at' => $league1->teams[1]->updated_at->toDateTimeString(),
                ],
            ],
            'created_at' => $league1->created_at->toDateTimeString(),
            'updated_at' => $league1->updated_at->toDateTimeString(),
        ]);
    }

    /**
     * @test
     */
    public function it_has_functional_pagination_when_viewing_all()
    {
        $this->verifySimplePagination(League::class, '/api/leagues');
    }

    /**
     * @test
     */
    public function authenticated_user_can_create_league()
    {
        $this->withoutExceptionHandling();

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/leagues', [
            'abbreviation' => 'AFL',
            'name' => 'Australian Football League',
        ]);

        $response->assertStatus(201);

        $league = League::first();

        $response->assertExactJson([
            'data' => [
                'id' => $league->uuid,
                'abbreviation' => 'AFL',
                'name' => 'Australian Football League',
                'series' => [],
                'teams' => [],
                'created_at' => $league->created_at->toDateTimeString(),
                'updated_at' => $league->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_required_fields()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/leagues', []);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation field is required.',
                ],
                'name' => [
                    'The name field is required.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_abbreviation_and_name_are_unique()
    {
        factory(League::class)->create([
            'abbreviation' => 'AFL',
            'name' => 'Australian Football League',
        ]);

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/leagues', [
            'abbreviation' => 'AFL',
            'name' => 'Australian Football League',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation has already been taken.',
                ],
                'name' => [
                    'The name has already been taken.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_basic_attributes()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/leagues', [
            'abbreviation' => 'xxxxxx',
            'name' => 'This is way more than the permitted allowance of characters.',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation may not be greater than 5 characters.',
                ],
                'name' => [
                    'The name may not be greater than 50 characters.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_create_league()
    {
        $response = $this->postJson('/api/leagues', [
            'abbreviation' => 'AFL',
            'name' => 'Australian Football League',
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_update_league()
    {
        $this->actingAs(factory(User::class)->create());

        $league = factory(League::class)->create();
        $updates = factory(League::class)->make();

        $data = [
            'abbreviation' => $updates->abbreviation,
            'name' => $updates->name,
        ];

        $response = $this->putJson('/api/leagues/'.$league->uuid, $data);

        $response->assertOk();

        $league->refresh();

        $response->assertExactJson([
            'data' => [
                'id' => $league->uuid,
                'abbreviation' => $data['abbreviation'],
                'name' => $data['name'],
                'series' => [],
                'teams' => [],
                'created_at' => $league->created_at->toDateTimeString(),
                'updated_at' => $league->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_abbreviation_and_name_are_unique()
    {
        $this->actingAs(factory(User::class)->create());

        $otherLeague = factory(League::class)->create();
        $league = factory(League::class)->create();

        $data = [
            'abbreviation' => $otherLeague->abbreviation,
            'name' => $otherLeague->name,
        ];

        $response = $this->putJson('/api/leagues/'.$league->uuid, $data);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation has already been taken.',
                ],
                'name' => [
                    'The name has already been taken.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_basic_attributes()
    {
        $league = factory(League::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->putJson('/api/leagues/'.$league->uuid, [
            'abbreviation' => 'xxxxxx',
            'name' => 'This is way more than the permitted allowance of characters.',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation may not be greater than 5 characters.',
                ],
                'name' => [
                    'The name may not be greater than 50 characters.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_update_league()
    {
        $league = factory(League::class)->create();

        $response = $this->putJson('/api/leagues/'.$league->uuid, ['name' => 'xxx']);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_delete_league()
    {
        $this->actingAs(factory(User::class)->create());

        $league = factory(League::class)->create();

        $response = $this->delete('/api/leagues/'.$league->uuid);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('leagues', ['id' => $league->id]);
    }

    /**
     * @test
     */
    public function authenticated_user_cannot_delete_league_associated_with_series()
    {
        $this->actingAs(factory(User::class)->create());

        $series = factory(Series::class)->create();

        $response = $this->delete('/api/leagues/'.$series->league->uuid);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'Unable to delete league as it has active dependants.',
            ],
        ]);
    }

    /**
     * @test
     */
    public function authenticated_user_cannot_delete_league_associated_with_team()
    {
        $this->actingAs(factory(User::class)->create());

        $team = factory(Team::class)->create();

        $response = $this->delete('/api/leagues/'.$team->league->uuid);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'Unable to delete league as it has active dependants.',
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_delete_league()
    {
        $league = factory(League::class)->create();

        $response = $this->deleteJson('/api/leagues/'.$league->uuid);

        $response->assertStatus(401);
    }
}
