<?php

namespace Tests\Feature;

use App\Models\Series;
use App\Models\Team;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LadderTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_view_current_ladder()
    {
        $this->withoutExceptionHandling();

        $series = factory(Series::class)->create()->fresh();

        $teamA = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team A']);
        $teamB = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team B']);
        $teamC = factory(Team::class)->create(['league_id' => $series->league->id, 'full_name' => 'Team C']);

        cache()->put('ladder_'.$series->uuid, [
            [
                'team' => $teamA,
                'played' => 0,
                'won' => 0,
                'drawn' => 0,
                'lost' => 0,
                'points' => 0,
                'percentage' => 0,
                'goals_for' => 0,
                'behinds_for' => 0,
                'points_for' => 0,
                'goals_against' => 0,
                'behinds_against' => 0,
                'points_against' => 0,
            ],
            [
                'team' => $teamB,
                'played' => 0,
                'won' => 0,
                'drawn' => 0,
                'lost' => 0,
                'points' => 0,
                'percentage' => 0,
                'goals_for' => 0,
                'behinds_for' => 0,
                'points_for' => 0,
                'goals_against' => 0,
                'behinds_against' => 0,
                'points_against' => 0,
            ],
            [
                'team' => $teamC,
                'played' => 0,
                'won' => 0,
                'drawn' => 0,
                'lost' => 0,
                'points' => 0,
                'percentage' => 0,
                'goals_for' => 0,
                'behinds_for' => 0,
                'points_for' => 0,
                'goals_against' => 0,
                'behinds_against' => 0,
                'points_against' => 0,
            ],
        ]);

        $response = $this->getJson('/api/ladders/'.$series->uuid);

        $response->assertOk();

        $response->assertJsonStructure([
            'status',
            'data' => [
                '*' => [
                    'played',
                    'won',
                    'drawn',
                    'lost',
                    'points',
                    'percentage',
                    'goals_for',
                    'behinds_for',
                    'points_for',
                    'goals_against',
                    'behinds_against',
                    'points_against',
                    'team',
                ],
            ],
        ]);

        $response->assertJsonFragment(
            [
                'played' => 0,
                'won' => 0,
                'drawn' => 0,
                'lost' => 0,
                'points' => 0,
                'percentage' => 0,
                'goals_for' => 0,
                'behinds_for' => 0,
                'points_for' => 0,
                'goals_against' => 0,
                'behinds_against' => 0,
                'points_against' => 0,
                'team' => [
                    'id' => $teamA->uuid,
                    'abbreviation' => $teamA->abbreviation,
                    'short_name' => $teamA->short_name,
                    'full_name' => $teamA->full_name,
                    'primary_colour' => $teamA->primary_colour,
                    'secondary_colour' => $teamA->secondary_colour,
                    'tertiary_colour' => $teamA->tertiary_colour,
                    'address' => $teamA->address,
                    'created_at' => $teamA->created_at->toDateTimeString(),
                    'updated_at' => $teamA->updated_at->toDateTimeString(),
                ],
            ]
        );

        $this->assertEquals($teamA->uuid, $this->responseJsonValue($response, 'data.0.team.id'));
        $this->assertEquals($teamB->uuid, $this->responseJsonValue($response, 'data.1.team.id'));
        $this->assertEquals($teamC->uuid, $this->responseJsonValue($response, 'data.2.team.id'));
    }
}
