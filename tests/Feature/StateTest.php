<?php

namespace Tests\Feature;

use App\Models\State;
use App\Models\Team;
use App\Models\User;
use App\Models\Venue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StateTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function can_view_state()
    {
        $this->withoutExceptionHandling();

        $state = factory(State::class)->create();

        factory(Venue::class, 2)->create(['state_id' => $state->id]);
        factory(Team::class, 2)->create(['home_state_id' => $state->id]);

        $response = $this->getJson('/api/states/'.$state->uuid);

        $response->assertOk();

        $response->assertExactJson([
            'data' => [
                'id' => $state->uuid,
                'abbreviation' => $state->abbreviation,
                'name' => $state->name,
                'timezone' => $state->timezone,
                'venues' => [
                    [
                        'id' => $state->venues[0]->uuid,
                        'abbreviation' => $state->venues[0]->abbreviation,
                        'name' => $state->venues[0]->name,
                        'capacity' => $state->venues[0]->capacity,
                        'latitude' => $state->venues[0]->latitude,
                        'longitude' => $state->venues[0]->longitude,
                        'created_at' => $state->venues[0]->created_at->toDateTimeString(),
                        'updated_at' => $state->venues[0]->updated_at->toDateTimeString(),
                    ],
                    [
                        'id' => $state->venues[1]->uuid,
                        'abbreviation' => $state->venues[1]->abbreviation,
                        'name' => $state->venues[1]->name,
                        'capacity' => $state->venues[1]->capacity,
                        'latitude' => $state->venues[1]->latitude,
                        'longitude' => $state->venues[1]->longitude,
                        'created_at' => $state->venues[1]->created_at->toDateTimeString(),
                        'updated_at' => $state->venues[1]->updated_at->toDateTimeString(),
                    ],
                ],
                'home_teams' => [
                    [
                        'id' => $state->homeTeams[0]->uuid,
                        'abbreviation' => $state->homeTeams[0]->abbreviation,
                        'short_name' => $state->homeTeams[0]->short_name,
                        'full_name' => $state->homeTeams[0]->full_name,
                        'primary_colour' => $state->homeTeams[0]->primary_colour,
                        'secondary_colour' => $state->homeTeams[0]->secondary_colour,
                        'tertiary_colour' => $state->homeTeams[0]->tertiary_colour,
                        'address' => $state->homeTeams[0]->address,
                        'created_at' => $state->homeTeams[0]->created_at->toDateTimeString(),
                        'updated_at' => $state->homeTeams[0]->updated_at->toDateTimeString(),
                    ],
                    [
                        'id' => $state->homeTeams[1]->uuid,
                        'abbreviation' => $state->homeTeams[1]->abbreviation,
                        'short_name' => $state->homeTeams[1]->short_name,
                        'full_name' => $state->homeTeams[1]->full_name,
                        'primary_colour' => $state->homeTeams[1]->primary_colour,
                        'secondary_colour' => $state->homeTeams[1]->secondary_colour,
                        'tertiary_colour' => $state->homeTeams[1]->tertiary_colour,
                        'address' => $state->homeTeams[1]->address,
                        'created_at' => $state->homeTeams[1]->created_at->toDateTimeString(),
                        'updated_at' => $state->homeTeams[1]->updated_at->toDateTimeString(),
                    ],
                ],
                'created_at' => $state->created_at->toDateTimeString(),
                'updated_at' => $state->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function can_view_all_states()
    {
        $state1 = factory(State::class)->create(['name' => 'South Australia']);
        factory(Venue::class, 2)->create(['state_id' => $state1->id]);
        factory(Team::class, 2)->create(['home_state_id' => $state1->id]);

        $state2 = factory(State::class)->create(['name' => 'New South Wales']);
        factory(Venue::class, 2)->create(['state_id' => $state2->id]);
        factory(Team::class, 2)->create(['home_state_id' => $state2->id]);

        $response = $this->getJson('/api/states');

        $response->assertOk();

        $response->assertJsonCount(2, 'data');

        $this->assertEquals($state2->uuid, $response->decodeResponseJson('data.0.id'));
        $this->assertEquals($state1->uuid, $response->decodeResponseJson('data.1.id'));

        $response->assertJsonFragment([
            'id' => $state1->uuid,
            'abbreviation' => $state1->abbreviation,
            'name' => $state1->name,
            'timezone' => $state1->timezone,
            'venues' => [
                [
                    'id' => $state1->venues[0]->uuid,
                    'abbreviation' => $state1->venues[0]->abbreviation,
                    'name' => $state1->venues[0]->name,
                    'capacity' => $state1->venues[0]->capacity,
                    'latitude' => $state1->venues[0]->latitude,
                    'longitude' => $state1->venues[0]->longitude,
                    'created_at' => $state1->venues[0]->created_at->toDateTimeString(),
                    'updated_at' => $state1->venues[0]->updated_at->toDateTimeString(),
                ],
                [
                    'id' => $state1->venues[1]->uuid,
                    'abbreviation' => $state1->venues[1]->abbreviation,
                    'name' => $state1->venues[1]->name,
                    'capacity' => $state1->venues[1]->capacity,
                    'latitude' => $state1->venues[1]->latitude,
                    'longitude' => $state1->venues[1]->longitude,
                    'created_at' => $state1->venues[1]->created_at->toDateTimeString(),
                    'updated_at' => $state1->venues[1]->updated_at->toDateTimeString(),
                ],
            ],
            'home_teams' => [
                [
                    'id' => $state1->homeTeams[0]->uuid,
                    'abbreviation' => $state1->homeTeams[0]->abbreviation,
                    'short_name' => $state1->homeTeams[0]->short_name,
                    'full_name' => $state1->homeTeams[0]->full_name,
                    'primary_colour' => $state1->homeTeams[0]->primary_colour,
                    'secondary_colour' => $state1->homeTeams[0]->secondary_colour,
                    'tertiary_colour' => $state1->homeTeams[0]->tertiary_colour,
                    'address' => $state1->homeTeams[0]->address,
                    'created_at' => $state1->homeTeams[0]->created_at->toDateTimeString(),
                    'updated_at' => $state1->homeTeams[0]->updated_at->toDateTimeString(),
                ],
                [
                    'id' => $state1->homeTeams[1]->uuid,
                    'abbreviation' => $state1->homeTeams[1]->abbreviation,
                    'short_name' => $state1->homeTeams[1]->short_name,
                    'full_name' => $state1->homeTeams[1]->full_name,
                    'primary_colour' => $state1->homeTeams[1]->primary_colour,
                    'secondary_colour' => $state1->homeTeams[1]->secondary_colour,
                    'tertiary_colour' => $state1->homeTeams[1]->tertiary_colour,
                    'address' => $state1->homeTeams[1]->address,
                    'created_at' => $state1->homeTeams[1]->created_at->toDateTimeString(),
                    'updated_at' => $state1->homeTeams[1]->updated_at->toDateTimeString(),
                ],
            ],
            'created_at' => $state1->created_at->toDateTimeString(),
            'updated_at' => $state1->updated_at->toDateTimeString(),
        ]);
    }

    /**
     * @test
     */
    public function it_has_functional_pagination_when_viewing_all()
    {
        $this->verifySimplePagination(State::class, '/api/states');
    }

    /**
     * @test
     */
    public function authenticated_user_can_create_state()
    {
        $this->withoutExceptionHandling();

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/states', [
            'abbreviation' => 'SA',
            'name' => 'South Australia',
            'timezone' => 'Australia/Adelaide',
        ]);

        $response->assertStatus(201);

        $state = State::first();

        $response->assertExactJson([
            'data' => [
                'id' => $state->uuid,
                'abbreviation' => 'SA',
                'name' => 'South Australia',
                'timezone' => 'Australia/Adelaide',
                'venues' => [],
                'home_teams' => [],
                'created_at' => $state->created_at->toDateTimeString(),
                'updated_at' => $state->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_required_fields()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/states', []);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation field is required.',
                ],
                'name' => [
                    'The name field is required.',
                ],
                'timezone' => [
                    'The timezone field is required.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_abbreviation_and_name_are_unique()
    {
        factory(State::class)->create([
            'abbreviation' => 'SA',
            'name' => 'South Australia',
        ]);

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/states', [
            'abbreviation' => 'SA',
            'name' => 'South Australia',
            'timezone' => 'Australia/Adelaide',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation has already been taken.',
                ],
                'name' => [
                    'The name has already been taken.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_basic_attributes()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/states', [
            'abbreviation' => 'xxxx',
            'name' => 'This is way more than the permitted allowance of characters.',
            'timezone' => 'Australia/xxx',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation may not be greater than 3 characters.',
                ],
                'name' => [
                    'The name may not be greater than 50 characters.',
                ],
                'timezone' => [
                    'The timezone must be a valid zone.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_create_state()
    {
        $response = $this->postJson('/api/states', [
            'abbreviation' => 'AFL',
            'name' => 'Australian Football League',
            'timezone' => 'Australia/Adelaide',
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_update_state()
    {
        $this->actingAs(factory(User::class)->create());

        $state = factory(State::class)->create();
        $updates = factory(State::class)->make();

        $data = [
            'abbreviation' => $updates->abbreviation,
            'name' => $updates->name,
            'timezone' => $updates->timezone,
        ];

        $response = $this->putJson('/api/states/'.$state->uuid, $data);

        $response->assertOk();

        $state->refresh();

        $response->assertExactJson([
            'data' => [
                'id' => $state->uuid,
                'abbreviation' => $data['abbreviation'],
                'name' => $data['name'],
                'timezone' => $data['timezone'],
                'venues' => [],
                'home_teams' => [],
                'created_at' => $state->created_at->toDateTimeString(),
                'updated_at' => $state->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_abbreviation_and_name_are_unique()
    {
        $this->actingAs(factory(User::class)->create());

        $otherState = factory(State::class)->create();
        $state = factory(State::class)->create();

        $data = [
            'abbreviation' => $otherState->abbreviation,
            'name' => $otherState->name,
        ];

        $response = $this->putJson('/api/states/'.$state->uuid, $data);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation has already been taken.',
                ],
                'name' => [
                    'The name has already been taken.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_basic_attributes()
    {
        $state = factory(State::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->putJson('/api/states/'.$state->uuid, [
            'abbreviation' => 'xxxx',
            'name' => 'This is way more than the permitted allowance of characters.',
            'timezone' => 'Australia/xxx',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation may not be greater than 3 characters.',
                ],
                'name' => [
                    'The name may not be greater than 50 characters.',
                ],
                'timezone' => [
                    'The timezone must be a valid zone.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_update_state()
    {
        $state = factory(State::class)->create();

        $response = $this->putJson('/api/states/'.$state->uuid, ['name' => 'xxx']);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_delete_state()
    {
        $this->actingAs(factory(User::class)->create());

        $state = factory(State::class)->create();

        $response = $this->delete('/api/states/'.$state->uuid);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('states', ['id' => $state->id]);
    }

    /**
     * @test
     */
    public function authenticated_user_cannot_delete_state_associated_with_venue()
    {
        $this->actingAs(factory(User::class)->create());

        $venue = factory(Venue::class)->create();

        $response = $this->delete('/api/states/'.$venue->state->uuid);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'Unable to delete state as it has active dependants.',
            ],
        ]);
    }

    /**
     * @test
     */
    public function authenticated_user_cannot_delete_state_associated_with_team()
    {
        $this->actingAs(factory(User::class)->create());

        $team = factory(Team::class)->create();

        $response = $this->delete('/api/states/'.$team->homeState->uuid);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'Unable to delete state as it has active dependants.',
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_delete_state()
    {
        $state = factory(State::class)->create();

        $response = $this->deleteJson('/api/leagues/'.$state->uuid);

        $response->assertStatus(401);
    }
}
