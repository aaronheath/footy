<?php

namespace Tests\Feature;

use App\Models\League;
use App\Models\Match;
use App\Models\State;
use App\Models\Team;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TeamTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function can_view_team()
    {
        $this->withoutExceptionHandling();

        $team = factory(Team::class)->create();

        $response = $this->getJson('/api/teams/'.$team->uuid);

        $response->assertOk();

        $response->assertExactJson([
            'data' => [
                'id' => $team->uuid,
                'abbreviation' => $team->abbreviation,
                'short_name' => $team->short_name,
                'full_name' => $team->full_name,
                'primary_colour' => $team->primary_colour,
                'secondary_colour' => $team->secondary_colour,
                'tertiary_colour' => $team->tertiary_colour,
                'address' => $team->address,
                'home_state' => [
                    'id' => $team->homeState->uuid,
                    'abbreviation' => $team->homeState->abbreviation,
                    'name' => $team->homeState->name,
                    'timezone' => $team->homeState->timezone,
                    'created_at' => $team->homeState->created_at->toDateTimeString(),
                    'updated_at' => $team->homeState->updated_at->toDateTimeString(),
                ],
                'league' => [
                    'id' => $team->league->uuid,
                    'abbreviation' => $team->league->abbreviation,
                    'name' => $team->league->name,
                    'created_at' => $team->league->created_at->toDateTimeString(),
                    'updated_at' => $team->league->updated_at->toDateTimeString(),
                ],
                'created_at' => $team->created_at->toDateTimeString(),
                'updated_at' => $team->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function can_view_all_teams()
    {
        $state = factory(State::class)->create();

        $essendon = factory(Team::class)->create([
            'short_name' => 'Essendon',
        ]);

        $adelaide = factory(Team::class)->create([
            'short_name' => 'Adelaide',
        ]);

        $portAdelaide = factory(Team::class)->create([
            'short_name' => 'Port Adelaide',
        ]);

        $response = $this->getJson('/api/teams');

        $response->assertOk();

        $response->assertJsonCount(3, 'data');

        $this->assertEquals($adelaide->short_name, $response->decodeResponseJson('data.0.short_name'));
        $this->assertEquals($essendon->short_name, $response->decodeResponseJson('data.1.short_name'));
        $this->assertEquals($portAdelaide->short_name, $response->decodeResponseJson('data.2.short_name'));

        $response->assertJsonFragment([
            'id' => $essendon->uuid,
            'abbreviation' => $essendon->abbreviation,
            'short_name' => $essendon->short_name,
            'full_name' => $essendon->full_name,
            'primary_colour' => $essendon->primary_colour,
            'secondary_colour' => $essendon->secondary_colour,
            'tertiary_colour' => $essendon->tertiary_colour,
            'address' => $essendon->address,
            'home_state' => [
                'id' => $essendon->homeState->uuid,
                'abbreviation' => $essendon->homeState->abbreviation,
                'name' => $essendon->homeState->name,
                'timezone' => $essendon->homeState->timezone,
                'created_at' => $essendon->homeState->created_at->toDateTimeString(),
                'updated_at' => $essendon->homeState->updated_at->toDateTimeString(),
            ],
            'league' => [
                'id' => $essendon->league->uuid,
                'abbreviation' => $essendon->league->abbreviation,
                'name' => $essendon->league->name,
                'created_at' => $essendon->league->created_at->toDateTimeString(),
                'updated_at' => $essendon->league->updated_at->toDateTimeString(),
            ],
            'created_at' => $essendon->created_at->toDateTimeString(),
            'updated_at' => $essendon->updated_at->toDateTimeString(),
        ]);
    }

    /**
     * @test
     */
    public function it_has_functional_pagination_when_viewing_all()
    {
        $this->verifySimplePagination(Team::class, '/api/teams');
    }

    /**
     * @test
     */
    public function authenticated_user_can_create_team()
    {
        $this->withoutExceptionHandling();

        $homeState = factory(State::class)->create();
        $league = factory(League::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/teams', [
            'home_state_id' => $homeState->uuid,
            'league_id' => $league->uuid,
            'abbreviation' => 'ESS',
            'short_name' => 'Essendon',
            'full_name' => 'Essendon FC',
            'primary_colour' => '#000000',
            'secondary_colour' => '#C90427',
            'tertiary_colour' => '#D1D1D1',
            'address' => '275 Melrose Dr, Melbourne Airport VIC 3045',
        ]);

        $response->assertStatus(201);

        $team = Team::with(['homeState', 'league'])->first();

        $response->assertExactJson([
            'data' => [
                'id' => $team->uuid,
                'abbreviation' => $team->abbreviation,
                'short_name' => $team->short_name,
                'full_name' => $team->full_name,
                'primary_colour' => $team->primary_colour,
                'secondary_colour' => $team->secondary_colour,
                'tertiary_colour' => $team->tertiary_colour,
                'address' => $team->address,
                'home_state' => [
                    'id' => $team->homeState->uuid,
                    'abbreviation' => $team->homeState->abbreviation,
                    'name' => $team->homeState->name,
                    'timezone' => $team->homeState->timezone,
                    'created_at' => $team->homeState->created_at->toDateTimeString(),
                    'updated_at' => $team->homeState->updated_at->toDateTimeString(),
                ],
                'league' => [
                    'id' => $team->league->uuid,
                    'abbreviation' => $team->league->abbreviation,
                    'name' => $team->league->name,
                    'created_at' => $team->league->created_at->toDateTimeString(),
                    'updated_at' => $team->league->updated_at->toDateTimeString(),
                ],
                'created_at' => $team->created_at->toDateTimeString(),
                'updated_at' => $team->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_required_fields()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/teams', []);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'home_state_id' => [
                    'The home state id field is required.',
                ],
                'league_id' => [
                    'The league id field is required.',
                ],
                'abbreviation' => [
                    'The abbreviation field is required.',
                ],
                'short_name' => [
                    'The short name field is required.',
                ],
                'full_name' => [
                    'The full name field is required.',
                ],
                'primary_colour' => [
                    'The primary colour field is required.',
                ],
                'secondary_colour' => [
                    'The secondary colour field is required.',
                ],
                'tertiary_colour' => [
                    'The tertiary colour field is required.',
                ],
                'address' => [
                    'The address field is required.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_fields_with_unique_requirement()
    {
//        $this->withoutExceptionHandling();

        $homeState = factory(State::class)->create();
        $league = factory(League::class)->create();

        factory(Team::class)->create([
            'home_state_id' => $homeState->id,
            'league_id' => $league->id,
            'abbreviation' => 'ESS',
            'short_name' => 'Essendon',
            'full_name' => 'Essendon FC',
            'primary_colour' => '#000000',
            'secondary_colour' => '#C90427',
            'tertiary_colour' => '#D1D1D1',
            'address' => '275 Melrose Dr, Melbourne Airport VIC 3045',
        ]);

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/teams', [
            'home_state_id' => $homeState->uuid,
            'league_id' => $league->uuid,
            'abbreviation' => 'ESS',
            'short_name' => 'Essendon',
            'full_name' => 'Essendon FC',
            'primary_colour' => '#000000',
            'secondary_colour' => '#C90427',
            'tertiary_colour' => '#D1D1D1',
            'address' => '275 Melrose Dr, Melbourne Airport VIC 3045',
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation is already associated with league.',
                ],
                'short_name' => [
                    'The short name is already associated with league.',
                ],
                'full_name' => [
                    'The full name is already associated with league.',
                ],
                'colours' => [
                    'The colours are in use by another team in league.',
                ],
                'address' => [
                    'The address is already associated with league.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_basic_attributes()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/teams', [
            'home_state_id' => 'xxx',
            'league_id' => 'xxx',
            'abbreviation' => 'xxxx',
            'short_name' => $this->stubText(21),
            'full_name' => $this->stubText(51),
            'primary_colour' => '#000000x',
            'secondary_colour' => '#C90427x',
            'tertiary_colour' => '#D1D1D1x',
            'address' => $this->stubText(101),
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'home_state_id' => [
                    'The selected home state id is invalid.',
                ],
                'league_id' => [
                    'The selected league id is invalid.',
                ],
                'abbreviation' => [
                    'The abbreviation may not be greater than 3 characters.',
                ],
                'short_name' => [
                    'The short name may not be greater than 20 characters.',
                ],
                'full_name' => [
                    'The full name may not be greater than 50 characters.',
                ],
                'primary_colour' => [
                    'The primary colour may not be greater than 7 characters.',
                ],
                'secondary_colour' => [
                    'The secondary colour may not be greater than 7 characters.',
                ],
                'tertiary_colour' => [
                    'The tertiary colour may not be greater than 7 characters.',
                ],
                'address' => [
                    'The address may not be greater than 100 characters.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_create_team()
    {
        $homeState = factory(State::class)->create();
        $league = factory(League::class)->create();

        $response = $this->postJson('/api/teams', [
            'home_state_id' => $homeState->uuid,
            'league_id' => $league->uuid,
            'abbreviation' => 'ESS',
            'short_name' => 'Essendon',
            'full_name' => 'Essendon FC',
            'primary_colour' => '#000000',
            'secondary_colour' => '#C90427',
            'tertiary_colour' => '#D1D1D1',
            'address' => '275 Melrose Dr, Melbourne Airport VIC 3045',
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_update_team()
    {
        $this->actingAs(factory(User::class)->create());

        $team = factory(Team::class)->create();

        $homeState = factory(State::class)->create();
        $league = factory(League::class)->create();

        $data = [
            'home_state_id' => $homeState->uuid,
            'league_id' => $league->uuid,
            'abbreviation' => 'ESS',
            'short_name' => 'Essendon',
            'full_name' => 'Essendon FC',
            'primary_colour' => '#000000',
            'secondary_colour' => '#C90427',
            'tertiary_colour' => '#D1D1D1',
            'address' => '275 Melrose Dr, Melbourne Airport VIC 3045',
        ];

        $response = $this->putJson('/api/teams/'.$team->uuid, $data);

        $response->assertOk();

        $team->refresh();

        $response->assertExactJson([
            'data' => [
                'id' => $team->uuid,
                'abbreviation' => $data['abbreviation'],
                'short_name' => $data['short_name'],
                'full_name' => $data['full_name'],
                'primary_colour' => $data['primary_colour'],
                'secondary_colour' => $data['secondary_colour'],
                'tertiary_colour' => $data['tertiary_colour'],
                'address' => $data['address'],
                'home_state' => [
                    'id' => $homeState->uuid,
                    'abbreviation' => $homeState->abbreviation,
                    'name' => $homeState->name,
                    'timezone' => $homeState->timezone,
                    'created_at' => $homeState->created_at->toDateTimeString(),
                    'updated_at' => $homeState->updated_at->toDateTimeString(),
                ],
                'league' => [
                    'id' => $league->uuid,
                    'abbreviation' => $league->abbreviation,
                    'name' => $league->name,
                    'created_at' => $league->created_at->toDateTimeString(),
                    'updated_at' => $league->updated_at->toDateTimeString(),
                ],
                'created_at' => $team->created_at->toDateTimeString(),
                'updated_at' => $team->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_unique_requirements()
    {
        $this->actingAs(factory(User::class)->create());

        $original = factory(Team::class)->create([
            'abbreviation' => 'ESS',
            'short_name' => 'Essendon',
            'full_name' => 'Essendon FC',
            'primary_colour' => '#000000',
            'secondary_colour' => '#C90427',
            'tertiary_colour' => '#D1D1D1',
            'address' => '275 Melrose Dr, Melbourne Airport VIC 3045',
        ]);

        $team = factory(Team::class)->create([
            'league_id' => $original->league->id,
        ]);

        $data = [
            'abbreviation' => 'ESS',
            'short_name' => 'Essendon',
            'full_name' => 'Essendon FC',
            'primary_colour' => '#000000',
            'secondary_colour' => '#C90427',
            'tertiary_colour' => '#D1D1D1',
            'address' => '275 Melrose Dr, Melbourne Airport VIC 3045',
        ];

        $response = $this->putJson('/api/teams/'.$team->uuid, $data);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'abbreviation' => [
                    'The abbreviation is already associated with league.',
                ],
                'short_name' => [
                    'The short name is already associated with league.',
                ],
                'full_name' => [
                    'The full name is already associated with league.',
                ],
                'colours' => [
                    'The colours are in use by another team in league.',
                ],
                'address' => [
                    'The address is already associated with league.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_basic_attributes()
    {
        $team = factory(Team::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->putJson('/api/teams/'.$team->uuid, [
            'home_state_id' => 'xxx',
            'league_id' => 'xxx',
            'abbreviation' => 'xxxx',
            'short_name' => $this->stubText(21),
            'full_name' => $this->stubText(51),
            'primary_colour' => '#000000x',
            'secondary_colour' => '#C90427x',
            'tertiary_colour' => '#D1D1D1x',
            'address' => $this->stubText(101),
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'home_state_id' => [
                    'The selected home state id is invalid.',
                ],
                'league_id' => [
                    'The selected league id is invalid.',
                ],
                'abbreviation' => [
                    'The abbreviation may not be greater than 3 characters.',
                ],
                'short_name' => [
                    'The short name may not be greater than 20 characters.',
                ],
                'full_name' => [
                    'The full name may not be greater than 50 characters.',
                ],
                'primary_colour' => [
                    'The primary colour may not be greater than 7 characters.',
                ],
                'secondary_colour' => [
                    'The secondary colour may not be greater than 7 characters.',
                ],
                'tertiary_colour' => [
                    'The tertiary colour may not be greater than 7 characters.',
                ],
                'address' => [
                    'The address may not be greater than 100 characters.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_update()
    {
        $team = factory(Team::class)->create();

        $response = $this->putJson('/api/teams/'.$team->uuid, ['abbreviation' => 'xxx']);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_delete()
    {
        $this->actingAs(factory(User::class)->create());

        $team = factory(Team::class)->create();

        $response = $this->delete('/api/teams/'.$team->uuid);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('teams', ['id' => $team->id]);
    }

    /**
     * @test
     */
    public function authenticated_user_cannot_delete_used_team()
    {
        $this->actingAs(factory(User::class)->create());

        $match = factory(Match::class)->create();

        $response = $this->delete('/api/teams/'.$match->homeTeam->uuid);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'Unable to delete team as it has active dependants.',
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_delete()
    {
        $team = factory(Team::class)->create();

        $response = $this->deleteJson('/api/teams/'.$team->uuid);

        $response->assertStatus(401);
    }
}
