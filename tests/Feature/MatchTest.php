<?php

namespace Tests\Feature;

use App\Jobs\GenerateLadder;
use App\Models\Match;
use App\Models\Round;
use App\Models\Team;
use App\Models\User;
use App\Models\Venue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class MatchTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function can_view_individual()
    {
        $this->withoutExceptionHandling();

        $match = factory(Match::class)->create();

        $response = $this->getJson('/api/matches/'.$match->uuid);

        $response->assertOk();

        $response->assertExactJson([
            'data' => [
                'id' => $match->uuid,
                'home_team_goals' => $match->home_team_goals,
                'home_team_behinds' => $match->home_team_behinds,
                'home_team_points' => $match->home_team_points,
                'away_team_goals' => $match->away_team_goals,
                'away_team_behinds' => $match->away_team_behinds,
                'away_team_points' => $match->away_team_points,
                'start_time' => $match->start_time,
                'attendance' => $match->attendance,
                'reference' => $match->reference,
                'name' => $match->name,
                'round' => [
                    'id' => $match->round->uuid,
                    'round' => $match->round->round,
                    'name' => $match->round->name,
                    'created_at' => $match->round->created_at->toDateTimeString(),
                    'updated_at' => $match->round->updated_at->toDateTimeString(),
                    'series' => [
                        'id' => $match->round->series->uuid,
                        'name' => $match->round->series->name,
                        'points_for_win' => $match->round->series->points_for_win,
                        'percentage_divisor' => $match->round->series->percentage_divisor,
                        'league' => [
                            'id' => $match->round->series->league->uuid,
                            'abbreviation' => $match->round->series->league->abbreviation,
                            'name' => $match->round->series->league->name,
                            'created_at' => $match->round->series->league->created_at->toDateTimeString(),
                            'updated_at' => $match->round->series->league->updated_at->toDateTimeString(),
                        ],
                        'created_at' => $match->round->series->created_at->toDateTimeString(),
                        'updated_at' => $match->round->series->updated_at->toDateTimeString(),
                    ],
                ],
                'venue' => [
                    'id' => $match->venue->uuid,
                    'abbreviation' => $match->venue->abbreviation,
                    'name' => $match->venue->name,
                    'capacity' => $match->venue->capacity,
                    'latitude' => $match->venue->latitude,
                    'longitude' => $match->venue->longitude,
                    'created_at' => $match->venue->created_at->toDateTimeString(),
                    'updated_at' => $match->venue->updated_at->toDateTimeString(),
                ],
                'home_team' => [
                    'id' => $match->homeTeam->uuid,
                    'abbreviation' => $match->homeTeam->abbreviation,
                    'short_name' => $match->homeTeam->short_name,
                    'full_name' => $match->homeTeam->full_name,
                    'primary_colour' => $match->homeTeam->primary_colour,
                    'secondary_colour' => $match->homeTeam->secondary_colour,
                    'tertiary_colour' => $match->homeTeam->tertiary_colour,
                    'address' => $match->homeTeam->address,
                    'created_at' => $match->homeTeam->created_at->toDateTimeString(),
                    'updated_at' => $match->homeTeam->updated_at->toDateTimeString(),
                ],
                'away_team' => [
                    'id' => $match->awayTeam->uuid,
                    'abbreviation' => $match->awayTeam->abbreviation,
                    'short_name' => $match->awayTeam->short_name,
                    'full_name' => $match->awayTeam->full_name,
                    'primary_colour' => $match->awayTeam->primary_colour,
                    'secondary_colour' => $match->awayTeam->secondary_colour,
                    'tertiary_colour' => $match->awayTeam->tertiary_colour,
                    'address' => $match->awayTeam->address,
                    'created_at' => $match->awayTeam->created_at->toDateTimeString(),
                    'updated_at' => $match->awayTeam->updated_at->toDateTimeString(),
                ],
                'created_at' => $match->created_at->toDateTimeString(),
                'updated_at' => $match->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function can_view_all()
    {
        $match2 = factory(Match::class)->create([
            'start_time' => now(),
        ]);

        $match1 = factory(Match::class)->create([
            'start_time' => now()->subHour(),
        ]);

        $match3 = factory(Match::class)->create([
            'start_time' => now()->addHour(),
        ]);

        $response = $this->getJson('/api/matches');

        $response->assertOk();

        $response->assertJsonCount(3, 'data');

        $this->assertEquals($match1->uuid, $response->decodeResponseJson('data.0.id'));
        $this->assertEquals($match2->uuid, $response->decodeResponseJson('data.1.id'));
        $this->assertEquals($match3->uuid, $response->decodeResponseJson('data.2.id'));

        $response->assertJsonFragment([
            'id' => $match1->uuid,
            'home_team_goals' => $match1->home_team_goals,
            'home_team_behinds' => $match1->home_team_behinds,
            'home_team_points' => $match1->home_team_points,
            'away_team_goals' => $match1->away_team_goals,
            'away_team_behinds' => $match1->away_team_behinds,
            'away_team_points' => $match1->away_team_points,
            'start_time' => $match1->start_time,
            'attendance' => $match1->attendance,
            'reference' => $match1->reference,
            'name' => $match1->name,
            'round' => [
                'id' => $match1->round->uuid,
                'round' => $match1->round->round,
                'name' => $match1->round->name,
                'created_at' => $match1->round->created_at->toDateTimeString(),
                'updated_at' => $match1->round->updated_at->toDateTimeString(),
            ],
            'venue' => [
                'id' => $match1->venue->uuid,
                'abbreviation' => $match1->venue->abbreviation,
                'name' => $match1->venue->name,
                'capacity' => $match1->venue->capacity,
                'latitude' => $match1->venue->latitude,
                'longitude' => $match1->venue->longitude,
                'created_at' => $match1->venue->created_at->toDateTimeString(),
                'updated_at' => $match1->venue->updated_at->toDateTimeString(),
            ],
            'home_team' => [
                'id' => $match1->homeTeam->uuid,
                'abbreviation' => $match1->homeTeam->abbreviation,
                'short_name' => $match1->homeTeam->short_name,
                'full_name' => $match1->homeTeam->full_name,
                'primary_colour' => $match1->homeTeam->primary_colour,
                'secondary_colour' => $match1->homeTeam->secondary_colour,
                'tertiary_colour' => $match1->homeTeam->tertiary_colour,
                'address' => $match1->homeTeam->address,
                'created_at' => $match1->homeTeam->created_at->toDateTimeString(),
                'updated_at' => $match1->homeTeam->updated_at->toDateTimeString(),
            ],
            'away_team' => [
                'id' => $match1->awayTeam->uuid,
                'abbreviation' => $match1->awayTeam->abbreviation,
                'short_name' => $match1->awayTeam->short_name,
                'full_name' => $match1->awayTeam->full_name,
                'primary_colour' => $match1->awayTeam->primary_colour,
                'secondary_colour' => $match1->awayTeam->secondary_colour,
                'tertiary_colour' => $match1->awayTeam->tertiary_colour,
                'address' => $match1->awayTeam->address,
                'created_at' => $match1->awayTeam->created_at->toDateTimeString(),
                'updated_at' => $match1->awayTeam->updated_at->toDateTimeString(),
            ],
            'created_at' => $match1->created_at->toDateTimeString(),
            'updated_at' => $match1->updated_at->toDateTimeString(),
        ]);
    }

    /**
     * @test
     */
    public function it_has_functional_pagination_when_viewing_all()
    {
        $this->verifySimplePagination(Match::class, '/api/matches');
    }

    /**
     * @test
     */
    public function authenticated_user_can_create()
    {
        $this->withoutExceptionHandling();

        Queue::fake();

        $round = factory(Round::class)->create();
        $venue = factory(Venue::class)->create();
        $homeTeam = factory(Team::class)->create();
        $awayTeam = factory(Team::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/matches', [
            'round_id' => $round->uuid,
            'venue_id' => $venue->uuid,
            'home_team_id' => $homeTeam->uuid,
            'home_team_goals' => 13,
            'home_team_behinds' => 7,
            'away_team_id' => $awayTeam->uuid,
            'away_team_goals' => 19,
            'away_team_behinds' => 17,
            'start_time' => now()->toDateTimeString(),
            'attendance' => 75215,
            'reference' => '2019R1',
            'name' => 'James Hird Trophy',
        ]);

        $response->assertStatus(201);

        $match = Match::with(['round', 'venue', 'homeTeam', 'awayTeam'])->first();

        $response->assertExactJson([
            'data' => [
                'id' => $match->uuid,
                'home_team_goals' => $match->home_team_goals,
                'home_team_behinds' => $match->home_team_behinds,
                'home_team_points' => $match->home_team_goals * 6 + $match->home_team_behinds,
                'away_team_goals' => $match->away_team_goals,
                'away_team_behinds' => $match->away_team_behinds,
                'away_team_points' => $match->away_team_goals * 6 + $match->away_team_behinds,
                'start_time' => $match->start_time,
                'attendance' => $match->attendance,
                'reference' => $match->reference,
                'name' => $match->name,
                'round' => [
                    'id' => $match->round->uuid,
                    'round' => $match->round->round,
                    'name' => $match->round->name,
                    'created_at' => $match->round->created_at->toDateTimeString(),
                    'updated_at' => $match->round->updated_at->toDateTimeString(),
                    'series' => [
                        'id' => $match->round->series->uuid,
                        'name' => $match->round->series->name,
                        'points_for_win' => $match->round->series->points_for_win,
                        'percentage_divisor' => $match->round->series->percentage_divisor,
                        'league' => [
                            'id' => $match->round->series->league->uuid,
                            'abbreviation' => $match->round->series->league->abbreviation,
                            'name' => $match->round->series->league->name,
                            'created_at' => $match->round->series->league->created_at->toDateTimeString(),
                            'updated_at' => $match->round->series->league->updated_at->toDateTimeString(),
                        ],
                        'created_at' => $match->round->series->created_at->toDateTimeString(),
                        'updated_at' => $match->round->series->updated_at->toDateTimeString(),
                    ],
                ],
                'venue' => [
                    'id' => $match->venue->uuid,
                    'abbreviation' => $match->venue->abbreviation,
                    'name' => $match->venue->name,
                    'capacity' => $match->venue->capacity,
                    'latitude' => $match->venue->latitude,
                    'longitude' => $match->venue->longitude,
                    'created_at' => $match->venue->created_at->toDateTimeString(),
                    'updated_at' => $match->venue->updated_at->toDateTimeString(),
                ],
                'home_team' => [
                    'id' => $match->homeTeam->uuid,
                    'abbreviation' => $match->homeTeam->abbreviation,
                    'short_name' => $match->homeTeam->short_name,
                    'full_name' => $match->homeTeam->full_name,
                    'primary_colour' => $match->homeTeam->primary_colour,
                    'secondary_colour' => $match->homeTeam->secondary_colour,
                    'tertiary_colour' => $match->homeTeam->tertiary_colour,
                    'address' => $match->homeTeam->address,
                    'created_at' => $match->homeTeam->created_at->toDateTimeString(),
                    'updated_at' => $match->homeTeam->updated_at->toDateTimeString(),
                ],
                'away_team' => [
                    'id' => $match->awayTeam->uuid,
                    'abbreviation' => $match->awayTeam->abbreviation,
                    'short_name' => $match->awayTeam->short_name,
                    'full_name' => $match->awayTeam->full_name,
                    'primary_colour' => $match->awayTeam->primary_colour,
                    'secondary_colour' => $match->awayTeam->secondary_colour,
                    'tertiary_colour' => $match->awayTeam->tertiary_colour,
                    'address' => $match->awayTeam->address,
                    'created_at' => $match->awayTeam->created_at->toDateTimeString(),
                    'updated_at' => $match->awayTeam->updated_at->toDateTimeString(),
                ],
                'created_at' => $match->created_at->toDateTimeString(),
                'updated_at' => $match->updated_at->toDateTimeString(),
            ],
        ]);

        Queue::assertPushed(GenerateLadder::class, function ($job) use ($round) {
            return $job->series->uuid === $round->series->uuid;
        });
    }

    /**
     * @test
     */
    public function upon_create_it_validates_required_fields()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/matches', []);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'round_id' => [
                    'The round id field is required.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_basic_attributes()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/matches', [
            'round_id' => 'xxx',
            'venue_id' => 'xxx',
            'home_team_id' => 'xxx',
            'home_team_goals' => 'xxx',
            'home_team_behinds' => 'xxx',
            'away_team_id' => 'xxx',
            'away_team_goals' => 'xxx',
            'away_team_behinds' => 'xxx',
            'start_time' => 'xxx',
            'attendance' => 'xxx',
            'reference' => $this->stubText(21),
            'name' => $this->stubText(65001),
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'round_id' => [
                    'The selected round id is invalid.',
                ],
                'venue_id' => [
                    'The selected venue id is invalid.',
                ],
                'home_team_id' => [
                    'The selected home team id is invalid.',
                ],
                'home_team_goals' => [
                    'The home team goals must be a number.',
                ],
                'home_team_behinds' => [
                    'The home team behinds must be a number.',
                ],
                'away_team_id' => [
                    'The selected away team id is invalid.',
                ],
                'away_team_goals' => [
                    'The away team goals must be a number.',
                ],
                'away_team_behinds' => [
                    'The away team behinds must be a number.',
                ],
                'start_time' => [
                    'The start time is not a valid date.',
                ],
                'attendance' => [
                    'The attendance must be a number.',
                ],
                'reference' => [
                    'The reference may not be greater than 20 characters.',
                ],
                'name' => [
                    'The name may not be greater than 65000 characters.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_create_it_validates_scores_are_positive()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/matches', [
            'round_id' => 'xxx',
            'venue_id' => 'xxx',
            'home_team_id' => 'xxx',
            'home_team_goals' => -1,
            'home_team_behinds' => -2,
            'away_team_id' => 'xxx',
            'away_team_goals' => -3,
            'away_team_behinds' => -4,
            'start_time' => 'xxx',
            'attendance' => 'xxx',
            'reference' => $this->stubText(21),
            'name' => $this->stubText(65001),
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'round_id' => [
                    'The selected round id is invalid.',
                ],
                'venue_id' => [
                    'The selected venue id is invalid.',
                ],
                'home_team_id' => [
                    'The selected home team id is invalid.',
                ],
                'home_team_goals' => [
                    'The home team goals must be at least 0.',
                ],
                'home_team_behinds' => [
                    'The home team behinds must be at least 0.',
                ],
                'away_team_id' => [
                    'The selected away team id is invalid.',
                ],
                'away_team_goals' => [
                    'The away team goals must be at least 0.',
                ],
                'away_team_behinds' => [
                    'The away team behinds must be at least 0.',
                ],
                'start_time' => [
                    'The start time is not a valid date.',
                ],
                'attendance' => [
                    'The attendance must be a number.',
                ],
                'reference' => [
                    'The reference may not be greater than 20 characters.',
                ],
                'name' => [
                    'The name may not be greater than 65000 characters.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_create()
    {
        $round = factory(Round::class)->create();

        $response = $this->postJson('/api/matches', [
            'round_id' => $round->id,
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_update_match()
    {
        Queue::fake();

        $this->actingAs(factory(User::class)->create());

        $match = factory(Match::class)->create();

        $round = factory(Round::class)->create();
        $venue = factory(Venue::class)->create();
        $homeTeam = factory(Team::class)->create();
        $awayTeam = factory(Team::class)->create();

        $data = [
            'round_id' => $round->uuid,
            'venue_id' => $venue->uuid,
            'home_team_id' => $homeTeam->uuid,
            'home_team_goals' => 13,
            'home_team_behinds' => 7,
            'away_team_id' => $awayTeam->uuid,
            'away_team_goals' => 19,
            'away_team_behinds' => 17,
            'start_time' => now()->toDateTimeString(),
            'attendance' => 75215,
            'reference' => '2019R1',
            'name' => 'James Hird Trophy',
        ];

        $response = $this->putJson('/api/matches/'.$match->uuid, $data);

        $response->assertOk();

        $match->refresh();

        $response->assertExactJson([
            'data' => [
                'id' => $match->uuid,
                'home_team_goals' => $match->home_team_goals,
                'home_team_behinds' => $match->home_team_behinds,
                'home_team_points' => $match->home_team_goals * 6 + $match->home_team_behinds,
                'away_team_goals' => $match->away_team_goals,
                'away_team_behinds' => $match->away_team_behinds,
                'away_team_points' => $match->away_team_goals * 6 + $match->away_team_behinds,
                'start_time' => $match->start_time,
                'attendance' => $match->attendance,
                'reference' => $match->reference,
                'name' => $match->name,
                'round' => [
                    'id' => $match->round->uuid,
                    'round' => $match->round->round,
                    'name' => $match->round->name,
                    'created_at' => $match->round->created_at->toDateTimeString(),
                    'updated_at' => $match->round->updated_at->toDateTimeString(),
                    'series' => [
                        'id' => $match->round->series->uuid,
                        'name' => $match->round->series->name,
                        'points_for_win' => $match->round->series->points_for_win,
                        'percentage_divisor' => $match->round->series->percentage_divisor,
                        'league' => [
                            'id' => $match->round->series->league->uuid,
                            'abbreviation' => $match->round->series->league->abbreviation,
                            'name' => $match->round->series->league->name,
                            'created_at' => $match->round->series->league->created_at->toDateTimeString(),
                            'updated_at' => $match->round->series->league->updated_at->toDateTimeString(),
                        ],
                        'created_at' => $match->round->series->created_at->toDateTimeString(),
                        'updated_at' => $match->round->series->updated_at->toDateTimeString(),
                    ],
                ],
                'venue' => [
                    'id' => $match->venue->uuid,
                    'abbreviation' => $match->venue->abbreviation,
                    'name' => $match->venue->name,
                    'capacity' => $match->venue->capacity,
                    'latitude' => $match->venue->latitude,
                    'longitude' => $match->venue->longitude,
                    'created_at' => $match->venue->created_at->toDateTimeString(),
                    'updated_at' => $match->venue->updated_at->toDateTimeString(),
                ],
                'home_team' => [
                    'id' => $match->homeTeam->uuid,
                    'abbreviation' => $match->homeTeam->abbreviation,
                    'short_name' => $match->homeTeam->short_name,
                    'full_name' => $match->homeTeam->full_name,
                    'primary_colour' => $match->homeTeam->primary_colour,
                    'secondary_colour' => $match->homeTeam->secondary_colour,
                    'tertiary_colour' => $match->homeTeam->tertiary_colour,
                    'address' => $match->homeTeam->address,
                    'created_at' => $match->homeTeam->created_at->toDateTimeString(),
                    'updated_at' => $match->homeTeam->updated_at->toDateTimeString(),
                ],
                'away_team' => [
                    'id' => $match->awayTeam->uuid,
                    'abbreviation' => $match->awayTeam->abbreviation,
                    'short_name' => $match->awayTeam->short_name,
                    'full_name' => $match->awayTeam->full_name,
                    'primary_colour' => $match->awayTeam->primary_colour,
                    'secondary_colour' => $match->awayTeam->secondary_colour,
                    'tertiary_colour' => $match->awayTeam->tertiary_colour,
                    'address' => $match->awayTeam->address,
                    'created_at' => $match->awayTeam->created_at->toDateTimeString(),
                    'updated_at' => $match->awayTeam->updated_at->toDateTimeString(),
                ],
                'created_at' => $match->created_at->toDateTimeString(),
                'updated_at' => $match->updated_at->toDateTimeString(),
            ],
        ]);

        Queue::assertPushed(GenerateLadder::class, function ($job) use ($round) {
            return $job->series->uuid === $round->series->uuid;
        });
    }

    /**
     * @test
     */
    public function can_update_nullable_values_to_null()
    {
        $this->withoutExceptionHandling();

        $this->actingAs(factory(User::class)->create());

        $match = factory(Match::class)->create();

        $data = [
            'venue_id' => null,
            'home_team_id' => null,
            'home_team_goals' => null,
            'home_team_behinds' => null,
            'away_team_id' => null,
            'away_team_goals' => null,
            'away_team_behinds' => null,
            'start_time' => null,
            'attendance' => null,
            'reference' => null,
            'name' => null,
        ];

        $response = $this->putJson('/api/matches/'.$match->uuid, $data);

        $response->assertOk();

        unset($data['venue_id']);
        unset($data['home_team_id']);
        unset($data['away_team_id']);

        $response->assertJsonFragment(array_merge($data, [
            'venue' => null,
            'home_team' => null,
            'away_team' => null,
        ]));
    }

    /**
     * @test
     */
    public function upon_update_it_validates_basic_attributes()
    {
        $match = factory(Match::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->putJson('/api/matches/'.$match->uuid, [
            'round_id' => 'xxx',
            'venue_id' => 'xxx',
            'home_team_id' => 'xxx',
            'home_team_goals' => 'xxx',
            'home_team_behinds' => 'xxx',
            'away_team_id' => 'xxx',
            'away_team_goals' => 'xxx',
            'away_team_behinds' => 'xxx',
            'start_time' => 'xxx',
            'attendance' => 'xxx',
            'reference' => $this->stubText(21),
            'name' => $this->stubText(65001),
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'round_id' => [
                    'The selected round id is invalid.',
                ],
                'venue_id' => [
                    'The selected venue id is invalid.',
                ],
                'home_team_id' => [
                    'The selected home team id is invalid.',
                ],
                'home_team_goals' => [
                    'The home team goals must be a number.',
                ],
                'home_team_behinds' => [
                    'The home team behinds must be a number.',
                ],
                'away_team_id' => [
                    'The selected away team id is invalid.',
                ],
                'away_team_goals' => [
                    'The away team goals must be a number.',
                ],
                'away_team_behinds' => [
                    'The away team behinds must be a number.',
                ],
                'start_time' => [
                    'The start time is not a valid date.',
                ],
                'attendance' => [
                    'The attendance must be a number.',
                ],
                'reference' => [
                    'The reference may not be greater than 20 characters.',
                ],
                'name' => [
                    'The name may not be greater than 65000 characters.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function upon_update_it_validates_scores_are_positive()
    {
        $match = factory(Match::class)->create();

        $this->actingAs(factory(User::class)->create());

        $response = $this->putJson('/api/matches/'.$match->uuid, [
            'round_id' => 'xxx',
            'venue_id' => 'xxx',
            'home_team_id' => 'xxx',
            'home_team_goals' => -1,
            'home_team_behinds' => -2,
            'away_team_id' => 'xxx',
            'away_team_goals' => -3,
            'away_team_behinds' => -4,
            'start_time' => 'xxx',
            'attendance' => 'xxx',
            'reference' => $this->stubText(21),
            'name' => $this->stubText(65001),
        ]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'round_id' => [
                    'The selected round id is invalid.',
                ],
                'venue_id' => [
                    'The selected venue id is invalid.',
                ],
                'home_team_id' => [
                    'The selected home team id is invalid.',
                ],
                'home_team_goals' => [
                    'The home team goals must be at least 0.',
                ],
                'home_team_behinds' => [
                    'The home team behinds must be at least 0.',
                ],
                'away_team_id' => [
                    'The selected away team id is invalid.',
                ],
                'away_team_goals' => [
                    'The away team goals must be at least 0.',
                ],
                'away_team_behinds' => [
                    'The away team behinds must be at least 0.',
                ],
                'start_time' => [
                    'The start time is not a valid date.',
                ],
                'attendance' => [
                    'The attendance must be a number.',
                ],
                'reference' => [
                    'The reference may not be greater than 20 characters.',
                ],
                'name' => [
                    'The name may not be greater than 65000 characters.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_update()
    {
        $match = factory(Match::class)->create();

        $response = $this->putJson('/api/matches/'.$match->uuid, ['attendance' => 19561]);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_delete()
    {
        $this->withoutExceptionHandling();

        Queue::fake();

        $this->actingAs(factory(User::class)->create());

        $match = factory(Match::class)->create();

        $response = $this->delete('/api/matches/'.$match->uuid);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('matches', ['id' => $match->id]);

        Queue::assertPushed(GenerateLadder::class, function ($job) use ($match) {
            return $job->series->uuid === $match->series->uuid;
        });
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_delete()
    {
        $match = factory(Match::class)->create();

        $response = $this->deleteJson('/api/matches/'.$match->uuid);

        $response->assertStatus(401);
    }
}
