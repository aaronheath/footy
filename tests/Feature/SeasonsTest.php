<?php

namespace Tests\Feature;

use App\Models\Season;
use App\Models\Series;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SeasonsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_view_season()
    {
        $season = factory(Season::class)->create();

        $response = $this->getJson('/api/seasons/'.$season->uuid);

        $response->assertOk();

        $response->assertExactJson([
            'data' => [
                'id' => $season->uuid,
                'year' => $season->year,
                'created_at' => $season->created_at->toDateTimeString(),
                'updated_at' => $season->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /** @test */
    public function can_view_seasons()
    {
        factory(Season::class)->create(['year' => 2001]);
        factory(Season::class)->create(['year' => 1997]);
        factory(Season::class)->create(['year' => 2003]);

        $response = $this->getJson('/api/seasons');

        $response->assertOk();

        $response->assertJsonCount(3, 'data');
        $this->assertEquals(1997, $response->decodeResponseJson('data.0.year'));
        $this->assertEquals(2001, $response->decodeResponseJson('data.1.year'));
        $this->assertEquals(2003, $response->decodeResponseJson('data.2.year'));
    }

    /**
     * @test
     */
    public function it_has_functional_pagination_when_viewing_all()
    {
        $this->verifySimplePagination(Season::class, '/api/seasons');
    }

    /** @test */
    public function authenticated_user_can_create_season()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/seasons', ['year' => 2004]);

        $response->assertStatus(201);

        $season = Season::first();

        $response->assertExactJson([
            'data' => [
                'id' => $season->uuid,
                'year' => 2004,
                'created_at' => $season->created_at->toDateTimeString(),
                'updated_at' => $season->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /** @test */
    public function unauthenticated_user_cannot_create_season()
    {
        $response = $this->postJson('/api/seasons', ['year' => 2004]);

        $response->assertStatus(401);
    }

    /** @test */
    public function it_validates_the_year_when_creating_season()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->postJson('/api/seasons', ['year' => 'xxxx']);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'year' => [
                    'The year must be a number.',
                    'The year must be at least 1990.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function authenticated_user_can_update_season()
    {
        $this->actingAs(factory(User::class)->create());

        $season = factory(Season::class)->create(['year' => 1993]);

        $response = $this->putJson('/api/seasons/'.$season->uuid, ['year' => 2004]);

        $response->assertOk();

        $season->refresh();

        $response->assertExactJson([
            'data' => [
                'id' => $season->uuid,
                'year' => 2004,
                'created_at' => $season->created_at->toDateTimeString(),
                'updated_at' => $season->updated_at->toDateTimeString(),
            ],
        ]);
    }

    /**
     * @test
     */
    public function it_validates_the_year_when_updating_season()
    {
        $this->actingAs(factory(User::class)->create());

        $season = factory(Season::class)->create(['year' => 1993]);

        $response = $this->putJson('/api/seasons/'.$season->uuid, ['year' => 'xxxx']);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'year' => [
                    'The year must be a number.',
                    'The year must be at least 1990.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function it_validates_the_year_is_unqiue_when_updating_season()
    {
        $this->actingAs(factory(User::class)->create());

        $season = factory(Season::class)->create(['year' => 1993]);
        $otherSeason = factory(Season::class)->create(['year' => 1994]);

        $response = $this->putJson('/api/seasons/'.$season->uuid, ['year' => $otherSeason->year]);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'year' => [
                    'The year has already been taken.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_update_season()
    {
        $season = factory(Season::class)->create(['year' => 1993]);

        $response = $this->putJson('/api/seasons/'.$season->uuid, ['year' => 2004]);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function authenticated_user_can_delete_season()
    {
        $this->actingAs(factory(User::class)->create());

        $season = factory(Season::class)->create();

        $response = $this->delete('/api/seasons/'.$season->uuid);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('seasons', ['id' => $season->id]);
    }

    /**
     * @test
     */
    public function authenticated_user_cannot_delete_used_season()
    {
        $this->actingAs(factory(User::class)->create());

        $series = factory(Series::class)->create();

        $response = $this->delete('/api/seasons/'.$series->season->uuid);

        $response->assertStatus(422);

        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'Unable to delete season as it has active dependants.',
            ],
        ]);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_delete_season()
    {
        $season = factory(Season::class)->create(['year' => 1993]);

        $response = $this->deleteJson('/api/seasons/'.$season->uuid);

        $response->assertStatus(401);
    }
}
