const mix = require('laravel-mix');
require('mix-tailwindcss');

mix.webpackConfig({ devServer: { disableHostCheck: true } })
  .js('resources/js/app.js', 'public/js')
  .ts('resources/webapps/public/app.js', 'public/js/app-public.js')
  .ts('resources/webapps/admin/app.js', 'public/js/app-admin.js')
  .extract()
  .tailwind('./tailwind.config.js')
  .sass('resources/sass/app.scss', 'public/css')
  .sass('resources/sass/webapp-public.scss', 'public/css')
  .sass('resources/sass/webapp-admin.scss', 'public/css')
  .sourceMaps()
  .disableNotifications();

if (mix.inProduction()) {
  mix.version();
}
